Tutorial:

### With the Ionic CLI:


```bash
$ npm install
$ ionic serve
```


## instrucciones  para Mac
#### instalar cordova
```bash
$ sudo npm install -g cordova
```

#### instalar ionic v3
```bash
$ sudo npm install -g ionic@3
```

#### instalar otras dependencias necesarias
```bash
$ sudo npm install -g ios-sim
$ sudo npm install -g ios-deploy --unsafe-perm=true
$ npm install -g node-gyp
```


#### Se crea la plataforma para ios
```bash
$ ionic cordova platform add ios
```



una vez que ya esta creada la platafoma se debe abrir el proyecto desde el xcode y buscar en ./beautyaccess/platforms/ios/Beautyaccess.xcodeproj