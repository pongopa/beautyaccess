import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

// Implementamos la librería de notificaciones Push.
import { Push, PushObject, PushOptions } from '@ionic-native/push';
//import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';

@Component({
  templateUrl: 'app.html',
  providers:[Push]
})
export class BeautyAccess {
	rootPage = 'VerificarPage';
	constructor(platform: Platform, 
				statusBar: StatusBar, 
				splashScreen: SplashScreen, 
				public storage: Storage,
				private push: Push
			   ) {
					    platform.ready().then(() => {
						        statusBar.styleDefault();
						        splashScreen.hide();
					    });
						if(localStorage.getItem('OPEN') == 'true'){
							this.rootPage = 'HomePage';
						}else{
							this.rootPage = 'VerificarPage';
							//this.rootPage = 'HomePage';
						}
						console.log(platform.is('ios'));
						      if (platform.is('ios')) {
						      	        localStorage.setItem('PUSH_PLATFORM', 'ios');
										this.loadcuerpo();
						}else if(platform.is('android')){
							            localStorage.setItem('PUSH_PLATFORM', 'android');
										this.loadcuerpo();
						}
						// comprobamos los permisos
	}
	loadcuerpo() {
		   // comprobamos los permisos
			this.push.hasPermission().then((res: any) => {
			    if (res.isEnabled) {
			      console.log('We have permission to send push notifications');
			    } else {
			      console.log('We do not have permission to send push notifications');
			    }
			});
			// inicializamos la configuración para android y ios
			const options: PushOptions = {
			   android: {
			       senderID: '874682685677'
			   },
			   ios: {
			      // senderID: 'SENDER_ID'//si no lo pones, se generará un token para APNS
			       alert: 'true',
			       badge: true,
			       sound: 'false'
			   },
			   windows: {}
			};
			const pushObject: PushObject = this.push.init(options);
			pushObject.on('notification').subscribe((notification: any) => {
			  //aquí recibimos las notificaciones de firebase
			});
			pushObject.on('registration').subscribe((registration: any) => {
			    const registrationId = registration.registrationId;
			    localStorage.setItem('PUSH_TOKEN', registrationId);
			    //registrationId lo debes guardar en mysql o similar para reutilizar
			});
			pushObject.on('error').subscribe(error => {
			    //console.error('Error with Push plugin', error)
			});
	}//fin function


}

