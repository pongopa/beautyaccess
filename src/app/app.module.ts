import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, Nav } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
// Implementamos la librería de notificaciones Push.
//import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { Push} from '@ionic-native/push';
// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { BeautyAccess } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { IonicStorageModule } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { HttpModule } from '@angular/http';
import { CustomFormsModule } from 'ng2-validation';
import { ThemeableBrowser } from '@ionic-native/themeable-browser';

import { HttpBackend, HttpXhrBackend } from '@angular/common/http'
import { NativeHttpModule, NativeHttpBackend, NativeHttpFallback } from 'ionic-native-http-connection-backend';
import { RequestOptions, Http } from '@angular/http';

import { Platform } from 'ionic-angular';




export const firebaseConfig = {
  apiKey: "AIzaSyAbD19sI5Xz_jKjEMCQhjuYXVPrdn3YEsY",
  authDomain: "codelab-22299.firebaseapp.com",
  databaseURL: "https://codelab-22299.firebaseio.com",
  projectId: "codelab-22299",
  storageBucket: "codelab-22299.appspot.com",
  messagingSenderId: "141181591031"
};

@NgModule({
  declarations: [
    BeautyAccess,

  ],
  imports: [
    BrowserModule,
    HttpModule,
    CustomFormsModule,
    NativeHttpModule,

    IonicModule.forRoot(BeautyAccess),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    Ionic2RatingModule // Put ionic2-rating module here
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    BeautyAccess
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Push,
    Nav,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    InAppBrowser,
    ThemeableBrowser,
    {provide: HttpBackend, useClass: NativeHttpFallback, deps: [Platform, NativeHttpBackend, HttpXhrBackend]}

  ]
})
export class AppModule {}
