import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { MyNavbarComponent } from './my-navbar-components';

@NgModule({
  declarations: [
    MyNavbarComponent,
   ],
  imports: [
    IonicModule,
  ],
  exports: [
    MyNavbarComponent
  ],
  entryComponents:[
    MyNavbarComponent
  ]
})
export class MyNavbarModule {}
