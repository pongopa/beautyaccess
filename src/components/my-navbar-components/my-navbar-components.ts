import { Component, Input } from '@angular/core';

import { NavController, Nav } from 'ionic-angular';

@Component({
  selector: 'my-navbar-header',
  templateUrl: 'my-navbar-components.html'
})
export class MyNavbarComponent {
  @Input('usuario') usuario: string = "";

  constructor(
    public nav: Nav,
    private navController: NavController
    ) {
  }

  ngAfterViewInit(){
  }

  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin mapa

  perfil(){
      if(localStorage.getItem('OPEN') == 'true'){ 
           this.nav.push('PerfilPage');
      }else{
          this.nav.push("VerificarPage");
      }
  }//fin perfil

}