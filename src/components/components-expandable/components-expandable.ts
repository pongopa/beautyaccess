import { Component, Input, ViewChild, ElementRef, Renderer } from '@angular/core';

@Component({
  selector: 'app-expandable',
  templateUrl: 'components-expandable.html'
})
export class ComponentsExpandableComponent {
   @ViewChild('expandWrapper', {read: ElementRef}) expandWrapper;
    @Input('expanded') expanded: boolean = false;
    @Input('height') height: string = "150px";
 
    constructor(public renderer: Renderer) {
 
    }
 
    ngAfterViewInit(){
      if(this.height){
        this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'height', this.height + 'px');    
      }
    }

}