import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeautypointPage } from './beautypoint';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    BeautypointPage
  ],
  imports: [
    IonicPageModule.forChild(BeautypointPage),
    MyNavbarModule
  ],
  exports: [
    BeautypointPage
  ]
})
export class BeautypointPageModule {}
