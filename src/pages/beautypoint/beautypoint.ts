import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-beautypoint',
  templateUrl: 'beautypoint.html',
  providers:[GlobalVars, Principal]
})
export class BeautypointPage {
    usuario = "";
    historiaid = "";
    data = "";
    usuarioid = "";

    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    constructor(
              public nav: Nav, 
              public navCtrl: NavController, 
              public _service: Principal,
              public storage: Storage,
              private navController: NavController, 
              public navParams: NavParams) {

  }
   link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid  = localStorage.getItem('ID');

                      this._service.beautypoint(this.usuarioid).subscribe((response) => { 
                            if (response.code!=200){

                            }else{
                              this.data = response.datos;//publicidad
                              console.log(this.data);
                            }
                      });
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  
}//fin componente 
