import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoriaPage } from './historia';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    HistoriaPage

  ],
  imports: [
    IonicPageModule.forChild(HistoriaPage),
    MyNavbarModule
  ],
  exports: [
    HistoriaPage
  ]
})
export class HistoriaPageModule {}
