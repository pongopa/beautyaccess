import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Historia } from '../../providers/servicio/historia';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-historia',
  templateUrl: 'historia.html',
  providers:[Historia, GlobalVars, Principal]
})
export class HistoriaPage {
    usuario = "";
    historiaid = "";
    data = "";
    general = 1;
    saldo = 0;

    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    constructor(
              public nav: Nav, 
              public navCtrl: NavController, 
              private navController: NavController, 
              public service: Historia,
              public _service: Principal,
              public storage: Storage,
              public navParams: NavParams) {

  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
            this.historiaid = this.navParams.get('id');
            this.urlimg2 = this.urlimg.getMyserve();
                      this._service.historias(this.historiaid).subscribe((response) => { 
                            if (response.code!=200){ 


                            }else{
                              console.log(response);
                              this.data = response.datos1;//publicidad
                              this.saldo = response.saldo;
                                 //this.navCtrl.remove(this.navCtrl.getPrevious().index);
                                 if(this.navCtrl.getPrevious().name != "PrincipalPage"){
                                    this.navCtrl.remove(this.navCtrl.getPrevious().index);
                                  }
                            }
                      });
        }else{
            this.storage.remove('ID'); 
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  verhistoria(){
     this.nav.push('HistorialistaPage');
  }//fin function
  vergeneral(){
     this.general=2;
  }//fin function
  principal(){
    //this.nav.push('PrincipalPage');
    this.navCtrl.setRoot('PrincipalPage');
  }
  
}//fin componente 
