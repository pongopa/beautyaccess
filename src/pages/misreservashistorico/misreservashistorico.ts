import { Component, Input } from '@angular/core';
import { IonicPage, NavController,LoadingController, Loading, AlertController, Nav, MenuController, NavParams, ModalController} from 'ionic-angular';
import { Reservas } from '../../providers/servicio/reservas';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';

declare var google;
@IonicPage()
@Component({
  selector: 'page-misreservashistorico',
  templateUrl: 'misreservashistorico.html',
  providers: [GlobalVars, Reservas]
})
export class MisreservashistoricoPage {
  @Input() data: any; 
  @Input() events: any;

  searchTerm:any="";
  allItems:any;

  items:any;
  headerImage = "";
	//@ViewChild(Nav) nav: Nav;
    map: any;
	marker: any;
	features: any;
	datas: any;
	public marcadores: any;
	public usuariodeno: any;
	public var : any;
	public query = "";
	public msj = "";
	public loading:Loading;

	directionsService: any = null;
	directionsDisplay: any = null;
	bounds: any;
	calificacion: any;
	myLatLng: any;
	waypoints: any[];

	public usuario = "";
	public urlimg   = new GlobalVars();
	public urlimg2  = "";
	public  usuarioid  = "";

	constructor(
	    public nav: Nav,
	    private navCtrl: NavController,
	    private navController: NavController, 
	    public  alertCtrl: AlertController,
	    public menuCtrl: MenuController,
	    private reservas_service: Reservas,
	    public storage: Storage,
	    public loadingCtrl: LoadingController,
	    public navParams: NavParams,
	    public modalCtrl : ModalController,
	  ) { 
          
        this.directionsService = new google.maps.DirectionsService();
	    this.directionsDisplay = new google.maps.DirectionsRenderer();
	    this.bounds            = new google.maps.LatLngBounds();
	    
	}
	ionViewDidLoad() {
    	if(localStorage.getItem('OPEN') == 'true'){
    		this.urlimg2 = this.urlimg.getMyserve();
            this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
            this.loag();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
		    localStorage.setItem('OPEN','false');
		    this.navController.setRoot("VerificarPage");
        }
	}
	loag(){
		this.usuarioid  = localStorage.getItem('ID');
		this.reservas_service.ReservashistoricoAll(this.usuarioid).subscribe((response) => {
			this.loading.dismiss().then( () => { 
		 	        if (response.code!=200){

		            }else{
		 				 console.log(response['datos']);
		                 this.items       = response['datos'];
		                 this.headerImage = "assets/background/27.jpg";
		                 console.log(this.items);

		                  this.msj = this.navParams.get('msj');
		                  console.log(this.msj);

		                if(this.msj=='1'){
				           let alert = this.alertCtrl.create({
				              title: "Su reserva a sido cancelada con éxito",
				              message:"&nbsp;&nbsp; Su reserva fue cancelada con éxito",
				              buttons: [
				                    {
				                      text: 'Aceptar',
				                      cssClass:'ion-aceptar',
				                      handler: data => {
				                        
				                      }
				                    }
				                  ],
				          });
				          alert.present();
				      	}

		            }
            });
	    },error => {
	          let alerta = this.alertCtrl.create({
	                title: "Aviso",
	                message: "Disculpe, no se pudo conectar al servidor",
	                buttons: [
	                  {
	                      text: "Reintentar",
	                      role: 'cancel',
	                      cssClass:'ion-aceptar',
	                      handler: data => {
	                        this.loading.dismiss().then( () => {this.nav.pop();});
	                      }
	                  }
	                ]
	              });
	              alerta.present();
	    });//FIN POST
		this.loading = this.loadingCtrl.create({
            dismissOnPageChange: false,
            content: 'Cargando...'
        });
        this.loading.present();
	}
	link(componente, p1){ 
	    //console.log(p1);
	    if(componente!=""){
	     this.nav.push(componente, { id : p1});
	    }
	}//fin function
	mapa(){
        this.navController.setRoot('InicioPage');
	}//fin function
	perfil(){
	    this.nav.push('PerfilPage');
	}//fin function
	getItems(event: any):void {
	    if (!this.allItems) {
	      this.allItems = this.items;
	    }
	    this.items = this.allItems.filter((item) => {
	        return item.razon_social.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
	    });
	    
	}
	onEvent(event:string, item:any) {//ITEM [EVENT OR SELECTED ITEM]
	    //if (this.events[event]) {
	      if ('onTextChange' === event) {
	        this.getItems(item);
	        //this.events[event](this.searchTerm);
	      } else {
	        //this.events[event](item);
	        this.nav.push("MisreservasverPage", { id : item.id});
	      }
	    //}
	    console.log(event);
	}
	

}//fin class