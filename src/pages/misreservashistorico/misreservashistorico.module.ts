import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisreservashistoricoPage } from './misreservashistorico';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    MisreservashistoricoPage
  ],
  imports: [
    IonicPageModule.forChild(MisreservashistoricoPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    MisreservashistoricoPage
  ]
})
export class MisreservashistoricoPageModule {}
