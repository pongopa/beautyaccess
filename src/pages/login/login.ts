import { Component } from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic';
import { ToastController, IonicPage, NavController,LoadingController, Loading, AlertController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
//import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { Login } from '../../providers/servicio/login';
import { Verifica } from '../../providers/servicio/verifica';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers:[Login, Diagnostic, Verifica]
})
export class LoginPage {

  myForm: FormGroup;
 // user: Observable<firebase.User>;
  public loading:Loading;
  public registro:Login;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private _service: Login,
    private _service_ve: Verifica,
    public storage: Storage,
    private diagnostic: Diagnostic,
    private toastCtrl: ToastController
  ) {
        this.myForm = this.formBuilder.group({
            code_verificacion: ['', Validators.required]
        });
        this.diagnostic.isLocationAvailable().then((state) => {
          if (state === true) {
            
          }else{
            let alert = this.alertCtrl.create({
              title: 'Informacion del GPS',
              message:'El GPS no esta activado en tu dispositivo',
                 buttons: [
                {
                  text: 'Entendido',
                  role: 'cancel',
                  handler: data => {
                    let toast = this.toastCtrl.create({
                              message: 'La aplicacion necesita del GPS, enciendalo para continuar.',
                              duration: 5000,
                              position: 'top'
                            });
                            toast.present();
                  }
                },
                {
                  text: 'Habilitar GPS',
                  handler: data => {
                    this.diagnostic.switchToLocationSettings()
                    this.diagnostic.isLocationAvailable()
                  }
                }
              ]
            });
            alert.present();
          }
        }).catch(e => console.error(e));
  }//fin constructor

  ir(opcion){

  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
          this._service_ve.Verifica().subscribe((response) => { 
             console.log(response);

                  if (response.code!=200){

                  }else{
                    this.navCtrl.setRoot('HomePage');
                  }
          });
          console.log("juan");
        }else{
             console.log("juan2");
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
        }
  }
  loginUser(){
    this._service.login(this.myForm.value).subscribe((response) => {  
                          if(response.token=='false'){
                               this.loading.dismiss().then( () => {
                                  let alert = this.alertCtrl.create({
                                    message: response.msg,
                                    buttons: [
                                      {
                                        text: "Ok",
                                        role: 'cancel'
                                      }
                                    ]
                                  });
                                  alert.present();
                                });
                    }else if (response.code!=200){
                                this.loading.dismiss().then( () => {
                                  let alert = this.alertCtrl.create({
                                    message: response.msg,
                                    buttons: [
                                      {
                                        text: "Ok",
                                        role: 'cancel'
                                      }
                                    ]
                                  });
                                  alert.present();
                                });
                    }else{
                          this.loading.dismiss().then( () => {
                                //Guardamos la sesion de datos aca
                                this.storage.remove('ID');
                                this.storage.remove('USERNAME');
                                this.storage.remove('NOMBRES');
                                this.storage.remove('APELLIDOS');
                                this.storage.remove('TOKEN');

                                this.storage.set('ID', response['datos']['id']);  
                                this.storage.set('USERNAME', response['datos']['username']);  
                                this.storage.set('NOMBRESAPELLIDOS', response['datos']['nombre_apellido']);  
                                this.storage.set('TIPOUSUARIO', response['datos']['role_id']);
                                this.storage.set('OPEN', 'true');
                                this.storage.set('TOKEN', response.token);

                                //cambio  
                                localStorage.setItem('ID', response['datos']['id']);
                                localStorage.setItem('USERNAME', response['datos']['username']);
                                localStorage.setItem('NOMBRESAPELLIDOS', response['datos']['nombre_apellido']);
                                localStorage.setItem('TIPOUSUARIO', response['datos']['role_id']);
                                localStorage.setItem('OPEN','true');
                                localStorage.setItem('TOKEN', response.token);

                                this.storage.set('OPEN', 'true');
                                this.navCtrl.setRoot('HomePage');
                          });
                    }//Fin else
    },error => {
          let alerta = this.alertCtrl.create({
                title: "Aviso",
                message: "Disculpe, no se pudo conectar al servidor",
                buttons: [
                  {
                      text: "Reintentar",
                      role: 'cancel',
                      cssClass:'ion-aceptar',
                      handler: data => {
                        this.loading.dismiss().then( () => {this.navCtrl.pop();});
                      }
                  }
                ]
              });
              alerta.present();
    });//FIN POST
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: false,
    });
    this.loading.present();
  }
  

  goToSignup(){
    this.navCtrl.push('SignupPage');
  }

  goToResetPassword(){
    this.navCtrl.push('ResetPasswordPage');
  }

}
