import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Salon } from '../../providers/servicio/salon';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Ionic2RatingModule } from "ionic2-rating";
import { IonicPage, NavController, LoadingController, Loading, AlertController, ViewController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-comentario',
  templateUrl: 'comentario.html',
  providers:[Salon, GlobalVars]
})
export class ComentarioPage {
myForm: FormGroup;
salonid: any;
public items:any;
public loading:Loading; 
public usuario = "";
public datos: any;
public contentString = "";
public contentStringT = "";
public contador:number = 0;
public valor:number = 0;
public result:any = 0;

public razon_social = "";
public email        = "";
public direccion    = "";
public telefono     = "";

public img1     = "";
public img2     = "";
public img3     = "";
public urlimg2  = "";
public urlimg   = new GlobalVars();

  constructor(public navController: NavController,
              public alertCtrl: AlertController, 
              public nave: Nav,  
              public nav: Nav,
              private viewCtrl: ViewController,
              private _service: Salon,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public formBuilder: FormBuilder,
              public navParams: NavParams) {
    this.myForm = this.formBuilder.group({
      texto: ['', Validators.required]
    });
  } 
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            //this.navCtrl.setRoot('HomePage');
            this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
            this.salonid = this.navParams.get('id');
            this.loadpage();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");

            localStorage.clear();
        }
  } 
  loadpage(){
   console.log(this.salonid);
      this._service.Datoscomentarios(this.salonid).subscribe((response) => { 
        this.loading.dismiss().then( () => {
                if (response.code==200){
                  this.datos        = response['datos'];
                  
                  this.razon_social = this.datos[0]['razon_social'];
                  this.email        = this.datos[0]['email'];
                  this.direccion    = this.datos[0]['direccion'];
                  this.telefono     = this.datos[0]['telefono'];

                  if(this.datos[0]['foto1']==""){this.datos[0]['foto1']="upload/default.png";}
                  if(this.datos[0]['foto2']==""){this.datos[0]['foto2']="upload/default.png";}
                  if(this.datos[0]['foto3']==""){this.datos[0]['foto3']="upload/default.png";}

                  this.img1     = this.datos[0]['foto1'];
                  this.img2     = this.datos[0]['foto2'];
                  this.img3     = this.datos[0]['foto3'];
                  this.urlimg2  = this.urlimg.getMyserve();

                  
                  console.log(response['datos']);
                  console.log(response['datos2']);
                  this.items   = response['datos3'];
                  console.log(this.razon_social);

                  let miscali  = response['datos2']; 

                  this.contentString = '<div item-capa>' +  
                                          '<table border="0"  width="90%">';
                                          miscali.forEach(valores2 => {
                                              this.contador=this.contador+1;
                                              this.valor=this.valor+valores2.valor;
                                              this.contentString +='<tr>'+
                                                              '<td class="calificacion_ver"><span class="item-title">'+valores2.deno+':</span></td>'+
                                                      '<td>';   if(valores2.valor<1){
                                                                      if(valores2.valor<0.5){
                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(valores2.valor<2){
                                                                      if(valores2.valor<1.5){
                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(valores2.valor<3){
                                                                    if(valores2.valor<2.5){
                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(valores2.valor<4){
                                                                    if(valores2.valor<3.5){
                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                     this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(valores2.valor<5){
                                                                   if(valores2.valor<4.5){
                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>';
                                                               }
                                                          }else{
                                                           this.contentString += '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>';
                                                          }
                                      this.contentString +='</td>'+
                                                      '</tr>';
                                          });
                            this.contentString += '</table><br>'+
                                           '</div>'; 

                            this.result = Number(this.valor/this.contador).toFixed(2);
                            if(isNaN(this.result)){this.result=0;}
                            this.contentStringT = '<div>' + 
                                          '<table border="0">';
                                              this.contentStringT +='<tr>'+
                                                      '<td><b class="calificacion" style="">'+this.result+'</b></td>'+
                                                      '<td>';   if(this.result<1){
                                                                      if(this.result<0.5){
                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(this.result<2){
                                                                      if(this.result<1.5){
                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(this.result<3){
                                                                    if(this.result<2.5){
                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(this.result<4){
                                                                    if(this.result<3.5){
                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                     this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(this.result<5){
                                                                   if(this.result<4.5){
                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-11.png"></img>';
                                                               }
                                                          }else{
                                                           this.contentStringT += '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>';
                                                          }
                                      this.contentStringT +='</td>'+
                                                      '</tr>';
                            this.contentStringT += '</table><br>'+
                                           '</div>';                                            
          }else{
            
          }//Fin else
         });
      },error => {
          let alerta = this.alertCtrl.create({
                title: "Aviso",
                message: "Disculpe, no se pudo conectar al servidor",
                buttons: [
                  {
                      text: "Reintentar",
                      role: 'cancel',
                      cssClass:'ion-aceptar',
                      handler: data => {
                        this.loading.dismiss().then( () => {this.nav.pop();});
                      }
                  }
                ]
              });
              alerta.present();
      });//FIN POST
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: false,
        content: 'Cargando...'
      });
      this.loading.present();
  }
  loadpage2(){
      this._service.Datoscomentarios(this.salonid).subscribe((response) => { 
        this.loading.dismiss().then( () => {
          if (response.code==200){
                  this.datos        = response['datos'];
                  this.razon_social = this.datos[0]['razon_social'];
                  this.email        = this.datos[0]['email'];
                  this.direccion    = this.datos[0]['direccion'];
                  this.telefono     = this.datos[0]['telefono'];
                  let miscali  = response['datos2']; 
                  this.items   = response['datos3'];
                  console.log(this.items);
                  this.contentString = '<div>' +     
                                          '<table border="0"  width="90%">';
                                          miscali.forEach(valores2 => {
                                              this.contador=this.contador+1;
                                              this.valor=this.valor+valores2.valor;
                                              this.contentString +='<tr>'+
                                                              '<td class="calificacion_ver"><span>'+valores2.deno+':</span></td>'+
                                                      '<td>';   if(valores2.valor<1){
                                                                      if(valores2.valor<0.5){
                                                                    this.contentString +=  '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentString +=  '<img weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(valores2.valor<2){
                                                                      if(valores2.valor<1.5){
                                                                    this.contentString +=  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentString +=  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(valores2.valor<3){
                                                                    if(valores2.valor<2.5){
                                                                    this.contentString +=  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentString +=  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(valores2.valor<4){
                                                                    if(valores2.valor<3.5){
                                                                    this.contentString +=  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                     this.contentString +=  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(valores2.valor<5){
                                                                   if(valores2.valor<4.5){
                                                                    this.contentString +=  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentString +=  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>';
                                                               }
                                                          }else{
                                                           this.contentString += '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>';
                                                          }
                                      this.contentString +='</td>'+
                                                      '</tr>';
                                          });
                            this.contentString += '</table><br>'+
                                           '</div>'; 

                            this.result = this.valor/this.contador;
                            if(isNaN(this.result)){this.result=0;}
                            this.contentStringT = '<div>' + 
                                          '<table border="0">';
                                              this.contentStringT +='<tr>'+
                                                      '<td><b class="calificacion_general">'+this.result+'</b></td>'+
                                                      '<td>';   if(this.result<1){
                                                                      if(this.result<0.5){
                                                                    this.contentStringT +=  '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentStringT +=  '<img weight="45px" height="45px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(this.result<2){
                                                                      if(this.result<1.5){
                                                                    this.contentStringT +=  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentStringT +=  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(this.result<3){
                                                                    if(this.result<2.5){
                                                                    this.contentStringT +=  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentStringT +=  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(this.result<4){
                                                                    if(this.result<3.5){
                                                                    this.contentStringT +=  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                     this.contentStringT +=  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-11.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>';
                                                               }
                                                          }else if(this.result<5){
                                                                   if(this.result<4.5){
                                                                    this.contentStringT +=  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-12.png"></img>';
                                                               }else{
                                                                    this.contentStringT +=  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                          '<img weight="45px" height="45px" src="./assets/images/iconos-11.png"></img>';
                                                               }
                                                          }else{
                                                           this.contentStringT += '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>' +
                                                                  '<img weight="45px" height="45px" src="./assets/images/iconos-10.png"></img>';
                                                          }
                                      this.contentStringT +='</td>'+
                                                      '</tr>';
                            this.contentStringT += '</table><br>'+
                                           '</div>';                                            
          }else{
            
          }//Fin else
        });
      },error => {
          let alerta = this.alertCtrl.create({
                title: "Aviso",
                message: "Disculpe, no se pudo conectar al servidor",
                buttons: [
                  {
                      text: "Reintentar",
                      role: 'cancel',
                      cssClass:'ion-aceptar',
                      handler: data => {
                        this.loading.dismiss().then( () => {this.nav.pop();});
                      }
                  }
                ]
              });
              alerta.present();
      });//FIN POST
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: false,
        content: 'Cargando...'
      });
      this.loading.present();
  }
 
}//fin componente 
