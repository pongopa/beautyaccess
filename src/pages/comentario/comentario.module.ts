import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComentarioPage } from './comentario';

// Import ionic-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    ComentarioPage
  ],
  imports: [
    Ionic2RatingModule,
    IonicPageModule.forChild(ComentarioPage),
    MyNavbarModule
  ],
  exports: [
    ComentarioPage
  ]
})
export class ComentarioPageModule {}
