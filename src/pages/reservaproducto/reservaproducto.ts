import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Salon } from '../../providers/servicio/salon';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { IonicPage, NavController, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-reservaproducto',
  templateUrl: 'reservaproducto.html',
  providers:[Salon, GlobalVars, Principal]
})
export class ReservaproductoPage {
  salonid: any;
  usuario = "";
  fecha   = "";
  fecha2  = "";
  hora    = "";
  hora2:any   = "";

  servicio1  = '0';
  servicio2  = '0';
  servicio3  = '0';
  servicio4  = '0';
  servicio5  = '0';
  servicio6  = '0';
  servicio7  = '0';
  servicio8  = '0';
  servicio9  = '0';
  servicio10 = '0';
  i          = 0;
  button     = "disabled";

  public loading:Loading;
  public loading2:Loading;
  public datos: any;
  public contentString = "";
  public contentStringT = "";
public contador:number = 0;
public valor:number = 0;
public result:any = 0;

  public ver      = 0;
  public razon_social = "";
  public email        = "";
  public direccion    = "";
  public telefono     = "";

  public img1     = "";
  public img2     = "";
  public img3     = "";
  public urlimg2  = "";
  public urlimg   = new GlobalVars();
  public contar = 0;
  public random = Math.random();

  public itemHeight: number = 0;
  public tiposervicio:any = 0;
  public items:any = 0;

  public user_id = "";



  usuarioid = "";
  data1 = "";
  data2 = "";
  data3 = "";
  data4 = "";
  data5 = "";
  data6 = "";
  constructor(public nav: Nav,
              private _service: Principal,
              public storage: Storage,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
              private salon_service: Salon,
              public navCtrl: NavController,
              private navController: NavController,
              public navParams: NavParams) {

    this.items = [
      { expanded: false },
      { expanded: false },
      { expanded: false },
      { expanded: false },
      { expanded: false },
      { expanded: false },
      { expanded: false },
      { expanded: false },
      { expanded: false }
    ];
  }//fin constructor
  expandItem(item){
      this.tiposervicio.map((listItem) => {
          if(item == listItem){
              listItem.expanded = !listItem.expanded;
          } else {
              listItem.expanded = false;
          }
          return listItem;
      });
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  habilita(p2){
    var input    = document.getElementById('checkbox'+this.random+p2).getElementsByTagName('button')[0].getAttribute('aria-checked');
    console.log(input);
    if(input=="false"){
              if(this.servicio1=='0'){  this.servicio1  = p2;
        }else if(this.servicio2=='0'){  this.servicio2  = p2;
        }else if(this.servicio3=='0'){  this.servicio3  = p2;
        }else if(this.servicio4=='0'){  this.servicio4  = p2;
        }else if(this.servicio5=='0'){  this.servicio5  = p2;
        }else if(this.servicio6=='0'){  this.servicio6  = p2;
        }else if(this.servicio7=='0'){  this.servicio7  = p2;
        }else if(this.servicio8=='0'){  this.servicio8  = p2;
        }else if(this.servicio9=='0'){  this.servicio9  = p2;
        }else if(this.servicio10=='0'){ this.servicio10 = p2;
        }
    }else{
              if(this.servicio1==p2){  this.servicio1  = '0';
        }else if(this.servicio2==p2){  this.servicio2  = '0';
        }else if(this.servicio3==p2){  this.servicio3  = '0';
        }else if(this.servicio4==p2){  this.servicio4  = '0';
        }else if(this.servicio5==p2){  this.servicio5  = '0';
        }else if(this.servicio6==p2){  this.servicio6  = '0';
        }else if(this.servicio7==p2){  this.servicio7  = '0';
        }else if(this.servicio8==p2){  this.servicio8  = '0';
        }else if(this.servicio9==p2){  this.servicio9  = '0';
        }else if(this.servicio10==p2){ this.servicio10 = '0';
        }
    }//fin else
    /////////////////////////////////////////////////////comprobar boton
    this.contar = 0;
              if(this.servicio1!='0'){  this.contar = 1;
        }else if(this.servicio2!='0'){  this.contar = 1;
        }else if(this.servicio3!='0'){  this.contar = 1;
        }else if(this.servicio4!='0'){  this.contar = 1;
        }else if(this.servicio5!='0'){  this.contar = 1;
        }else if(this.servicio6!='0'){  this.contar = 1;
        }else if(this.servicio7!='0'){  this.contar = 1;
        }else if(this.servicio8!='0'){  this.contar = 1;
        }else if(this.servicio9!='0'){  this.contar = 1;
        }else if(this.servicio10!='0'){ this.contar = 1;
        }
        if(this.contar==0){
              this.button   = "disabled";
        }else{
              this.button   = "";
        }
  }//fin function
  comentario(){
    this.nav.push("ComentarioPage", { id : this.salonid});
  }
  setHora2(){
    var current_date = new Date();
    var mifecha = this.fecha.split('-');
    var mihora = this.hora.split(':');

    current_date.setFullYear(parseInt(mifecha[0]),parseInt(mifecha[1]),parseInt(mifecha[2]));
    if(mihora[0]){
      current_date.setHours(parseInt(mihora[0]),parseInt(mihora[1]),0);
    }

    this.hora2 = current_date;
  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
            this.ver = this.navParams.get('id');
            if(this.ver!=null){
              this.storage.set('salonid', this.navParams.get('id'));
              localStorage.setItem('salonid',this.navParams.get('id'));

              this.storage.set('fechareserva', this.navParams.get('f'));
              localStorage.setItem('fechareserva',this.navParams.get('f'));

              this.storage.set('hora', this.navParams.get('h'));
              localStorage.setItem('hora',this.navParams.get('h'));
            }
            this.salonid    = localStorage.getItem('salonid');
            this.fecha      = localStorage.getItem('fechareserva');
            this.hora       = localStorage.getItem('hora');
            this.setHora2();
            //this.hora2      = this.fecha+' '+this.hora+':00';
            this.usuarioid  = localStorage.getItem('ID');
            this._service.productosreservas(this.usuarioid, this.salonid).subscribe((response) => {
                    this.loading2.dismiss().then( () => {
                          if (response.code!=200){

                          }else{
                            this.data1 = response.datos1.cantida_servicio;
                            this.data2 = response.datos2;
                            this.data3 = response.datos3;
                            this.data4 = response.datos4;
                            this.data5 = response.datos5['0'];
                            this.data6 = response.datos6;
                            console.log(this.data6);
                            console.log(this.data5);
                            console.log(this.data4);
                            this.loadpage();

                          }
                    });
            },error => {
                      let alerta = this.alertCtrl.create({
                            title: "Aviso",
                            message: "Disculpe, no se pudo conectar al servidor",
                            buttons: [
                              {
                                  text: "Reintentar",
                                  role: 'cancel',
                                  cssClass:'ion-aceptar',
                                  handler: data => {
                                    this.loading2.dismiss().then( () => {this.nav.pop();});
                                  }
                              }
                            ]
                          });
                          alerta.present();
            });//FIN POST
            this.loading2 = this.loadingCtrl.create({
              dismissOnPageChange: false,
              content: 'Cargando...'
            });
            this.loading2.present();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        }
  }
  continuar(){
    this.nav.push("ReservafacturaPage", { id : this.salonid,
                                            f : this.fecha,
                                            h : this.hora,
                                            s1  : this.servicio1,
                                            s2  : this.servicio2,
                                            s3  : this.servicio3,
                                            s4  : this.servicio4,
                                            s5  : this.servicio5,
                                            s6  : this.servicio6,
                                            s7  : this.servicio7,
                                            s8  : this.servicio8,
                                            s9  : this.servicio9,
                                            s10 : this.servicio10,
                                         });
  }
  anterior(){
    /*this.nav.push("ReservahoraPage", { id : this.salonid,
                                         f : this.fecha,
                               });*/
    this.navController.remove(this.navController.getActive().index);
  }
  loadpage(){
   console.log(this.salonid);
   this.user_id = localStorage.getItem('ID');
      this.salon_service.Datos(this.salonid, this.user_id).subscribe((response) => {
                 this.loading.dismiss().then( () => {
                        if (response.code==200){
                                this.datos        = response['datos'];
                                this.tiposervicio = response['datos'][0].servicios[0].deno_tiposer;
                                this.razon_social = this.datos[0]['razon_social'];
                                this.email        = this.datos[0]['email'];
                                this.direccion    = this.datos[0]['direccion'];
                                this.telefono     = this.datos[0]['telefono'];

                                if(this.datos[0]['foto1']==""){this.datos[0]['foto1']="upload/default.png";}
                                if(this.datos[0]['foto2']==""){this.datos[0]['foto2']="upload/default.png";}
                                if(this.datos[0]['foto3']==""){this.datos[0]['foto3']="upload/default.png";}

                                this.img1     = this.datos[0]['foto1'];
                                this.img2     = this.datos[0]['foto2'];
                                this.img3     = this.datos[0]['foto3'];
                                this.urlimg2  = this.urlimg.getMyserve();


                          }else{

                          }//Fin else
                });
      },error => {
                let alerta = this.alertCtrl.create({
                      title: "Aviso",
                      message: "Disculpe, no se pudo conectar al servidor",
                      buttons: [
                        {
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                              this.loading.dismiss().then( () => {this.nav.pop();});
                            }
                        }
                      ]
                    });
                    alerta.present();
      });//FIN POST
      this.loading = this.loadingCtrl.create({
          dismissOnPageChange: false,
          content: 'Cargando...'
      });
      this.loading.present();

  }

}//fin componente
