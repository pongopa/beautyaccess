import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReservaproductoPage } from './reservaproducto';
import { ComponentsExpandableComponent2 } from "../../components/components-expandable2/components-expandable2";
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    ReservaproductoPage,
    ComponentsExpandableComponent2,
  ],
  imports: [
    IonicPageModule.forChild(ReservaproductoPage),
    MyNavbarModule
  ],
  exports: [
    ReservaproductoPage
  ]
})
export class ReservaproductoPageModule {}
