import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Salon } from '../../providers/servicio/salon';
import { Reservas } from '../../providers/servicio/reservas';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Events } from 'ionic-angular';
import { IonicPage, NavController, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-reservamodificarfecha',
  templateUrl: 'reservamodificarfecha.html',
  providers:[Salon, GlobalVars, Reservas]
})
export class ReservamodificarfechaPage {

@Input()
  ver_fecha: number = 0;

salonid: any;
usuario = "";
//f       = ""
//fecha   = "";
//h       = "";
//hora    = "";
end     = "";
mesa    = 0;
mesb    = "";
diaa    = 0;
diab    = "";

horaa   = 0;
horab   = "";
minta   = 0;
mintb   = "";

h     = new Date();
hora  = "";
hora2:any = "";
f     = new Date();
fecha = "";
idreserva = "";

public loading:Loading;
public loading2:Loading;
public loading3:Loading;
public datos: any;
public contentString = "";
public contentStringT = "";
public contador:number = 0;
public valor:number = 0;
public result:any = 0;
public ver      = 0;

public razon_social = "";
public email        = "";
public direccion    = "";
public telefono     = "";

public punto = "";

public img1     = "";
public img2     = "";
public img3     = "";
public urlimg2  = "";
public urlimg   = new GlobalVars();
public disabled = 2;
public fecha_:any = [];

public user_id = "";

constructor(public nav: Nav,
              private _service: Salon,
               public alertCtrl: AlertController,
              public storage: Storage,
              public loadingCtrl: LoadingController,
              private navController: NavController,
              public navParams: NavParams,
              public ser_reserva: Reservas,
              private events: Events) {


 }/** Returns a value as a string value appropriate to the host environment's current locale. */
    /** Used by the JSON.stringify method to enable the transformation of an object's data for JavaScript Object Notation (JSON) serialization. */
  
  onDaySelect(event){
    console.log(event);

      this.fecha = event.year+'-'+(eval(event.month+1))+'-'+event.date;

      var x=new Date();
      this.fecha_ = this.fecha.split("-");
      x.setFullYear(this.fecha_[0],this.fecha_[1]-1,this.fecha_[2]);
      var today = new Date();
      if (x < today){

            let alert = this.alertCtrl.create({
              title: "Validación",
              message: "La fecha es menor al dia actual",
              buttons: [
                {
                  text: 'Aceptar',
                  cssClass:'ion-aceptar',
                  handler: data => {
                  }
                }
            ]
            });
            alert.present();
            this.disabled = 2;
      }else{
            this._service.validar_fecha(this.salonid, this.fecha).subscribe((response) => {
              this.loading2.dismiss().then( () => {
                            if (response.code==200){
                                        this.disabled = 1;
                            }else{

                                         let alert = this.alertCtrl.create({
                                            title: "Validación",
                                            message: "La fecha seleccionada no se encuentra disponible, intente de nuevamente!",
                                            buttons: [
                                              {
                                                text: 'Aceptar',
                                                cssClass:'ion-aceptar',
                                                role: 'cancel',
                                                handler: data => {
                                                  this.disabled = 2;
                                                }
                                              }
                                          ]
                                          });
                                          alert.present();

                              }
                });
            },error => {
                    let alerta = this.alertCtrl.create({
                          title: "Aviso",
                          message: "Disculpe, no se pudo conectar al servidor",
                          buttons: [
                            {
                                text: "Reintentar",
                                role: 'cancel',
                                cssClass:'ion-aceptar',
                                handler: data => {
                                  this.loading2.dismiss().then( () => {this.nav.pop();});
                                }
                            }
                          ]
                        });
                        alerta.present();
            });//FIN POST
            this.loading2 = this.loadingCtrl.create({
                dismissOnPageChange: false,
                content: 'Cargando...'
            });
            this.loading2.present();

      }
    console.log(this.fecha );
  }

  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  comentario(){
    this.nav.push("ComentarioPage", { id : this.salonid});
  }
  setHora2(){
    var current_date = new Date();
    var mifecha = this.fecha.split('-');
    var mihora = this.hora.split(':');

    current_date.setFullYear(parseInt(mifecha[0]),parseInt(mifecha[1]),parseInt(mifecha[2]));
    if(mihora[0]){
      current_date.setHours(parseInt(mihora[0]),parseInt(mihora[1]),0);
    }

    this.hora2 = current_date;
  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.ver = this.navParams.get('id');

            if(this.ver!=null){
              this.storage.set('salonid', this.navParams.get('id'));
              localStorage.setItem('salonid',this.navParams.get('id'));

              this.storage.set('idreserva',    this.navParams.get('idreserva'));
              localStorage.setItem('idreserva',this.navParams.get('idreserva'));

              this.storage.set('punto',    this.navParams.get('punto'));
              localStorage.setItem('punto',this.navParams.get('punto'));
            }

            this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
            this.salonid   = localStorage.getItem('salonid');
            this.idreserva = localStorage.getItem('idreserva');
            this.punto     = localStorage.getItem('punto');

            this.mesa = (this.f.getMonth()+1);
            if(this.mesa<=9){ this.mesb = '0'+this.mesa;}else{this.mesb = ''+this.mesa;}
            this.diaa = this.f.getDate();
            if(this.diaa<=9){ this.diab = '0'+this.diaa;}else{this.diab = ''+this.diaa;}
            this.fecha = this.f.getFullYear()+"-"+this.mesb+"-"+this.diab;

            this.horaa = this.h.getHours();
            if(this.horaa<=9){ this.horab = '0'+this.horaa;}else{this.horab = ''+this.horaa;}
            this.minta = this.h.getMinutes();
            if(this.minta<=9){ this.mintb = '0'+this.minta;}else{this.mintb = ''+this.minta;}
             this.hora = this.horab+":"+this.mintb;
             this.setHora2();
             //this.hora2      = this.fecha+' '+this.hora+':00';
             this.loadpage();

        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        }
  }
  continuar(){
    this.nav.push("ReservamodificarhoraPage", { id : this.salonid,
                                                f : this.fecha,
                                                idreserva: this.idreserva
                                      });
  }
  modificar(){
            this.user_id = localStorage.getItem('ID');
            this.ser_reserva.reservasupdatefecha(this.idreserva, this.user_id, this.fecha).subscribe((response) => {
                  this.loading2.dismiss().then( () => {
                     let alert = this.alertCtrl.create({
                          title: "Aviso",
                          message: "Los datos fueron modificados",
                          buttons: [
                            {
                              text: 'Aceptar',
                              cssClass:'ion-aceptar',
                              role: 'cancel',
                              handler: data => {
                                  this.nav.pop();
                              }
                            }
                          ]
                      });
                      alert.present();
                  });
            },error => {
                    let alerta = this.alertCtrl.create({
                          title: "Aviso",
                          message: "Disculpe, no se pudo conectar al servidor",
                          buttons: [
                            {
                                text: "Reintentar",
                                role: 'cancel',
                                cssClass:'ion-aceptar',
                                handler: data => {
                                  this.loading2.dismiss().then( () => {
                                      //this.nav.pop();
                                  });
                                }
                            }
                          ]
                        });
                        alerta.present();
            });//FIN POST
            this.loading2 = this.loadingCtrl.create({
                dismissOnPageChange: false,
                content: 'Cargando...'
            });
            this.loading2.present();
  }
  anterior(){
    this.nav.pop();
  }
  loadpage(){
   console.log(this.salonid);
   this.user_id = localStorage.getItem('ID');
      this._service.Datos(this.salonid, this.user_id).subscribe((response) => {
            this.loading3.dismiss().then( () => {
                      if (response.code2==200){
                                          this._service.validar_fecha(this.salonid, this.fecha).subscribe((response_fecha) => {
                                                    this.loading.dismiss().then( () => {
                                                                if (response_fecha.code==200){
                                                                            this.disabled = 1;
                                                                }else{

                                                                             let alert_fecha = this.alertCtrl.create({
                                                                                title: "Validación",
                                                                                message: "La fecha seleccionada no se encuentra disponible, intente de nuevamente!",
                                                                                buttons: [
                                                                                  {
                                                                                    text: 'Aceptar',
                                                                                    cssClass:'ion-aceptar',
                                                                                    role: 'cancel',
                                                                                    handler: data => {
                                                                                      this.disabled = 2;
                                                                                    }
                                                                                  }
                                                                              ]
                                                                              });
                                                                              alert_fecha.present();

                                                                  }
                                                    });
                                          },error => {
                                                    let alerta = this.alertCtrl.create({
                                                          title: "Aviso",
                                                          message: "Disculpe, no se pudo conectar al servidor",
                                                          buttons: [
                                                            {
                                                                text: "Reintentar",
                                                                role: 'cancel',
                                                                cssClass:'ion-aceptar',
                                                                handler: data => {
                                                                  this.loading.dismiss().then( () => {this.nav.pop();});
                                                                }
                                                            }
                                                          ]
                                                        });
                                                        alerta.present();
                                          });//FIN POST
                                          this.loading = this.loadingCtrl.create({
                                              dismissOnPageChange: false,
                                              content: 'Cargando...'
                                          });
                                          this.loading.present();
                                        this.datos        = response['datos'];

                                        this.razon_social = this.datos[0]['razon_social'];
                                        this.email        = this.datos[0]['email'];
                                        this.direccion    = this.datos[0]['direccion'];
                                        this.telefono     = this.datos[0]['telefono'];

                                        if(this.datos[0]['foto1']==""){this.datos[0]['foto1']="upload/default.png";}
                                        if(this.datos[0]['foto2']==""){this.datos[0]['foto2']="upload/default.png";}
                                        if(this.datos[0]['foto3']==""){this.datos[0]['foto3']="upload/default.png";}

                                        this.img1     = this.datos[0]['foto1'];
                                        this.img2     = this.datos[0]['foto2'];
                                        this.img3     = this.datos[0]['foto3'];
                                        this.urlimg2  = this.urlimg.getMyserve();


                                        console.log(response['datos']);
                                        console.log(response['datos2']);
                                        console.log(this.razon_social);

                    }else if (response.code2==201){
                              let alert_fecha = this.alertCtrl.create({
                              title: "¡Alerta BeautyAccess!",
                              message: "Debe completar su perfil antes de continuar",
                              buttons: [
                                {
                                      text: 'Aceptar',
                                      cssClass:'ion-aceptar',
                                      role: 'cancel',
                                      handler: data => { this.nav.push('PerfilPage');}
                                }
                            ]
                            });
                            alert_fecha.present();
                    }else{

                    }//Fin else
            });//fin this.loading
      },error => {
                let alerta = this.alertCtrl.create({
                      title: "Aviso",
                      message: "Disculpe, no se pudo conectar al servidor",
                      buttons: [
                        {
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                              this.loading3.dismiss().then( () => {this.nav.pop();});
                            }
                        }
                      ]
                    });
                    alerta.present();
        });//FIN POST
      this.loading3 = this.loadingCtrl.create({
        dismissOnPageChange: false,
        content: 'Cargando...'
      });
      this.loading3.present();
  }
}//fin componente
