import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReservamodificarfechaPage } from './reservamodificarfecha';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

import { CalendarModule } from 'ionic3-calendar-en';

@NgModule({
  declarations: [
    ReservamodificarfechaPage
  ],
  imports: [
    IonicPageModule.forChild(ReservamodificarfechaPage),
    MyNavbarModule,
    CalendarModule
  ],
  exports: [
    ReservamodificarfechaPage
  ]
})
export class ReservamodificarfechaPageModule {}
