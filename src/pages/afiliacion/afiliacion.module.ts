import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AfiliacionPage } from './afiliacion';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    AfiliacionPage
  ],
  imports: [
    IonicPageModule.forChild(AfiliacionPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    AfiliacionPage
  ]
})
export class AfiliacionPageModule {}
