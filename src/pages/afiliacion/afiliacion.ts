import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Afiliacion } from '../../providers/servicio/afiliacion';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Ionic2RatingModule } from 'ionic2-rating';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-afiliacion',
  templateUrl: 'afiliacion.html',
  providers:[Afiliacion, GlobalVars]
})
export class AfiliacionPage {
    usuario = "";
    usuarioid = "";
    historiaid = "";
    public loading:Loading;
    fecha = "";
    dat  = 0;
    servicio_mes = 0;
    promocionid = 0;
    searchTerm:any="";
    allItems:any;
    items:any="";
    @Input() data: any;
    @Input() data2: any;
    @Input() data3: any;
    @Input() events: any;
    @ViewChild('wizardSlider') slider: Slides;
    sliderOptions = { pager: true };
    path:boolean = false;
    prev:boolean = true;
    next:boolean = true;
    finish:boolean = true;


    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    constructor(
              public nav: Nav, 
              public navCtrl: NavController,
              private navController: NavController,  
              public service: Afiliacion,
              public storage: Storage,
              public loadingCtrl: LoadingController,
              public navParams: NavParams) {

  }
  cargar(v, c){ 
   this.nav.push(v, { id : c});
  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario     = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid   = localStorage.getItem('ID');
            this.urlimg2     = this.urlimg.getMyserve();
            this.service.afiliacion().subscribe((response) => { 
              this.loading.dismiss().then( () => {
                      if (response.code!=200){

                      }else{
                            this.data2 = response.datos;//publicidad
                            this.data3 = response.datos;//publicidad
                            this.servicio_mes = response.servicio_mes;//publicidad
                            console.log(this.data3);
                            console.log(this.data2);

                      }
                  });
              });
              this.loading = this.loadingCtrl.create({
                  dismissOnPageChange: false,
                  content: 'Cargando... '
              });
              this.loading.present();
            this.data  = this.getDataForLayout1();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  getDataForLayout1 = (): any => {
        return {
            "toolBarTitle":"Simple + icon",
            "btnPrev":"<",
            "btnNext":">",
            "btnFinish":"Finish",
            "items":[
               {
                  "logo":"",
                  "iconSlider":"./assets/images/6-imagen2-03.png",
                  "title":"Fragment Example 1",
                  "buttonNext":">"
               },
               {
                  "logo":"",
                  "iconSlider":"./assets/images/6-imagen2-03.png",
                  "title":"Fragment Example 2",
                  "description":"Text for Fragment Example 2 Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                  "buttonNext":">",
                  "buttonPrevious":"<"
               },
               {
                  "logo":"",
                  "iconSlider":"./assets/images/6-imagen2-03.png",
                  "title":"Fragment Example 3",
                  "buttonPrevious":"<"
               }
            ]
         };
  };
  changeSlide(index: number): void {
        if (index > 0) {
            this.slider.slideNext(300);
        } else {
            this.slider.slidePrev(300);
        }
  }
  slideHasChanged(index: number): void {
        try {
            this.prev = !this.slider.isBeginning();
            this.next = this.slider.getActiveIndex() < (this.slider.length() - 1);
            this.finish = this.slider.isEnd();
        } catch (e) { }
  }
}//fin componente 
