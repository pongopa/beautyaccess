import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecargastargetasPage } from './recargastargetas';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    RecargastargetasPage
  ],
  imports: [
    IonicPageModule.forChild(RecargastargetasPage),
    MyNavbarModule
  ],
  exports: [
    RecargastargetasPage
  ]
})
export class RecargastargetasPageModule {}
