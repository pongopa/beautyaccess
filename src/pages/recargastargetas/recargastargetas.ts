import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Perfil } from '../../providers/servicio/perfil';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser';
import { IonicPage, NavController, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-recargastargetas',
  templateUrl: 'recargastargetas.html',
  providers:[Perfil, GlobalVars]
})
export class RecargastargetasPage {
  salonid: any;
  usuario = "";
  fecha   = "";
  fecha2  = "";
  hora    = "";
  public option3: any;
  public options2: ThemeableBrowserOptions = {
       location : 'no',
       fullscreen : 'yes',
        statusbar: {
           color: '#ffffffff'
       },
       toolbar: {
           height: 0,
           color: '#f0f0f0ff',
       },
       backButtonCanClose: true
  };
  public options : InAppBrowserOptions = {
      location : 'no',//Or 'no' 
      /*hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only */ 
      fullscreen : 'yes',//Windows only    
  };
  myForm: FormGroup;


  public loading:Loading;
  public loading2:Loading;
  public data: any;
  public url_ser  = new GlobalVars();
  public url_ser_ = "";
  public ser:any =  [];
  public datos = "";
  public predefinido = "";


  usuarioid = "";

  constructor(public platform: Platform,
              public nav: Nav,
              private _service: Perfil,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public formBuilder: FormBuilder,
              public navCtrl: NavController, 
              private navController: NavController, 
              public  alertCtrl: AlertController,
              public iab: InAppBrowser,
              public themeableBrowser: ThemeableBrowser,
              public navParams: NavParams) {

  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){ 
     this.nav.push("RecargastargetashistoriasPage");
  }//fin function
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){

            this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid = localStorage.getItem('ID');
            this._service.Suscripcionlist(this.usuarioid).subscribe((response) => { 
                this.loading2.dismiss().then( () => {
                     this.datos = response.datos;
                     this.predefinido = response.predefinido;
                     console.log(this.datos);
                });
            },error => {
                    let alerta = this.alertCtrl.create({
                          title: "Aviso",
                          message: "Disculpe, no se pudo conectar al servidor",
                          buttons: [
                            {
                                text: "Reintentar",
                                role: 'cancel',
                                cssClass:'ion-aceptar',
                                handler: data => {
                                  this.loading2.dismiss().then( () => {this.nav.pop();});
                                }
                            }
                          ]
                        });
                        alerta.present();
            });//FIN POST
            this.loading2 = this.loadingCtrl.create({
              dismissOnPageChange: false,
            });
            this.loading2.present();
        }else{ 
            this.storage.remove('ID');
            this.storage.remove('USERNAME');  
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }//fin function

  definir(v){
            this.usuarioid = localStorage.getItem('ID');
                let alert = this.alertCtrl.create({
                  title: "Confirmar",
                  message: "Que opción desea realizar",
                  buttons: [
                    {
                      text: 'Eliminar',
                      cssClass:'ion-cancelar',
                      handler: data => {
                        //AQUI REGRESAR
                              this._service.Suscripciondelete(this.predefinido, v, this.usuarioid).subscribe((response) => { 
                                 this.loading.dismiss().then( () => {
                                    this.predefinido = response.predefinido;
                                    this.datos  = response.datos;
                                });
                              },error => {
                                      let alerta = this.alertCtrl.create({
                                            title: "Aviso",
                                            message: "Disculpe, no se pudo conectar al servidor",
                                            buttons: [
                                              {
                                                  text: "Reintentar",
                                                  role: 'cancel',
                                                  cssClass:'ion-aceptar',
                                                  handler: data => {
                                                    this.loading.dismiss().then( () => {this.nav.pop();});
                                                  }
                                              }
                                            ]
                                          });
                                          alerta.present();
                              });//FIN POST
                              this.loading = this.loadingCtrl.create({
                                dismissOnPageChange: false,
                              });
                              this.loading.present();
                      }
                    },
                    {
                      text: 'Predefinido',
                      cssClass:'ion-aceptar',
                      handler: data => {
                        //AQUI REGRESAR
                            this._service.Suscripcionupdate(this.predefinido, v, this.usuarioid).subscribe((response) => { 
                               this.loading.dismiss().then( () => {
                                  this.predefinido = v;
                                  this.datos  = response.datos;
                              });
                            },error => {
                                    let alerta = this.alertCtrl.create({
                                          title: "Aviso",
                                          message: "Disculpe, no se pudo conectar al servidor",
                                          buttons: [
                                            {
                                                text: "Reintentar",
                                                role: 'cancel',
                                                cssClass:'ion-aceptar',
                                                handler: data => {
                                                  this.loading.dismiss().then( () => {this.nav.pop();});
                                                }
                                            }
                                          ]
                                        });
                                        alerta.present();
                            });//FIN POST
                            this.loading = this.loadingCtrl.create({
                              dismissOnPageChange: false,
                            });
                            this.loading.present();
                      }
                    }
                  ]
                });
                alert.present(); 
  }//fin function
  add_pago(){
       this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid = localStorage.getItem('ID');
            this._service.Perfilsuscripcion(this.usuarioid).subscribe((response) => { 
                  this.loading.dismiss().then( () => {
                                if (response.code==200){
                                    //const browser = this.iab.create(response.processUrl,"_self", this.options);
                                    //browser.on('loadstart').subscribe((response2) => {
                                    //this.url_ser_ = this.url_ser.getMyGlobalVar()+'update_transaccion_procesar';
                                    //if(response2.url==this.url_ser_){
                                    //this.platform.ready().then((readySource) => {
                                    //const browser: ThemeableBrowserObject = this.themeableBrowser.create(response.processUrl, '_self', this.options2);
                                    /*browser.on('helloPressed').subscribe(data => {console.log('1');});
                                    browser.on('backPressed').subscribe(data => { console.log('2');});
                                    browser.on('sharePressed').subscribe(data => {console.log('3');});
                                    browser.on('openPressed').subscribe(data => { console.log('4');});
                                    browser.on('loadstart').subscribe(data => {   console.log('5');});
                                    browser.on('loadstart').subscribe(data => { 

                                      console.log('6'+JSON.stringify({data})); 

                                    });*/
                                    const browser: ThemeableBrowserObject = this.themeableBrowser.create(response.processUrl, '_blank', this.options2);
                                    browser.on('loadstart').subscribe(response2 => {
                                    this.url_ser_ = this.url_ser.getMyGlobalVar()+'update_transaccion_procesar';
                                              if(response2['url']==this.url_ser_){
                                                  browser.close();
                                                  //browser.executeScript({code:  "   window.location.href = google.com ;"  });
                                                  this._service.suscripcion_placetoplay(this.usuarioid, response.requestId).subscribe((response3) => {  
                                                        this.loading.dismiss().then( () => {
                                                                 if (response3.code==200){
                                                                                  let alert = this.alertCtrl.create({
                                                                                    title: response3.msgh,
                                                                                    message: response3.pasarela_message,
                                                                                    buttons: [
                                                                                      {
                                                                                        text: 'Aceptar',
                                                                                        role: 'cancel',
                                                                                        cssClass:'ion-aceptar',
                                                                                        handler: data => {
                                                                                          //AQUI REGRESAR
                                                                                            //this.navController.remove(this.navController.getActive().index);
                                                                                            this.predefinido = response3.predefinido;
                                                                                            this.datos  = response3.datos;
                                                                                        }
                                                                                      }
                                                                                    ]
                                                                                  });
                                                                                  alert.present();
                                                                  }else{
                                                                                  let alert = this.alertCtrl.create({
                                                                                    title: response3.msgh,
                                                                                    message: response3.pasarela_message,
                                                                                    buttons: [
                                                                                      {
                                                                                        text: 'Aceptar',
                                                                                        role: 'cancel',
                                                                                        cssClass:'ion-cancelar',
                                                                                        handler: data => {
                                                                                          //AQUI REGRESAR
                                                                                            //this.navController.remove(this.navController.getActive().index);
                                                                                            this.predefinido = response3.predefinido;
                                                                                            this.datos       = response3.datos;
                                                                                        }
                                                                                      },
                                                                                      {
                                                                                        text: 'Reintentar',
                                                                                        role: 'cancel',
                                                                                        cssClass:'ion-aceptar',
                                                                                        handler: data => {
                                                                                          //AQUI REGRESAR
                                                                                            //this.navController.remove(this.navController.getActive().index);
                                                                                            this.predefinido = response3.predefinido;
                                                                                            this.datos  = response3.datos;
                                                                                            this.add_pago();
                                                                                        }
                                                                                      }
                                                                                    ]
                                                                                  });
                                                                                  alert.present();
                                                                  }//fin else
                                                      });//FIN LOADING
                                                  },error => {
                                                          let alerta = this.alertCtrl.create({
                                                                title: "Aviso",
                                                                message: "Disculpe, no se pudo conectar al servidor",
                                                                buttons: [
                                                                  {
                                                                      text: "Reintentar",
                                                                      role: 'cancel',
                                                                      cssClass:'ion-aceptar',
                                                                      handler: data => {
                                                                        this.loading.dismiss().then( () => {this.nav.pop();});
                                                                      }
                                                                  }
                                                                ]
                                                              });
                                                              alerta.present();
                                                  });//FIN POST
                                                  this.loading = this.loadingCtrl.create({
                                                    dismissOnPageChange: false,
                                                  });
                                                  this.loading.present();
                                            }//Fin if
                                    });
                                }else{
                                          this.loading.dismiss().then( () => {
                                                let alert = this.alertCtrl.create({
                                                  title: "Suscripción de tarjeta!",
                                                  message: "La Suscripción de tarjeta no pudo ser procesada, intente de nuevo!",
                                                  buttons: [
                                                      {
                                                      text: 'Salir',
                                                      role: 'cancel',
                                                      cssClass:'ion-cancelar',
                                                      handler: data => {
                                                                //AQUI REGRESAR
                                                                this.navController.remove(this.navController.getActive().index);
                                                      }
                                                    }
                                                ]
                                                });
                                                alert.present();
                                            });
                                  }//Fin else
                  });//fin loading
            },error => {
                    let alerta = this.alertCtrl.create({
                          title: "Aviso",
                          message: "Disculpe, no se pudo conectar al servidor",
                          buttons: [
                            {
                                text: "Reintentar",
                                role: 'cancel',
                                cssClass:'ion-aceptar',
                                handler: data => {
                                  this.loading.dismiss().then( () => {this.nav.pop();});
                                }
                            }
                          ]
                        });
                        alerta.present();
            });//FIN POST
            this.loading = this.loadingCtrl.create({
                dismissOnPageChange: false,
              });
            this.loading.present();

  }


  update(){
      console.log(this.myForm.value);
      let alert = this.alertCtrl.create({
          title: "¿Está seguro que desea actualizar?",
          message:"&nbsp;&nbsp; Los datos de usuarios van a ser modificados.",
          buttons: [
                {
                  text: 'No',
                  role: 'cancel',
                  cssClass:'ion-cancelar',
                  handler: data => {

                  }
                },
                {
                  text: 'Si',
                  cssClass:'ion-aceptar',
                  handler: data => {
                      this._service.Perfilupdate(this.usuarioid, this.myForm.value).subscribe((response) => { 
                                    let alert = this.alertCtrl.create({
                                    title: "Modificación Perfil",
                                    message:"&nbsp;&nbsp; Sus datos fueron modificados con éxito",
                                    buttons: [
                                          {
                                            text: 'Aceptar',
                                            cssClass:'ion-aceptar',
                                            handler: data => {
                                              localStorage.setItem('NOMBRESAPELLIDOS', this.myForm.value.nombres+' '+this.myForm.value.apellidos)
                                              //this.navController.pop();
                                              this.navController.setRoot('InicioPage');
                                            }
                                          }
                                        ],
                                    });
                                    alert.present();
                        },error => {
                                  let alerta = this.alertCtrl.create({
                                        title: "Aviso",
                                        message: "Disculpe, no se pudo conectar al servidor",
                                        buttons: [
                                          {
                                              text: "Reintentar",
                                              role: 'cancel',
                                              cssClass:'ion-aceptar',
                                              handler: data => {
                                                this.loading.dismiss().then( () => {this.nav.pop();});
                                              }
                                          }
                                        ]
                                      });
                                      alerta.present();
                          });//FIN POST
                  }
                }
              ],
      });
      alert.present();
  }//fin function

}//fin componente 
