import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Canjes } from '../../providers/servicio/canjes';
import { Perfil } from '../../providers/servicio/perfil';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Ionic2RatingModule } from 'ionic2-rating';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
     selector: 'page-canjes',
  templateUrl: 'canjes.html',
  providers:[Perfil, Canjes, GlobalVars, Principal]
})
export class CanjesPage {
    @Input() data2: any;
    @Input() data3: any;
    @Input() events: any;
    @ViewChild('wizardSlider') slider: Slides;
    sliderOptions = { pager: true };
    path:boolean = false;
    prev:boolean = true;
    next:boolean = true;
    finish:boolean = true;
    
    usuario = "";
    usuarioid = "";
    historiaid = "";
    data  = "";
    fecha = "";
    dat  = 0;
    mensaje = "";

    public loading:Loading;

    searchTerm:any="";
    allItems:any;
    items:any="";
    public option3: any;
    public options2: ThemeableBrowserOptions = {
       location : 'no',
       fullscreen : 'yes',
        statusbar: {
           color: '#ffffffff'
       },
       toolbar: {
           height: 0,
           color: '#f0f0f0ff',
       },
       backButtonCanClose: true
    };
    public options : InAppBrowserOptions = {
      location : 'no',//Or 'no' 
      /*hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only */ 
      fullscreen : 'yes',//Windows only    
    };
    public url_ser  = new GlobalVars();
    public url_ser_ = "";
    public urlimg   = new GlobalVars();
    public urlimg2  = "";

    public url_ser_perfil  = new GlobalVars();
    public url_ser__perfil = "";
    constructor(
              public nav: Nav, 
              public navCtrl: NavController, 
              private service_perfil: Perfil,
              public alertCtrl: AlertController, 
              public service: Canjes,
              public themeableBrowser: ThemeableBrowser,
              public iab: InAppBrowser,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              private navController: NavController, 
              public navParams: NavParams) {

      this.prev = false;
      this.next = true;
      this.finish = false;

  }
  cargar(v, c){
   this.nav.push(v, { id : c});
  }
  canjear(v, d){
    if(v!=""){ 
           this.mensaje ="&nbsp;&nbsp; "+d;
           let alert_ = this.alertCtrl.create({
              title: "¿Está seguro de canjear?",
              message:this.mensaje,
              buttons: [
                    {
                      text: 'Cancelar',
                      role: 'cancel',
                      cssClass:'ion-cancelar',
                      handler: data => {
                              console.log("cancelar");
                      }
                    },
                    {
                      text: 'Aceptar',
                      cssClass:'ion-aceptar',
                      handler: data => { 
                                this.usuarioid = localStorage.getItem('ID');
                                this.service.save(v, this.usuarioid).subscribe((response) => { 
                                            if (response.code==200){
                                                                      this.loading.dismiss().then( () => {
                                                                          let alert = this.alertCtrl.create({
                                                                            title: response.msgh,
                                                                            message: response.pasarela_message,
                                                                            buttons: [
                                                                              {
                                                                                text: 'Aceptar',
                                                                                role: 'cancel',
                                                                                cssClass:'ion-aceptar',
                                                                                handler: data => {
                                                                                   // this.nav.push("CanjesdetallesPage", {});
                                                                                   this.navController.setRoot('InicioPage');
                                                                                }
                                                                              }
                                                                            ]
                                                                          });
                                                                          alert.present();
                                                                      });
                                            }else if (response.code==301){/////////////////ENVIA A CREAR SUSCRIPCIóN
                                                                      this.usuarioid = localStorage.getItem('ID');
                                                                      this.service_perfil.Perfilsuscripcion(this.usuarioid).subscribe((response_perfil) => { 
                                                                                        if (response_perfil.code==200){                                                                                                  
                                                                                                  /*const browser_perfil = this.iab.create(response_perfil.processUrl,"_blank", this.options);
                                                                                                  browser_perfil.on('loadstart').subscribe((response2_perfl) => {
                                                                                                          this.url_ser__perfil = this.url_ser_perfil.getMyGlobalVar()+'update_transaccion_procesar';
                                                                                                          if(response2_perfl.url==this.url_ser__perfil){*/

                                                                                                  const browser_perfil: ThemeableBrowserObject = this.themeableBrowser.create(response_perfil.processUrl, '_blank', this.options2);
                                                                                                  browser_perfil.on('loadstart').subscribe(response2_perfl => {
                                                                                                  this.url_ser__perfil = this.url_ser_perfil.getMyGlobalVar()+'update_transaccion_procesar';
                                                                                                            if(response2_perfl['url']==this.url_ser__perfil){

                                                                                                              
                                                                                                                this.service_perfil.suscripcion_placetoplay(this.usuarioid, response_perfil.requestId).subscribe((response3_perfil) => {  
                                                                                                                      browser_perfil.close();
                                                                                                                      this.loading.dismiss().then( () => {
                                                                                                                          let alert_perfil = this.alertCtrl.create({
                                                                                                                            title: response3_perfil.msgh,
                                                                                                                            message: response3_perfil.pasarela_message+' en breve sera procesada su solicitud',
                                                                                                                            buttons: [
                                                                                                                              {
                                                                                                                                text: 'Aceptar',
                                                                                                                                role: 'cancel',
                                                                                                                                cssClass:'ion-aceptar',
                                                                                                                                handler: data => {
                                                                                                                                                  this.navController.setRoot('InicioPage');                                                                                                                                               
                                                                                                                                    }//fin handler
                                                                                                                                }
                                                                                                                              ]
                                                                                                                            });
                                                                                                                            alert_perfil.present();
                                                                                                                        });
                                                                                                                },error => {
                                                                                                                    let alerta = this.alertCtrl.create({
                                                                                                                          title: "Aviso",
                                                                                                                          message: "Disculpe, no se pudo conectar al servidor",
                                                                                                                          buttons: [
                                                                                                                            {
                                                                                                                                text: "Reintentar",
                                                                                                                                role: 'cancel',
                                                                                                                                cssClass:'ion-aceptar',
                                                                                                                                handler: data => {
                                                                                                                                  this.loading.dismiss().then( () => {this.nav.pop();});
                                                                                                                                }
                                                                                                                            }
                                                                                                                          ]
                                                                                                                        });
                                                                                                                        alerta.present();
                                                                                                                });//FIN POST
                                                                                                        }//Fin if
                                                                                                });//fin proceso de suscripcion
                                                                                      }else{
                                                                                                    this.loading.dismiss().then( () => {
                                                                                                          let alert = this.alertCtrl.create({
                                                                                                            title: "Suscripción de tarjeta!",
                                                                                                            message: "La Suscripción de tarjeta no pudo ser procesada, intente de nuevo!",
                                                                                                            buttons: [
                                                                                                                {
                                                                                                                text: 'Salir',
                                                                                                                role: 'cancel',
                                                                                                                cssClass:'ion-cancelar',
                                                                                                                handler: data => {
                                                                                                                        this.navController.setRoot('InicioPage');
                                                                                                                          //AQUI REGRESAR
                                                                                                                         // this.navController.remove(this.navController.getActive().index);
                                                                                                                }
                                                                                                              }
                                                                                                          ]
                                                                                                          });
                                                                                                          alert.present();
                                                                                                      });
                                                                                       }//Fin else
                                                                      },error => {
                                                                            let alerta = this.alertCtrl.create({
                                                                                  title: "Aviso",
                                                                                  message: "Disculpe, no se pudo conectar al servidor",
                                                                                  buttons: [
                                                                                    {
                                                                                        text: "Reintentar",
                                                                                        role: 'cancel',
                                                                                        cssClass:'ion-aceptar',
                                                                                        handler: data => {
                                                                                          this.loading.dismiss().then( () => {this.nav.pop();});
                                                                                        }
                                                                                    }
                                                                                  ]
                                                                                });
                                                                                alerta.present();
                                                                        });//FIN POST
                                            }else if (response.code==201){
                                                    this.loading.dismiss().then( () => {
                                                        let alert = this.alertCtrl.create({
                                                          title: response.msgh,
                                                          message: response.msg,
                                                          buttons: [
                                                                      {
                                                                        text: 'Aceptar',
                                                                        role: 'cancel',
                                                                        cssClass:'ion-aceptar',
                                                                        handler: data => {
                                                                            //this.nav.push("CanjesdetallesPage", {});
                                                                            this.navController.setRoot('InicioPage');
                                                                        }
                                                                      }
                                                                    ]
                                                        });
                                                        alert.present();
                                                    });
                                            }else if (response.code==202){
                                                    this.loading.dismiss().then( () => {
                                                        let alert = this.alertCtrl.create({
                                                          title: response.msgh,
                                                          message: response.msg,
                                                          buttons: [
                                                                      {
                                                                        text: 'Aceptar',
                                                                        role: 'cancel',
                                                                        cssClass:'ion-aceptar',
                                                                        handler: data => {
                                                                            //this.nav.push("CanjesdetallesPage", {});
                                                                            this.navController.setRoot('InicioPage');
                                                                        }
                                                                      }
                                                                    ]
                                                        });
                                                        alert.present();
                                                    });
                                            }else{
                                                    this.loading.dismiss().then( () => {
                                                          let alert = this.alertCtrl.create({
                                                            title: "Respuesta canje!",
                                                            message: "El canje no pudo ser realizado.",
                                                            buttons: [
                                                                {
                                                                text: 'Salir',
                                                                role: 'cancel',
                                                                cssClass:'ion-cancelar',
                                                                handler: data => {
                                                                    //this.navController.setRoot('InicioPage');
                                                                    this.navController.setRoot('InicioPage');
                                                                }
                                                              },
                                                              {
                                                                text: 'Aceptar',
                                                                cssClass:'ion-aceptar',
                                                                handler: data => {
                                                                        this.navController.setRoot('InicioPage');
                                                                }
                                                              }
                                                          ]
                                                          });
                                                          alert.present();
                                                      });
                                            }//Fin else
                                },error => {
                                      let alerta = this.alertCtrl.create({
                                            title: "Aviso",
                                            message: "Disculpe, no se pudo conectar al servidor",
                                            buttons: [
                                              {
                                                  text: "Reintentar",
                                                  role: 'cancel',
                                                  cssClass:'ion-aceptar',
                                                  handler: data => {
                                                    this.loading.dismiss().then( () => {this.nav.pop();});
                                                  }
                                              }
                                            ]
                                          });
                                          alerta.present();
                                  });//FIN POST
                              this.loading = this.loadingCtrl.create({
                                dismissOnPageChange: false,
                              });
                              this.loading.present();
                                
                      }
                    }
                  ],
                
          });
          alert_.present();
          
          
    }//fin if
  }//fin function
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid = localStorage.getItem('ID');
            this.urlimg2   = this.urlimg.getMyserve();
            this.service.list().subscribe((response) => {
                this.loading.dismiss().then( () => {
                     if (response.code!=200){

                      }else{
                            this.data  = response.datos1;//publicidad
                            this.data2 = response.datos2;//publicidad
                            console.log(this.data2);
                      }
                });
            },error => {
                let alerta = this.alertCtrl.create({
                      title: "Aviso",
                      message: "Disculpe, no se pudo conectar al servidor",
                      buttons: [
                        {
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                              this.loading.dismiss().then( () => {this.nav.pop();});
                            }
                        }
                      ]
                    });
                    alerta.present();
            });//FIN POST
            this.loading = this.loadingCtrl.create({
                dismissOnPageChange: false,
                content: 'Cargando...'
            });
            this.loading.present();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function

  changeSlide(index: number): void {
        if (index > 0) {
            this.slider.slideNext(300);
        } else {
            this.slider.slidePrev(300);
        }
  }
  slideHasChanged(index: number): void {
        try {
            this.prev = !this.slider.isBeginning();
            this.next = this.slider.getActiveIndex() < (this.slider.length() - 1);
            this.finish = this.slider.isEnd();
        } catch (e) { }
  }
}//fin componente 
