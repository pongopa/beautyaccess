import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PerfilPage } from './perfil';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    PerfilPage
  ],
  imports: [
    IonicPageModule.forChild(PerfilPage),
    MyNavbarModule
  ],
  exports: [
    PerfilPage
  ]
})
export class PerfilPageModule {}
