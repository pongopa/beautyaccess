import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Perfil } from '../../providers/servicio/perfil';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { IonicPage, NavController, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';

import { CustomFormsModule } from 'ng2-validation';

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
  providers:[Perfil, GlobalVars]
})
export class PerfilPage {
  salonid: any;
  usuario = "";
  fecha   = "";
  fecha2  = "";
  hora    = "";

  myForm: FormGroup;


  public loading:Loading;
  public data: any;

  public opcion1 = "";
  public opcion2 = "1";


  usuarioid = "";
  constructor(public nav: Nav,
              private _service: Perfil,
              public custom: CustomFormsModule,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public formBuilder: FormBuilder,
              public navCtrl: NavController, 
              private navController: NavController, 
              public  alertCtrl: AlertController,
              public navParams: NavParams) {

        this.myForm = this.formBuilder.group({
            fecha: ['', Validators.required],
            direccion: ['', Validators.required],
            sexo: ['', Validators.required],       
            cabello: ['', Validators.required],       
            
            nombres: ['',   [Validators.pattern('[a-zA-ZáéíóúÁÉÍÓÚñÑ ]*'), Validators.required]],
            apellidos: ['', [Validators.pattern('[a-zA-ZáéíóúÁÉÍÓÚñÑ ]*'), Validators.required]],
            ci: ['',        [Validators.pattern('[0-9]*'), Validators.maxLength(11), Validators.required]],
            telefono: ['',  [Validators.pattern('[0-9]*'), Validators.maxLength(20), Validators.required]]

            //nombres: ['',   Validators.required],
            //apellidos: ['', Validators.required],
            //ci: ['',        Validators.required],
            //telefono: ['',  Validators.required]
        });
  }
  getText(e){
      var elementValue = e.srcElement.value;
      var elementId = e.srcElement;
      if(elementValue){
           var regex = /^[a-zA-Z ]+$/;   
          if (!regex.test(elementValue)) {
            var tempValue = elementValue.substring(0, elementValue.length - 1);
            console.log("Entered char is not alphabet");
            elementId.value = tempValue;
          }
      }
  }
  getNum(e){
     var elementValue = e.srcElement.value;
    if(elementValue){
       var regex = /^[0-9]+$/;   
        var tempValue = elementValue.substring(0, elementValue.length - 1);
        if (!regex.test(elementValue)) {
          console.log("Entered char is not alphabet");
          e.srcElement.value = tempValue;
        }
    }
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.opcion1 = this.navParams.get('idsalon');
            if(this.opcion1!=null){
                this.opcion2 = '2';
            }
            this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid = localStorage.getItem('ID');
            this._service.Perfillist(this.usuarioid).subscribe((response) => { 
                this.loading.dismiss().then( () => {
                      if (response.code!=200){

                      }else{
                            this.data = response.datos[0];//publicidad
                            this.fecha = this.data.fecha_nacimiento;
                            console.log(this.data);
                      }
                });
            },error => {
                  let alerta = this.alertCtrl.create({
                        title: "Aviso",
                        message: "Disculpe, no se pudo conectar al servidor",
                        buttons: [
                          {
                              text: "Reintentar",
                              role: 'cancel',
                              cssClass:'ion-aceptar',
                              handler: data => {
                                this.loading.dismiss().then( () => {this.nav.pop();});
                              }
                          }
                        ]
                      });
                      alerta.present();
            });//FIN POST
             this.loading = this.loadingCtrl.create({
              dismissOnPageChange: false,
              content: 'Cargando...'
            });
            this.loading.present();
        }else{ 
            this.storage.remove('ID');
            this.storage.remove('USERNAME');  
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }//fin function
  update(){
      console.log(this.myForm.value);
      let alert = this.alertCtrl.create({
          title: "¿Está seguro que desea actualizar?",
          message:"&nbsp;&nbsp; Los datos de usuarios van a ser modificados.",
          buttons: [
                {
                  text: 'No',
                  role: 'cancel',
                  cssClass:'ion-cancelar',
                  handler: data => {

                  }
                },
                {
                  text: 'Si',
                  cssClass:'ion-aceptar',
                  handler: data => {
                          this._service.Perfilupdate(this.usuarioid, this.myForm.value).subscribe((response) => { 
                                this.loading.dismiss().then( () => {
                                      let alert = this.alertCtrl.create({
                                      title: "Modificación Perfil",
                                      message:"&nbsp;&nbsp; Sus datos fueron modificados con éxito",
                                      buttons: [
                                            {
                                              text: 'Aceptar',
                                              cssClass:'ion-aceptar',
                                              handler: data => {
                                                localStorage.setItem('NOMBRESAPELLIDOS', this.myForm.value.nombres+' '+this.myForm.value.apellidos)
                                                //this.navController.pop();
                                                //this.navController.setRoot('InicioPage');
                                                  if(this.opcion2=='2'){
                                                      this.nav.push('ReservafechahoraPage', { id : this.opcion1});
                                                  }else{
                                                      this.nav.pop();  
                                                  }
                                              }
                                            }
                                          ],

                                      });
                                      alert.present();
                                });
                          },error => {
                                let alerta = this.alertCtrl.create({
                                      title: "Aviso",
                                      message: "Disculpe, no se pudo conectar al servidor",
                                      buttons: [
                                        {
                                            text: "Reintentar",
                                            role: 'cancel',
                                            cssClass:'ion-aceptar',
                                            handler: data => {
                                              this.loading.dismiss().then( () => {this.nav.pop();});
                                            }
                                        }
                                      ]
                                    });
                                    alerta.present();
                          });//FIN POST
                          this.loading = this.loadingCtrl.create({
                            dismissOnPageChange: false,
                            content: 'Cargando...'
                          });
                          this.loading.present();
                  }
                }
              ],
      });
      alert.present();
  }//fin function

}//fin componente 
