import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Recuperar1Page } from './recuperar1';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    Recuperar1Page,
  ],
  imports: [
    IonicPageModule.forChild(Recuperar1Page),
    Ionic2RatingModule
  ],
  exports: [
    Recuperar1Page
  ]
})
export class Recuperar1PageModule {}
