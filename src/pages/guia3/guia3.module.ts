import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Guia3 } from './guia3';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    Guia3,
  ],
  imports: [
    IonicPageModule.forChild(Guia3),
    Ionic2RatingModule
  ],
  exports: [
    Guia3
  ]
})
export class Guia3Module {}
