import { Component } from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic';
import { ToastController, IonicPage, NavController,LoadingController, Loading, AlertController, Nav} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
//import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { Login } from '../../providers/servicio/login';
import { Verifica } from '../../providers/servicio/verifica';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-verificarclave',
  templateUrl: 'verificarclave.html',
  providers:[Login, Diagnostic, Verifica]
})
export class VerificarclavePage {

  myForm: FormGroup;
 // user: Observable<firebase.User>;
  public loading:Loading;
  public registro:Login;
  public email = "";

  public usuario = "";
  public push_token = "";
  public push_paltf = "";
  public usuarioid  = "";

  constructor(
    public navCtrl: NavController,
    public nav: Nav,
    public navController: NavController,
    public formBuilder: FormBuilder,
    public afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private _service: Login,
    private _service_ve: Verifica,
    public storage: Storage,
    private diagnostic: Diagnostic,
    private _login: Login,
    private toastCtrl: ToastController
  ) {
        this.myForm = this.formBuilder.group({
            code_verificacion: ['', Validators.required]
        });
       
  }//fin constructor
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
          this._service_ve.Verifica().subscribe((response) => { 
                  this.loading.dismiss().then( () => { 
                        if (response.code!=200){

                        }else{
                          this.navCtrl.setRoot('HomePage');
                        }
                  });//FIN POST
          },error => {
                      let alerta = this.alertCtrl.create({
                            title: "Aviso",
                            message: "Disculpe, no se pudo conectar al servidor",
                            buttons: [
                              {
                                  text: "Reintentar",
                                  role: 'cancel',
                                  cssClass:'ion-aceptar',
                                  handler: data => {
                                    this.loading.dismiss().then( () => {this.nav.pop();});
                                  }
                              }
                            ]
                          });
                          alerta.present();
          });//FIN POST
          this.loading = this.loadingCtrl.create({
            dismissOnPageChange: false,
          });
          this.loading.present();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            //this.navController.setRoot("VerificarPage");
        }
  }
  ir(opcion){
  	this.storage.set('OPEN', 'true');
  	      if(opcion==1){
  		this.navController.push("LoginPage");
  	}else if(opcion==2){
  		this.navController.push("PoliticasPage");
    }else if(opcion==4){
      this.navController.push("Recuperar1Page");
  	}else{
  		this.navController.push("TerminosPage");
  	}
  }



  loginUser(){
    this.email = localStorage.getItem('EMAIL');
    console.log(this.email);
    this._service.verificarclave(this.myForm.value, this.email).subscribe((response) => { 
                this.loading.dismiss().then( () => {  
                          if(response.token=='false'){
                               this.loading.dismiss().then( () => {
                                  let alert = this.alertCtrl.create({
                                    message: response.msg,
                                    buttons: [
                                      {
                                        text: "Ok",
                                        role: 'cancel'
                                      }
                                    ]
                                  });
                                  alert.present();
                                });
                    }else if (response.code!=200){
                                this.loading.dismiss().then( () => {
                                  let alert = this.alertCtrl.create({
                                    message: response.msg,
                                    buttons: [
                                      {
                                        text: "Ok",
                                        role: 'cancel'
                                      }
                                    ]
                                  });
                                  alert.present();
                                });
                    }else{
                        //Guardamos la sesion de datos aca
                        this.storage.remove('ID');
                        this.storage.remove('USERNAME');
                        this.storage.remove('NOMBRES');
                        this.storage.remove('APELLIDOS');
                        this.storage.remove('TOKEN');

                        this.storage.set('ID', response['datos']['id']);  
                        this.storage.set('USERNAME', response['datos']['username']);  
                        this.storage.set('NOMBRESAPELLIDOS', response['datos']['nombres']);  
                        this.storage.set('TIPOUSUARIO', response['datos']['role_id']);
                        this.storage.set('OPEN', 'true');
                        this.storage.set('TOKEN', response.token);

                        //cambio  
                        localStorage.setItem('ID', response['datos']['id']);
                        localStorage.setItem('USERNAME', response['datos']['username']);
                        localStorage.setItem('NOMBRESAPELLIDOS', response['datos']['nombres']);
                        localStorage.setItem('TIPOUSUARIO', response['datos']['role_id']);
                        localStorage.setItem('OPEN','true');
                        localStorage.setItem('TOKEN', response.token);

                        this.push_token = localStorage.getItem('PUSH_TOKEN');
                        this.push_paltf = localStorage.getItem('PUSH_PLATFORM');

                        this._login.UpdatePush(this.push_token, this.push_paltf, response['datos']['id'] ).subscribe((response) => {});

                        this.storage.set('OPEN', 'true');
                        this.navCtrl.setRoot('HomePage');
                    }//Fin else
                });//FIN POST
    },error => {
              let alerta = this.alertCtrl.create({
                    title: "Aviso",
                    message: "Disculpe, no se pudo conectar al servidor",
                    buttons: [
                      {
                          text: "Reintentar",
                          role: 'cancel',
                          cssClass:'ion-aceptar',
                          handler: data => {
                            this.loading.dismiss().then( () => {this.nav.pop();});
                          }
                      }
                    ]
                  }); 
                  alerta.present();
    });//FIN POST
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: false,
    });
    this.loading.present();
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  goToSignup(){
    this.navCtrl.push('SignupPage');
  }

  goToResetPassword(){
    this.navCtrl.push('ResetPasswordPage');
  }

}
