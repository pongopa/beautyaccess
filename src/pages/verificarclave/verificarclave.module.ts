import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerificarclavePage } from './verificarclave';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    VerificarclavePage,
  ],
  imports: [
    IonicPageModule.forChild(VerificarclavePage),
    Ionic2RatingModule
  ],
  exports: [
    VerificarclavePage
  ]
})
export class VerificarclavePagePageModule {}
