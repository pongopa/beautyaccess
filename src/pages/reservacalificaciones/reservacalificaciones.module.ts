import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReservacalificacionesPage } from './reservacalificaciones';

import { StarRatingModule } from 'ionic3-star-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    ReservacalificacionesPage
  ],
  imports: [
    StarRatingModule,
    IonicPageModule.forChild(ReservacalificacionesPage),
    MyNavbarModule
  ],
  exports: [
    ReservacalificacionesPage
  ]
})
export class ReservacalificacionesPageModule {}
