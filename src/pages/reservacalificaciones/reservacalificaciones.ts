import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Reservas } from '../../providers/servicio/reservas';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Events } from 'ionic-angular';

// Import ionic-rating module

import { IonicPage, NavController, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-reservacalificaciones',
  templateUrl: 'reservacalificaciones.html',
  providers:[Reservas, GlobalVars]
})
export class ReservacalificacionesPage {

  @Input()
  rating: number = 3;
  @Input()
  readonly: string = "false";
  @Input()
  activeColor : string = '#488aff';
  @Input()
  defaultColor : string = '#f4f4f4';
  @Input()
  activeIcon : string = 'ios-star';
  @Input()
  defaultIcon : string = 'ios-star-outline';
  Math: any;
  parseFloat : any;

myForm: FormGroup;
reserva_id: any;
salonid: any;
public loading:Loading;
public usuario = "";
public datos: any;
public contentString = "";
public contentStringT = "";
public contador = 0;
public valor    = 0;
public result   = 0;

public razon_social = "";
public email        = "";
public direccion    = "";
public telefono     = "";
public mensaje     = "";

public fecha    = "";
public hora     = "";

public cancelar    = "";
public calificar     = "";

public ver = "";

public img1     = "";
public img2     = "";
public img3     = "";
public urlimg2  = "";
public urlimg   = new GlobalVars();

public  usuarioid  = "";
public  tipocalificacion ="";
public cali:any =  [];
public comentario = "";
public atendido = "";

public servicio = "";
public localizacion = "";
public atencion = "";
public infraestructura = "";

public calificar1 = 0;
public calificar2 = 0;
public calificar3 = 0;
public calificar4 = 0;

public calificar_array: any = [];

  constructor(public nav: Nav,
              public navController: NavController,
              public alertCtrl: AlertController, 
              public nave: Nav,  
              private _service: Reservas,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public formBuilder: FormBuilder,
              public navParams: NavParams,
              private events: Events) {
    this.myForm = this.formBuilder.group({
      comentario: ['', Validators.required],
      atendido: ['', Validators.required],
      servicio: ['', Validators.required]
    });
    this.Math = Math;
    this.parseFloat = parseFloat;
    //events.subscribe('star-rating:changed', (starRating) => {});
      //events.subscribe('star-rating:changed', (starRating) => {console.log(starRating)});
  }
  changeRating(event, id){
    if(this.readonly && this.readonly === "true") return;
    // event is different for firefox and chrome
    this.rating = event.target.id ? parseInt(event.target.id) + 1 : parseInt(event.target.parentElement.id) + 1;
    // subscribe this event to get the changed value in ypour parent compoanent 
    this.events.publish('star-rating:changed', this.rating);

    this.calificar_array[id] =  this.rating;
    //console.log( this.rating+' - '+id);
     /*     if(id==1){  this.calificar1 =  this.rating;

    }else if(id==2){  this.calificar2 =  this.rating;

    }else if(id==3){  this.calificar3 =  this.rating;

    }else if(id==4){  this.calificar4 =  this.rating;

    }*/
  }
  log(vari){
    console.log('10');
  }
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  cambio(name){
    console.log(name);
  }
  
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            //this.navCtrl.setRoot('HomePage');
            this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid  = localStorage.getItem('ID');
            this.ver = this.navParams.get('id');
            if(this.ver!=null){
              this.storage.set('reserva_id', this.navParams.get('id'));
              localStorage.setItem('reserva_id',this.navParams.get('id'));

              this.storage.set('salonid', this.navParams.get('salid'));
              localStorage.setItem('salonid',this.navParams.get('salid'));
            }
            this.reserva_id = localStorage.getItem('reserva_id');
            this.salonid    = localStorage.getItem('salonid');

            this.loadpage();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");

            localStorage.clear();
        }
  }
  reservafechahora(){
    this.nav.push("ReservafechahoraPage", { id : this.reserva_id});
  }
  califica(){
    this.nav.push("ReservacalificarPage", { residid : this.reserva_id, salid : this.salonid});
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  procesar(form, events){
    console.log(document.getElementsByTagName('ionic3-star-rating'));
    console.log(document.getElementById('rating1'));
    //events.subscribe('star-rating:changed', (starRating) => {this.cali[form] = starRating; console.log('id '+form);});
         this._service.calificaciones(this.salonid, this.usuarioid, this.reserva_id, form.value, this.calificar_array, this.atendido).subscribe((response) => {   
                        this.loading.dismiss().then( () => {
                          let alert = this.alertCtrl.create({
                            title: response.msgh,
                            message: response.msg,
                            buttons: [
                              {
                                text: 'Aceptar',
                                cssClass:'ion-aceptar',
                                handler: data => {
                                    //this.nav.push("MisreservasPage", {});
                                    this.navController.setRoot('InicioPage');
                                }
                              }
                          ]
                          });
                          alert.present();
                        }); 
        },error => {
                  let alerta = this.alertCtrl.create({
                        title: "Aviso",
                        message: "Disculpe, no se pudo conectar al servidor",
                        buttons: [
                          {
                              text: "Reintentar",
                              role: 'cancel',
                              cssClass:'ion-aceptar',
                              handler: data => {
                                this.loading.dismiss().then( () => {this.nav.pop();});
                              }
                          }
                        ]
                      });
                      alerta.present();
        });//FIN POST
        this.loading = this.loadingCtrl.create({
        dismissOnPageChange: false,
      });
      this.loading.present(); 
  }//fin function
  loadpage(){
   
      this._service.Ver(this.reserva_id).subscribe((response) => { 
                            if (response.code==200){
                              this.datos        = response['datos'];

                              this.cancelar     = response['cancelar'];
                              this.calificar    = response['calificar'];
                              this.tipocalificacion    = response['tipocalificacion'];
                              console.log(this.tipocalificacion); 

                              let miscalifi  = response['tipocalificacion']; 
                              miscalifi.forEach(val => {
                                this.cali[val.id] = "";
                              });
                              
                              this.razon_social = this.datos[0]['salones']['razon_social'];
                              this.email        = this.datos[0]['salones']['email'];
                              this.direccion    = this.datos[0]['salones']['direccion'];
                              this.telefono     = this.datos[0]['salones']['telefono'];
                              this.telefono     = this.datos[0]['salones']['telefono'];
                              this.salonid      = this.datos[0]['salones']['id'];

                              this.fecha = this.datos[0]['fecha'];
                              this.hora  = this.datos[0]['hora'];

                              if(this.datos[0]['foto1']==""){this.datos[0]['foto1']="upload/default.png";}
                              if(this.datos[0]['foto2']==""){this.datos[0]['foto2']="upload/default.png";}
                              if(this.datos[0]['foto3']==""){this.datos[0]['foto3']="upload/default.png";}

                              this.img1     = this.datos[0]['salones']['foto1'];
                              this.img2     = this.datos[0]['salones']['foto2'];
                              this.img3     = this.datos[0]['salones']['foto3'];
                              this.urlimg2  = this.urlimg.getMyserve();
                              
                              let miscali  = response['datos2']; 

                                                                                
                            }else{
                              
                            }//Fin else
      },error => {
                let alerta = this.alertCtrl.create({
                      title: "Aviso",
                      message: "Disculpe, no se pudo conectar al servidor",
                      buttons: [
                        {
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                              this.loading.dismiss().then( () => {this.nav.pop();});
                            }
                        }
                      ]
                    });
                    alerta.present();
      });//FIN POST
  }
 
}//fin componente 
