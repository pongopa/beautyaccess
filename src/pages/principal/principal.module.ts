import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrincipalPage } from './principal';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    PrincipalPage
  ],
  imports: [
    IonicPageModule.forChild(PrincipalPage),
    MyNavbarModule
  ],
  exports: [
    PrincipalPage
  ]
})
export class PrincipalPageModule {}
