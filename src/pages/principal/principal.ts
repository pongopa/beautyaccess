import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Salon } from '../../providers/servicio/salon';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-principal',
  templateUrl: 'principal.html',
  providers:[Salon, GlobalVars, Principal]
})
export class PrincipalPage {
    @Input() data: any;
    @Input() data2: any;
    @Input() data3: any;
    @Input() events: any;
    public data4 = "";
    @ViewChild('wizardSlider') slider: Slides;
    sliderOptions = { pager: true };
    path:boolean = false;
    prev:boolean = true;
    next:boolean = true;
    finish:boolean = true;
    @ViewChild('wizardSlider2') slider2: Slides;
    sliderOptions2 = { pager2: true };
    path2:boolean = false;
    prev2:boolean = true;
    next2:boolean = true;
    finish2:boolean = true;
    public loading:Loading;
    usuario = "";
    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    constructor(
              public nav: Nav, 
              private _service: Principal,
              private navController: NavController, 
              public storage: Storage,
              public statusBar: StatusBar, 
              public loadingCtrl: LoadingController,
              public menuCtrl: MenuController,
              public navCtrl: NavController,
              public  alertCtrl: AlertController,
              public navParams: NavParams) {

      this.prev = false;
      this.next = true;
      this.finish = false;

     // this.statusBar.backgroundColorByHexString('#f59332');

      this.prev2 = false;
      this.next2 = true;
      this.finish2 = false;
  }
  ionViewDidLoad() { 
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
            this.urlimg2 = this.urlimg.getMyserve();

            this._service.principal('1').subscribe((response) => { 
                this.loading.dismiss().then( () => {
                  if (response.code!=200){

                  }else{
                    console.log(response.datos2);
                    this.data2 = response.datos1;//publicidad
                    this.data3 = response.datos2;//historias
                    this.data4 = response.datos4;//historias
                  }
                });
            },error => {
                    let alerta = this.alertCtrl.create({
                          title: "Aviso",
                          message: "Disculpe, no se pudo conectar al servidor",
                          buttons: [
                            {
                                text: "Reintentar",
                                role: 'cancel',
                                cssClass:'ion-aceptar',
                                handler: data => {
                                  this.loading.dismiss().then( () => {this.nav.pop();});
                                }
                            }
                          ]
                        });
                        alerta.present();
            });//FIN POST
            this.loading = this.loadingCtrl.create({
                dismissOnPageChange: false,
                content: 'Cargando...'
            });
            this.loading.present();
            this.data  = this.getDataForLayout1();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  openPage() {
      window.open('whatsapp://send?phone=+593988488913', '_system');
  }//fin
  link(p1){ 
        console.log(p1);
              if(p1=="1"){ this.nav.push("MisreservasPage");

        }else if(p1=="2"){ this.nav.push("BuscarPage");

        }else if(p1=="3"){ this.nav.push("BuscarservicioPage");

        }else if(p1=="4"){ this.nav.push("RecargasgifcardPage");

        }else if(p1=="5"){ this.nav.push("PromocionlistPage");

        }else if(p1=="6"){ this.nav.push("HistorialPage");

        }else if(p1=="7"){ this.nav.push("BeautytipsPage");

        }else if(p1=="8"){ this.nav.push("HistoriaPage");

        }else if(p1=="9"){ this.nav.push("CompartirPage");

        }else if(p1=="10"){ window.open('whatsapp://send?phone=+593988488913', '_system');


        }
  }//fin function
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  getDataForLayout1 = (): any => {
        return {
            "toolBarTitle":"Simple + icon",
            "btnPrev":"<",
            "btnNext":">",
            "btnFinish":"Finish",
            "items":[
               {
                  "logo":"",
                  "iconSlider":"./assets/images/6-imagen2-03.png",
                  "title":"Fragment Example 1",
                  "buttonNext":">"
               },
               {
                  "logo":"",
                  "iconSlider":"./assets/images/6-imagen2-03.png",
                  "title":"Fragment Example 2",
                  "description":"Text for Fragment Example 2 Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                  "buttonNext":">",
                  "buttonPrevious":"<"
               },
               {
                  "logo":"",
                  "iconSlider":"./assets/images/6-imagen2-03.png",
                  "title":"Fragment Example 3",
                  "buttonPrevious":"<"
               }
            ]
         };
  };
  changeSlide(index: number): void {
        if (index > 0) {
            this.slider.slideNext(300);
        } else {
            this.slider.slidePrev(300);
        }
  }
  slideHasChanged(index: number): void {
        try {
            this.prev = !this.slider.isBeginning();
            this.next = this.slider.getActiveIndex() < (this.slider.length() - 1);
            this.finish = this.slider.isEnd();
        } catch (e) { }
  }

  changeSlide2(index: number): void {
        if (index > 0) {
            this.slider2.slideNext(300);
        } else {
            this.slider2.slidePrev(300);
        }
  }
  slideHasChanged2(index: number): void {
        try {
            this.prev2   = !this.slider2.isBeginning();
            this.next2   = this.slider2.getActiveIndex() < (this.slider2.length() - 1);
            this.finish2 = this.slider2.isEnd();
        } catch (e) { }
  }
}//fin componente 
