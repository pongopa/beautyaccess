import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Salon } from '../../providers/servicio/salon';
import { Principal } from '../../providers/servicio/principal';
import { Perfil } from '../../providers/servicio/perfil';
import { Reservas } from '../../providers/servicio/reservas';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { IonicPage, NavController, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-reservafactura',
  templateUrl: 'reservafactura.html',
  providers:[Perfil, Salon, GlobalVars, Principal, Reservas]
})
export class ReservafacturaPage {
  salonid: any;
  usuario = "";
  fecha   = "";
  fecha2  = "";
  hora    = "";
  hora2:any = "";

  servicio1  = "";
  servicio2  = "";
  servicio3  = "";
  servicio4  = "";
  servicio5  = "";
  servicio6  = "";
  servicio7  = "";
  servicio8  = "";
  servicio9  = "";
  servicio10 = "";
  i          = 0;

  public loading:Loading;
  public loading2:Loading;
  public loading3:Loading;
  public datos: any;
  public contentString = "";
  public contentStringT = "";
  public contador = 0;
  public valor    = 0;
  public user_id = "";
  public result   = 0;

  public ver      = 0;
  public razon_social = "";
  public email        = "";
  public direccion    = "";
  public telefono     = "";

  public img1     = "";
  public img2     = "";
  public img3     = "";
  public urlimg2  = "";
  public urlimg   = new GlobalVars();
  public url_ser  = new GlobalVars();
  public url_ser_ = "";
  public ser:any =  [];

   public facturar     = "";
   public totales      = "";
   public total_total  = "";

   public url_ser_perfil  = new GlobalVars();
  public url_ser__perfil = "";

  usuarioid = "";
  data1 = "";
  data2 = "";
  data3 = "";
  data4 = "";
  public option3: any;
  public options2: ThemeableBrowserOptions = {
       location : 'no',
       fullscreen : 'yes',
        statusbar: {
           color: '#ffffffff'
       },
       toolbar: {
           height: 0,
           color: '#f0f0f0ff',
       },
       backButtonCanClose: true
  };
  public options : InAppBrowserOptions = {
      location : 'no',//Or 'no'
      /*hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only
      toolbar : 'yes', //iOS only
      enableViewportScale : 'no', //iOS only
      allowInlineMediaPlayback : 'no',//iOS only
      presentationstyle : 'pagesheet',//iOS only */
      fullscreen : 'yes',//Windows only
  };
  constructor(public nav: Nav,
              private _service: Principal,
              private reserva_service: Reservas,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              private service_perfil: Perfil,
              private salon_service: Salon,
              public navCtrl: NavController,
              private navController: NavController,
              public  alertCtrl: AlertController,
              public iab: InAppBrowser,
              public themeableBrowser: ThemeableBrowser,
              public navParams: NavParams) {
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  comentario(){
    this.nav.push("ComentarioPage", { id : this.salonid});
  }
  setHora2(){
    var current_date = new Date();
      var mifecha = this.fecha.split('-');
      var mihora = this.hora.split(':');

      current_date.setFullYear(parseInt(mifecha[0]),parseInt(mifecha[1]),parseInt(mifecha[2]));
      if(mihora[0]){
         current_date.setHours(parseInt(mihora[0]),parseInt(mihora[1]),0);
      }

      this.hora2 = current_date;
  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
            this.ver = this.navParams.get('id');
            if(this.ver!=null){
              this.storage.set('salonid', this.navParams.get('id'));
              localStorage.setItem('salonid',this.navParams.get('id'));

              this.storage.set('fechareserva', this.navParams.get('f'));
              localStorage.setItem('fechareserva',this.navParams.get('f'));

              this.storage.set('hora', this.navParams.get('h'));
              localStorage.setItem('hora',this.navParams.get('h'));

              this.storage.set('s1', this.navParams.get('s1'));
              localStorage.setItem('s1',this.navParams.get('s1'));

              this.storage.set('s2', this.navParams.get('s2'));
              localStorage.setItem('s2',this.navParams.get('s2'));

              this.storage.set('s3', this.navParams.get('s3'));
              localStorage.setItem('s3',this.navParams.get('s3'));

              this.storage.set('s4', this.navParams.get('s4'));
              localStorage.setItem('s4',this.navParams.get('s4'));

              this.storage.set('s5', this.navParams.get('s5'));
              localStorage.setItem('s5',this.navParams.get('s5'));

              this.storage.set('s6', this.navParams.get('s6'));
              localStorage.setItem('s6',this.navParams.get('s6'));

              this.storage.set('s7', this.navParams.get('s7'));
              localStorage.setItem('s7',this.navParams.get('s7'));

              this.storage.set('s8', this.navParams.get('s8'));
              localStorage.setItem('s8',this.navParams.get('s8'));

              this.storage.set('s9', this.navParams.get('s9'));
              localStorage.setItem('s9',this.navParams.get('s9'));

              this.storage.set('s10', this.navParams.get('s10'));
              localStorage.setItem('s10',this.navParams.get('s10'));
            }
            this.salonid    = localStorage.getItem('salonid');
            this.fecha      = localStorage.getItem('fechareserva');
            this.hora       = localStorage.getItem('hora');
            this.usuarioid  = localStorage.getItem('ID');

            //this.hora2      = this.fecha+' '+this.hora+':00';
            this.setHora2();

            this.servicio1   = localStorage.getItem('s1');
            this.servicio2   = localStorage.getItem('s2');
            this.servicio3   = localStorage.getItem('s3');
            this.servicio4   = localStorage.getItem('s4');
            this.servicio5   = localStorage.getItem('s5');
            this.servicio6   = localStorage.getItem('s6');
            this.servicio7   = localStorage.getItem('s7');
            this.servicio8   = localStorage.getItem('s8');
            this.servicio9   = localStorage.getItem('s9');
            this.servicio10  = localStorage.getItem('s10');

            this.ser[0] = this.servicio1;
            this.ser[1] = this.servicio2;
            this.ser[2] = this.servicio3;
            this.ser[3] = this.servicio4;
            this.ser[4] = this.servicio5;
            this.ser[5] = this.servicio6;
            this.ser[6] = this.servicio7;
            this.ser[7] = this.servicio8;
            this.ser[8] = this.servicio9;
            this.ser[9] = this.servicio10;

            this.user_id = localStorage.getItem('ID');

            this.salon_service.Datos(this.salonid, this.user_id).subscribe((response) => {
               this.loading2.dismiss().then( () => {
                     this.loadpage();
               });
            },error => {
                    let alerta = this.alertCtrl.create({
                          title: "Aviso",
                          message: "Disculpe, no se pudo conectar al servidor",
                          buttons: [
                            {
                                text: "Reintentar",
                                role: 'cancel',
                                cssClass:'ion-aceptar',
                                handler: data => {
                                  this.loading2.dismiss().then( () => {this.nav.pop();});
                                }
                            }
                          ]
                        });
                        alerta.present();
            });//FIN POST
            this.loading2 = this.loadingCtrl.create({
              dismissOnPageChange: false,
              content: 'Cargando...'
            });
            this.loading2.present();


        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        }
  }
  continuar(op){
    this.reserva_service.procesar(this.salonid,this.usuarioid,  this.fecha, this.hora, this.ser, op).subscribe((response) => {
                       if (response.code==202){
                                                  this.loading.dismiss().then( () => {
                                                      let alert = this.alertCtrl.create({
                                                        title: response.msgh,
                                                        message: response.pasarela_message,
                                                        buttons: [
                                                          {
                                                            text: 'Aceptar',
                                                            role: 'cancel',
                                                            cssClass:'ion-aceptar',
                                                            handler: data => {
                                                                this.navController.setRoot('InicioPage');
                                                            }
                                                          }
                                                        ]
                                                      });
                                                      alert.present();
                                                  });
                        }else if (response.code==300){/////////////////ENVIA A CREAR SUSCRIPCIóN
                                            
                                            /*const browser_perfil = this.iab.create(response.processUrl,"_blank", this.options);
                                              browser_perfil.on('loadstart').subscribe((response2_perfl) => {
                                                      this.url_ser__perfil = this.url_ser_perfil.getMyGlobalVar()+'update_transaccion_procesar';
                                                      if(response2_perfl.url==this.url_ser__perfil){*/

                                            const browser_perfil: ThemeableBrowserObject = this.themeableBrowser.create(response.processUrl, '_blank', this.options2);
                                            browser_perfil.on('loadstart').subscribe(response2_perfl => {
                                            this.url_ser__perfil = this.url_ser_perfil.getMyGlobalVar()+'update_transaccion_procesar';
                                                      if(response2_perfl['url']==this.url_ser__perfil){

                                                            this.reserva_service.update_transaccion(response.requestId).subscribe((response3_perfil) => {
                                                                      browser_perfil.close();
                                                                      this.loading.dismiss().then( () => {
                                                                          let alert_perfil = this.alertCtrl.create({
                                                                            title: response3_perfil.msgh,
                                                                            message: response3_perfil.pasarela_message,
                                                                            buttons: [
                                                                              {
                                                                                text: 'Aceptar',
                                                                                role: 'cancel',
                                                                                cssClass:'ion-aceptar',
                                                                                handler: data => {
                                                                                                  this.navController.setRoot('InicioPage');
                                                                                      }//fin handler
                                                                                }
                                                                              ]
                                                                            });
                                                                            alert_perfil.present();
                                                                    });
                                                            },error => {
                                                                    let alerta = this.alertCtrl.create({
                                                                          title: "Aviso",
                                                                          message: "Disculpe, no se pudo conectar al servidor",
                                                                          buttons: [
                                                                            {
                                                                                text: "Reintentar",
                                                                                role: 'cancel',
                                                                                cssClass:'ion-aceptar',
                                                                                handler: data => {
                                                                                  this.loading.dismiss().then( () => {this.nav.pop();});
                                                                                }
                                                                            }
                                                                          ]
                                                                        });
                                                                        alerta.present();
                                                            });//FIN POST
                                                        }//Fin if
                                                });//fin proceso de suscripcion
                        }else if (response.code==301){/////////////////ENVIA A CREAR SUSCRIPCIóN
                                        this.usuarioid = localStorage.getItem('ID');
                                        this.service_perfil.Perfilsuscripcion(this.usuarioid).subscribe((response_perfil) => {
                                                      if (response_perfil.code==200){
                                                                    /*const browser_perfil = this.iab.create(response_perfil.processUrl,"_blank", this.options);
                                                                    browser_perfil.on('loadstart').subscribe((response2_perfl) => {
                                                                            this.url_ser__perfil = this.url_ser_perfil.getMyGlobalVar()+'update_transaccion_procesar';
                                                                            if(response2_perfl.url==this.url_ser__perfil){*/

                                                                    const browser_perfil: ThemeableBrowserObject = this.themeableBrowser.create(response_perfil.processUrl, '_blank', this.options2);
                                                                    browser_perfil.on('loadstart').subscribe(response2_perfl => {
                                                                    this.url_ser__perfil = this.url_ser_perfil.getMyGlobalVar()+'update_transaccion_procesar';
                                                                              if(response2_perfl['url']==this.url_ser__perfil){

                                                                                  this.service_perfil.suscripcion_placetoplay(this.usuarioid, response_perfil.requestId).subscribe((response3_perfil) => {
                                                                                        browser_perfil.close();
                                                                                        this.loading.dismiss().then( () => {
                                                                                            let alert_perfil = this.alertCtrl.create({
                                                                                              title: response3_perfil.msgh,
                                                                                              message: response3_perfil.pasarela_message,
                                                                                              buttons: [
                                                                                                {
                                                                                                  text: 'Aceptar',
                                                                                                  role: 'cancel',
                                                                                                  cssClass:'ion-aceptar',
                                                                                                  handler: data => {
                                                                                                                    this.navController.setRoot('InicioPage');
                                                                                                                 
                                                                                                        }//fin handler
                                                                                                  }
                                                                                                ]
                                                                                              });
                                                                                              alert_perfil.present();
                                                                                          });
                                                                                  },error => {
                                                                                          let alerta = this.alertCtrl.create({
                                                                                                title: "Aviso",
                                                                                                message: "Disculpe, no se pudo conectar al servidor",
                                                                                                buttons: [
                                                                                                  {
                                                                                                      text: "Reintentar",
                                                                                                      role: 'cancel',
                                                                                                      cssClass:'ion-aceptar',
                                                                                                      handler: data => {
                                                                                                        this.loading.dismiss().then( () => {this.nav.pop();});
                                                                                                      }
                                                                                                  }
                                                                                                ]
                                                                                              });
                                                                                              alerta.present();
                                                                                  });//FIN POST
                                                                              }//Fin if
                                                                      });//fin proceso de suscripcion


                                                       }else{
                                                                          this.loading.dismiss().then( () => {
                                                                                let alert = this.alertCtrl.create({
                                                                                  title: "Suscripción de tarjeta!",
                                                                                  message: "La Suscripción de tarjeta no pudo ser procesada, intente de nuevo!",
                                                                                  buttons: [
                                                                                      {
                                                                                      text: 'Salir',
                                                                                      role: 'cancel',
                                                                                      cssClass:'ion-cancelar',
                                                                                      handler: data => {
                                                                                                //AQUI REGRESAR
                                                                                               // this.navController.remove(this.navController.getActive().index);
                                                                                               this.navController.setRoot('InicioPage');
                                                                                      }
                                                                                    }
                                                                                ]
                                                                                });
                                                                                alert.present();
                                                                            });
                                                      }//Fin else
                                      },error => {
                                              let alerta = this.alertCtrl.create({
                                                    title: "Aviso",
                                                    message: "Disculpe, no se pudo conectar al servidor",
                                                    buttons: [
                                                      {
                                                          text: "Reintentar",
                                                          role: 'cancel',
                                                          cssClass:'ion-aceptar',
                                                          handler: data => {
                                                            this.loading.dismiss().then( () => {this.nav.pop();});
                                                          }
                                                      }
                                                    ]
                                                  });
                                                  alerta.present();
                                      });//FIN POST //fin de service_perfil
                        }else if (response.code==203){
                                this.loading.dismiss().then( () => {
                                    let alert = this.alertCtrl.create({
                                      title: response.msgh,
                                      message: response.msg,
                                      buttons: [
                                                  {
                                                    text: 'Aceptar',
                                                    role: 'cancel',
                                                    cssClass:'ion-aceptar',
                                                    handler: data => {
                                                        this.navController.setRoot('InicioPage');
                                                    }
                                                  }
                                                ]
                                    });
                                    alert.present();
                                });
                         }else if (response.code==204){
                                this.loading.dismiss().then( () => {
                                    let alert = this.alertCtrl.create({
                                      title: response.msgh,
                                      message: response.msg,
                                      buttons: [
                                                  {
                                                    text: 'Aceptar',
                                                    role: 'cancel',
                                                    cssClass:'ion-aceptar',
                                                    handler: data => {
                                                        this.navController.setRoot('InicioPage');
                                                    }
                                                  }
                                                ]
                                    });
                                    alert.present();
                                });
                        }else{
                                this.loading.dismiss().then( () => {
                                    let alert = this.alertCtrl.create({
                                      title: response.msgh,
                                      message: response.msg,
                                      buttons: [
                                          {
                                          text: 'Salir',
                                          role: 'cancel',
                                          cssClass:'ion-cancelar',
                                          handler: data => {
                                              this.navController.setRoot('InicioPage');
                                          }
                                        },
                                        {
                                          text: 'Aceptar',
                                          cssClass:'ion-aceptar',
                                          handler: data => {

                                          }
                                        }
                                    ]
                                    });
                                    alert.present();
                                });
                        }
        },error => {
                  let alerta = this.alertCtrl.create({
                        title: "Aviso",
                        message: "Disculpe, no se pudo conectar al servidor",
                        buttons: [
                          {
                              text: "Reintentar",
                              role: 'cancel',
                              cssClass:'ion-aceptar',
                              handler: data => {
                                this.loading.dismiss().then( () => {this.nav.pop();});
                              }
                          }
                        ]
                      });
                      alerta.present();
        });//FIN POST
        this.loading = this.loadingCtrl.create({
          dismissOnPageChange: false,
        });
        this.loading.present();
  }
  anterior(){
    /*this.nav.push("ReservaproductoPage", { id : this.salonid,
                                         f : this.fecha,
                                         h : this.hora,
                               });*/
    this.navCtrl.remove(this.navCtrl.getActive().index);
  }
  loadpage(){
      this.reserva_service.factura(this.salonid, this.fecha, this.hora, this.ser, this.usuarioid).subscribe((response) => {
              this.loading3.dismiss().then( () => {
                      if (response.code==200){
                            this.datos   = response['datos'];

                            this.facturar     = response['facturar'];
                            this.totales      = response['totales'];
                            this.total_total  = response['total_total'];

                            console.log(this.facturar);
                            console.log(this.totales);
                            console.log(this.total_total);

                            this.razon_social = this.datos[0]['razon_social'];
                            this.email        = this.datos[0]['email'];
                            this.direccion    = this.datos[0]['direccion'];
                            this.telefono     = this.datos[0]['telefono'];

                            if(this.datos[0]['foto1']==""){this.datos[0]['foto1']="upload/default.png";}
                            if(this.datos[0]['foto2']==""){this.datos[0]['foto2']="upload/default.png";}
                            if(this.datos[0]['foto3']==""){this.datos[0]['foto3']="upload/default.png";}

                            this.img1     = this.datos[0]['foto1'];
                            this.img2     = this.datos[0]['foto2'];
                            this.img3     = this.datos[0]['foto3'];
                            this.urlimg2  = this.urlimg.getMyserve();

                            let miscali  = response['datos2'];


                      }else{

                      }//Fin else
            });
      },error => {
                let alerta = this.alertCtrl.create({
                      title: "Aviso",
                      message: "Disculpe, no se pudo conectar al servidor",
                      buttons: [
                        {
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                              this.loading3.dismiss().then( () => {this.nav.pop();});
                            }
                        }
                      ]
                    });
                    alerta.present();
        });//FIN POST
      this.loading3 = this.loadingCtrl.create({
        dismissOnPageChange: false,
        content: 'Cargando...'
      });
      this.loading3.present();
  }//fin function

}//fin componente
