import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReservafacturaPage } from './reservafactura';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    ReservafacturaPage
  ],
  imports: [
    IonicPageModule.forChild(ReservafacturaPage),
    MyNavbarModule
  ],
  exports: [
    ReservafacturaPage
  ]
})
export class ReservafacturaPageModule {}
