import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactanosPage } from './contactanos';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    ContactanosPage
  ],
  imports: [
    IonicPageModule.forChild(ContactanosPage),
    MyNavbarModule
  ],
  exports: [
    ContactanosPage
  ]
})
export class ContactanosPageModule {}
