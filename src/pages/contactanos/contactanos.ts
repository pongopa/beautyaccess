import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Contactanos } from '../../providers/servicio/contactanos';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, LoadingController, Loading, AlertController, ViewController, MenuController, Nav } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-contactanos',
  templateUrl: 'contactanos.html',
  providers:[Contactanos]
})
export class ContactanosPage {
@ViewChild(Nav) nav: Nav;
myForm: FormGroup;
public loading:Loading;
public usuario = "";
  constructor(public navController: NavController,
              public alertCtrl: AlertController, 
              public nave: Nav,  
              private viewCtrl: ViewController,
              private _service: Contactanos,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public formBuilder: FormBuilder) {
    this.myForm = this.formBuilder.group({
      texto: ['', Validators.required]
    });
  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            //this.navCtrl.setRoot('HomePage');
             this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");

            localStorage.clear();
        }
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  addC(){
      this._service.postAdd(this.myForm.value).subscribe(
        (response) => {  
               
                       this.loading.dismiss().then( () => {
                          let alert = this.alertCtrl.create({
                            message: response.msg,
                            buttons: [
                              {
                                text: "Ok",
                                role: 'cancel'
                              }
                            ]
                          });
                          alert.present();
                        });
      },error => {
          let alerta = this.alertCtrl.create({
                title: "Aviso",
                message: "Disculpe, no se pudo conectar al servidor",
                buttons: [
                  {
                      text: "Reintentar",
                      role: 'cancel',
                      cssClass:'ion-aceptar',
                      handler: data => {
                        this.loading.dismiss().then( () => {this.nav.pop();});
                      }
                  }
                ]
              });
              alerta.present();
      });//FIN POST
      this.myForm.reset();
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: false,
      });
      this.loading.present();
  }
} 
