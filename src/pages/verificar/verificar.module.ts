import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerificarPage } from './verificar';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    VerificarPage,
  ],
  imports: [
    IonicPageModule.forChild(VerificarPage),
    Ionic2RatingModule
  ],
  exports: [
    VerificarPage
  ]
})
export class VerificarPageModule {}
