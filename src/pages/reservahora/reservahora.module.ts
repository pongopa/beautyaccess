import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReservahoraPage } from './reservahora';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    ReservahoraPage
  ],
  imports: [
    IonicPageModule.forChild(ReservahoraPage),
    MyNavbarModule
  ],
  exports: [
    ReservahoraPage
  ]
})
export class ReservahoraPageModule {}
