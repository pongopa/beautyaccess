import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Guia1 } from './guia1';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    Guia1,
  ],
  imports: [
    IonicPageModule.forChild(Guia1),
    Ionic2RatingModule
  ],
  exports: [
    Guia1
  ]
})
export class Guia1Module {}
