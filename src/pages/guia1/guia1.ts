import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Historia } from '../../providers/servicio/historia';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-guia1',
  templateUrl: 'guia1.html',
  providers:[Historia, GlobalVars, Principal]
})
export class Guia1 {
    usuario = "";
    usuarioid = "";
    data1 = "";
    data2 = "";
    data3 = "";

    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    constructor(
              public nav: Nav, 
              public navCtrl: NavController, 
              public service: Historia,
              public _service: Principal,
              private navController: NavController, 
              public storage: Storage,
              public navParams: NavParams) {

  }
  link(componente, p1){ 
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  ionViewDidLoad() {

  }
 
  
}//fin componente 
