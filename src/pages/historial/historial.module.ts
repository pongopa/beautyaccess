import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistorialPage } from './historial';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    HistorialPage
  ],
  imports: [
    IonicPageModule.forChild(HistorialPage),
    MyNavbarModule
  ],
  exports: [
    HistorialPage
  ]
})
export class HistorialPageModule {}
