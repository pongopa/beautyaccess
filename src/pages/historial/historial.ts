import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Historia } from '../../providers/servicio/historia';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Ionic2RatingModule } from 'ionic2-rating';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-historial',
  templateUrl: 'historial.html',
  providers:[Historia, GlobalVars, Principal]
})
export class HistorialPage {
    usuario = "";
    usuarioid = "";
    historiaid = "";
    data  = "";
    data2 = "";
    fecha = ""; 
    dat  = 0;

    searchTerm:any="";
    allItems:any;
    items:any="";


    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    public loading:Loading;
    public loading2:Loading;
    constructor(
              public nav: Nav, 
              public navCtrl: NavController, 
              private navController: NavController, 
              public service: Historia,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
              public _service: Principal,
              public storage: Storage,
              public navParams: NavParams) {

  }
  cargar(v, c){
   this.nav.push(v, { id : c});
  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid = localStorage.getItem('ID');
            this.urlimg2 = this.urlimg.getMyserve();
                      this._service.historial(this.usuarioid).subscribe((response) => { 
                         this.loading.dismiss().then( () => {
                            if (response.code!=200){

                            }else{
                                  this.data = response.datos;//publicidad
                                  this._service.beautypoint(this.usuarioid).subscribe((response2) => { 
                                      this.loading2.dismiss().then( () => {
                                           if (response.code!=200){

                                            }else{
                                              console.log(this.data);
                                              this.data2 = response2.datos;//publicidad
                                              this.fecha = response2.fecha;//publicidad
                                            }
                                      });

                                  },error => {
                                        let alerta = this.alertCtrl.create({
                                              title: "Aviso",
                                              message: "Disculpe, no se pudo conectar al servidor",
                                              buttons: [
                                                {
                                                    text: "Reintentar",
                                                    role: 'cancel',
                                                    cssClass:'ion-aceptar',
                                                    handler: data => {
                                                      this.loading2.dismiss().then( () => {this.nav.pop();});
                                                    }
                                                }
                                              ]
                                            });
                                            alerta.present();
                                  });//FIN POST
                                   this.loading2 = this.loadingCtrl.create({
                                      dismissOnPageChange: false,
                                      content: 'Cargando...'
                                  });
                                  this.loading2.present();


                            }
                        });
                      },error => {
                            let alerta = this.alertCtrl.create({
                                  title: "Aviso",
                                  message: "Disculpe, no se pudo conectar al servidor",
                                  buttons: [
                                    {
                                        text: "Reintentar",
                                        role: 'cancel',
                                        cssClass:'ion-aceptar',
                                        handler: data => {
                                          this.loading.dismiss().then( () => {this.nav.pop();});
                                        }
                                    }
                                  ]
                                });
                                alerta.present();
                      });//FIN POST
                      this.loading = this.loadingCtrl.create({
                          dismissOnPageChange: false,
                          content: 'Cargando...'
                      });
                      this.loading.present();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  principal(){
    //this.nav.push('PrincipalPage');
    this.navCtrl.setRoot('PrincipalPage');
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  getItems(event: any):void {
      if (!this.allItems) {
        this.allItems = this.items;
      }
      this.items = this.allItems.filter((item) => {
          return item.razon_social.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
      });
      
  }
  onEvent(event:string, item:any) {//ITEM [EVENT OR SELECTED ITEM]
      //if (this.events[event]) {
        if ('onTextChange' === event) {
          this.getItems(item);
          //this.events[event](this.searchTerm);
        } else {
          //this.events[event](item);
          this.nav.push("SalonPage", { id : item.id});
        }
      //}
      console.log(event);
  }
  
}//fin componente 
