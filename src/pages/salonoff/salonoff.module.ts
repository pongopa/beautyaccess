import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalonoffPage } from './salonoff';
import { ComponentsExpandableComponent2 } from "../../components/components-expandable2/components-expandable2";
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    SalonoffPage,
    ComponentsExpandableComponent2,
  ],
  imports: [
    IonicPageModule.forChild(SalonoffPage),
    MyNavbarModule
  ],
  exports: [
    SalonoffPage
  ]
})
export class SalonoffPageModule {}
