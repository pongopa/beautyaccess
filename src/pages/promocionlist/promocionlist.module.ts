import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromocionlistPage } from './promocionlist';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    PromocionlistPage
  ],
  imports: [
    IonicPageModule.forChild(PromocionlistPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    PromocionlistPage
  ]
})
export class PromocionlistPageModule {}
