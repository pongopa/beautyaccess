import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Promociones } from '../../providers/servicio/promociones';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Ionic2RatingModule } from 'ionic2-rating';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-promocionlist',
  templateUrl: 'promocionlist.html',
  providers:[Promociones, GlobalVars, Principal]
})
export class PromocionlistPage {
    usuario = "";
    usuarioid = "";
    historiaid = "";
    data  = "";
    data2 = "";
    fecha = "";
    dat  = 0;

    searchTerm:any="";
    allItems:any;
    items:any="";


    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    public loading:Loading;
    constructor(
              public nav: Nav, 
              public navCtrl: NavController, 
              public service: Promociones,
              public storage: Storage,
              private navController: NavController, 
              public loadingCtrl: LoadingController,
              public  alertCtrl: AlertController,
              public navParams: NavParams) {

  }
  cargar(v, c){
   this.nav.push(v, { id : c});
  }
  ionViewWillEnter() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid = localStorage.getItem('ID');
            this.urlimg2   = this.urlimg.getMyserve();
            this.service.list(this.usuarioid).subscribe((response) => { 
                this.loading.dismiss().then( () => {
                     if (response.code!=200){

                      }else{
                            this.data  = response.datos1;//publicidad
                            this.data2 = response.datos2;//publicidad
                             /*if(this.navCtrl.getPrevious().name != "PrincipalPage"){
                                this.navCtrl.remove(this.navCtrl.getPrevious().index);
                              }*/
                      }
                });
            },error => {
                    let alerta = this.alertCtrl.create({
                          title: "Aviso",
                          message: "Disculpe, no se pudo conectar al servidor",
                          buttons: [
                            {
                                text: "Reintentar",
                                role: 'cancel',
                                cssClass:'ion-aceptar',
                                handler: data => {
                                  this.loading.dismiss().then( () => {this.nav.pop();});
                                }
                            }
                          ]
                        });
                        alerta.present();
            });//FIN POST
            this.loading = this.loadingCtrl.create({
                  dismissOnPageChange: false,
                  content: 'Cargando... '
              });
              this.loading.present();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  link(componente, p1){ 
    console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
}//fin componente 
