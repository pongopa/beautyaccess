import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecargasgifcardhistoriaPage } from './recargasgifcardhistoria';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    RecargasgifcardhistoriaPage
  ],
  imports: [
    IonicPageModule.forChild(RecargasgifcardhistoriaPage),
    MyNavbarModule
  ],
  exports: [
    RecargasgifcardhistoriaPage
  ]
})
export class RecargasgifcardhistoriaPageModule {}
