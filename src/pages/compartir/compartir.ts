import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Perfil } from '../../providers/servicio/perfil';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { IonicPage, NavController, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
import { CustomFormsModule } from 'ng2-validation';

import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-compartir',
  templateUrl: 'compartir.html',
  providers:[Perfil, GlobalVars, SocialSharing]
})
export class CompartirPage {
  salonid: any;
  usuario = "";
  fecha   = "";
  fecha2  = "";
  hora    = "";

  myForm: FormGroup;


  public loading:Loading;
  public data: any;
  public datos = "";
  public codigo_compartir = "";
  public bodys = "";
  public valor_recibir = "";

  usuarioid = "";
  constructor(public nav: Nav,
              private _service: Perfil,
              public custom: CustomFormsModule,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public formBuilder: FormBuilder,
              public navCtrl: NavController, 
              private navController: NavController, 
              public  alertCtrl: AlertController,
              public navParams: NavParams,
              private socialSharing: SocialSharing
              ){

  }
  compartir(){
         //this.bodys = "¡Hola! Descárgate la app de BeautyAccess e introduce mi código "+this.codigo_compartir+" para tener $"+this.valor_recibir+" de descuento en nuestros servicios. beautyacces::https://onelink.to/y6tyxb";
         this.bodys = "¡Hola! Descárgate la app de BeautyAccess e introduce mi código "+this.codigo_compartir+" para tener $"+this.valor_recibir+" de descuento en nuestros servicios. https://beautyaccess.app/referidos/"+this.codigo_compartir;
         this.socialSharing.share(this.bodys, null, null, null);
  }//fin function

  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid = localStorage.getItem('ID');
            this._service.compartirlist(this.usuarioid).subscribe((response) => { 
                this.loading.dismiss().then( () => {
                    this.datos = response['datos'][0];
                    this.valor_recibir = response['datos'][0].valor_recibir;
                    this.codigo_compartir = response['codigo_compartir'];
                    console.log(response);
                });//fin loading
            },error => {
                let alerta = this.alertCtrl.create({
                      title: "Aviso",
                      message: "Disculpe, no se pudo conectar al servidor",
                      buttons: [
                        {
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                              this.loading.dismiss().then( () => {this.nav.pop();});
                            }
                        }
                      ]
                    });
                    alerta.present();
            });//FIN POST
            this.loading = this.loadingCtrl.create({
              dismissOnPageChange: false,
              content: 'Cargando...'
            });
            this.loading.present();
         
        }else{ 
            this.storage.remove('ID');
            this.storage.remove('USERNAME');  
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }//fin function

}//fin componente 
