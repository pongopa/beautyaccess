import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompartirPage } from './compartir';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    CompartirPage
  ],
  imports: [
    IonicPageModule.forChild(CompartirPage),
    MyNavbarModule
  ],
  exports: [
    CompartirPage
  ]
})
export class CompartirPageModule {}
