import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistroendPage } from './registroend';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    RegistroendPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistroendPage),
    Ionic2RatingModule
  ],
  exports: [
    RegistroendPage
  ]
})
export class RegistroendPageModule {}
