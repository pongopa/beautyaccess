import { Component, ViewChild} from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic';
import { ToastController, IonicPage, NavController,LoadingController, Loading, AlertController, Nav} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
//import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { Login } from '../../providers/servicio/login';
import { Verifica } from '../../providers/servicio/verifica';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-registroend',
  templateUrl: 'registroend.html',
  providers:[Login, Diagnostic, Verifica]
})
export class RegistroendPage {

  myForm: FormGroup;
 // user: Observable<firebase.User>;
  public loading:Loading;
  public registro:Login;
  public fecha = "";
  public email = "";
  public nombre = "";
  public clave = "";

  constructor(
    public navCtrl: NavController,
    public nav: Nav,
    public navController: NavController,
    public formBuilder: FormBuilder,
    public afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private _service: Login,
    private _service_ve: Verifica,
    public storage: Storage,
    private diagnostic: Diagnostic,
    private toastCtrl: ToastController
  ) {
        this.myForm = this.formBuilder.group({
            nombres: ['', Validators.required],
            apellidos: ['', Validators.required],
            fecha: ['', Validators.required],
            clave: ['', Validators.required]
        });
       
  }//fin constructor
  ir(opcion){
          if(opcion==1){
      this.navCtrl.setRoot('HomePage');
    }else if(opcion==2){
      this.nav.push('PerfilPage');
    }else{
    }
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  ionViewDidLoad() {
        this.nombre = localStorage.getItem('REGISTRADO');
        if(localStorage.getItem('OPEN') == 'true'){
          

        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.email = localStorage.getItem('EMAIL');
            this.clave = localStorage.getItem('CLAVE');
            this._service.verificarclave_registro(this.clave, this.email).subscribe((response) => {
                      this.loading.dismiss().then( () => {   
                            if (response.code==200){
                              //Guardamos la sesion de datos aca
                              this.storage.remove('ID');
                              this.storage.remove('USERNAME');
                              this.storage.remove('NOMBRES');
                              this.storage.remove('APELLIDOS');
                              this.storage.remove('TOKEN');

                              this.storage.set('ID', response['datos']['id']);  
                              this.storage.set('USERNAME', response['datos']['username']);  
                              this.storage.set('NOMBRESAPELLIDOS', response['datos']['nombre_apellido']);  
                              this.storage.set('TIPOUSUARIO', response['datos']['role_id']);
                              this.storage.set('OPEN', 'true');
                              this.storage.set('TOKEN', response.token);

                              //cambio  
                              localStorage.setItem('ID', response['datos']['id']);
                              localStorage.setItem('USERNAME', response['datos']['username']);
                              localStorage.setItem('NOMBRESAPELLIDOS', response['datos']['nombre_apellido']);
                              localStorage.setItem('TIPOUSUARIO', response['datos']['role_id']);
                              localStorage.setItem('OPEN','true');
                              localStorage.setItem('TOKEN', response.token);

                              this.storage.set('OPEN', 'true');
                              this.navCtrl.setRoot('HomePage');
                          }//Fin else
                      });//FIN POST
            },error => {
                    let alerta = this.alertCtrl.create({
                          title: "Aviso",
                          message: "Disculpe, no se pudo conectar al servidor",
                          buttons: [
                            {
                                text: "Reintentar",
                                role: 'cancel',
                                cssClass:'ion-aceptar',
                                handler: data => {
                                  this.loading.dismiss().then( () => {this.nav.pop();});
                                }
                            }
                          ]
                        });
                        alerta.present();
            });//FIN POST
            this.loading = this.loadingCtrl.create({
              dismissOnPageChange: false,
            });
            this.loading.present();
        }//fin else
  }//fin function
}//fin class
