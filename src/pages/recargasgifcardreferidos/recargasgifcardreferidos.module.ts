import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecargasgifcardreferidosPage } from './recargasgifcardreferidos';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    RecargasgifcardreferidosPage
  ],
  imports: [
    IonicPageModule.forChild(RecargasgifcardreferidosPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    RecargasgifcardreferidosPage
  ]
})
export class RecargasgifcardreferidosPageModule {}
