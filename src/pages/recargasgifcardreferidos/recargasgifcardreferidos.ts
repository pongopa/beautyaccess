import { Component, Input } from '@angular/core';
import { IonicPage, NavController,LoadingController, Loading, AlertController, Nav, MenuController, NavParams, ModalController} from 'ionic-angular';
import { Recargas } from '../../providers/servicio/recargas';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var google;
@IonicPage()
@Component({
  selector: 'page-recargasgifcardreferidos',
  templateUrl: 'recargasgifcardreferidos.html',
  providers: [GlobalVars, Recargas]
})
export class RecargasgifcardreferidosPage {
  @Input() data: any; 
  @Input() events: any;

   myForm: FormGroup;

   public loading:Loading;

  searchTerm:any="";
  allItems:any;

  items:any;
  headerImage = "";
  tiporecarga_id = "";
	//@ViewChild(Nav) nav: Nav;
    map: any;
	marker: any;
	features: any;
	datas: any;
	public marcadores: any;
	public usuariodeno: any;
	public var : any;
	public query = "";
	public msj = "";
	public saldo = "";
	public user_id = "";
	public codigo = "";

	directionsService: any = null;
	directionsDisplay: any = null;
	bounds: any;
	calificacion: any;
	myLatLng: any;
	waypoints: any[];

	public usuario = "";
	public urlimg   = new GlobalVars();
	public urlimg2  = "";

	constructor(
	    public nav: Nav,
	    private navCtrl: NavController,
	    private navController: NavController, 
	    public  alertCtrl: AlertController,
	    public formBuilder: FormBuilder,
	    public loadingCtrl: LoadingController,
	    public menuCtrl: MenuController,
	    private recargas_service: Recargas,
	    public storage: Storage,
	    public navParams: NavParams,
	    public modalCtrl : ModalController,
	  ) { 
          
        this.directionsService = new google.maps.DirectionsService();
	    this.directionsDisplay = new google.maps.DirectionsRenderer();
	    this.bounds            = new google.maps.LatLngBounds();
	    this.myForm = this.formBuilder.group({
            code_verificacion: ['', Validators.required]
        });
	    
	}
	ionViewDidLoad() {
    	if(localStorage.getItem('OPEN') == 'true'){
    		this.urlimg2 = this.urlimg.getMyserve();
            this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
            this.user_id = localStorage.getItem('ID');
            this.tiporecarga_id = localStorage.getItem('tiporecarga_id');

            this.codigo =  this.navParams.get('codigo');

            this.loag();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
		    localStorage.setItem('OPEN','false');
		    this.navController.setRoot("VerificarPage");
        }
	}
	recargar(){
		this.recargas_service.Recargasgifcard(this.myForm.value, this.user_id, this.tiporecarga_id ).subscribe((response) => {  
                  this.loading.dismiss().then( () => {
				                    let alert = this.alertCtrl.create({
				                      title: response.msgh,
				                      message: response.msg,
				                      buttons: [
						                    {
						                      text: 'Aceptar',
						                      cssClass:'ion-aceptar',
						                      handler: data => {
						                        	//this.nav.push("RecargasPage", {});
						                        	 this.nav.pop();
						                      }
						                    }
					                  ]
				                    });
				                    alert.present();
                  });
        },error => {
                let alerta = this.alertCtrl.create({
                      title: "Aviso",
                      message: "Disculpe, no se pudo conectar al servidor",
                      buttons: [
                        {
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                              this.loading.dismiss().then( () => {this.nav.pop();});
                            }
                        }
                      ]
                    });
                    alerta.present();
        });//FIN POST
        this.loading = this.loadingCtrl.create({
	      dismissOnPageChange: false,
	    });
	    this.loading.present();
	}
	loag(){
		this.recargas_service.Recargasaldo(this.user_id).subscribe((response) => { 
 	        this.loading.dismiss().then( () => {
		 	        if (response.code!=200){

		            }else{
		                 this.saldo       = response['saldo'];
		                 this.headerImage = "assets/background/27.jpg";
		                 console.log(this.saldo);
		            }
		    });
	    },error => {
                let alerta = this.alertCtrl.create({
                      title: "Aviso",
                      message: "Disculpe, no se pudo conectar al servidor",
                      buttons: [
                        {
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                              this.loading.dismiss().then( () => {this.nav.pop();});
                            }
                        }
                      ]
                    });
                    alerta.present();
        });//FIN POST
	    this.loading = this.loadingCtrl.create({
	      dismissOnPageChange: false,
	    });
	    this.loading.present();
	}
	link(componente, p1){ 
	    //console.log(p1);
	    if(componente!=""){
	     this.nav.push(componente, { id : p1});
	    }
	}//fin function
	mapa(){
        this.navController.setRoot('InicioPage');
	}//fin function
	perfil(){
	    this.nav.push('PerfilPage');
	}//fin function
	getItems(event: any):void {
	    if (!this.allItems) {
	      this.allItems = this.items;
	    }
	    this.items = this.allItems.filter((item) => {
	        return item.razon_social.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
	    });
	    
	}
	onEvent(event:string, item:any) {//ITEM [EVENT OR SELECTED ITEM]
	    //if (this.events[event]) {
	      if ('onTextChange' === event) {
	        this.getItems(item);
	        //this.events[event](this.searchTerm);
	      } else {
	        //this.events[event](item);
	        if(item.link!=""){
	        	this.nav.push(item.link, {});	
	        }
	      }
	    //}
	    console.log(event);
	}
	

}//fin class