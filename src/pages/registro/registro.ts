import { Component, ViewChild} from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic';
import { ToastController, IonicPage, NavController,LoadingController, Loading, AlertController, Nav} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
//import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { Login } from '../../providers/servicio/login';
import { Verifica } from '../../providers/servicio/verifica';
import { Storage } from '@ionic/storage';

import { CustomFormsModule } from 'ng2-validation';

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
  providers:[Login, Diagnostic, Verifica]
})
export class RegistroPage {

  myForm: FormGroup;
 // user: Observable<firebase.User>;
  public loading:Loading;
  public registro:Login;
  public fecha = "";
  public email = "";

  constructor(
    public navCtrl: NavController,
    public nav: Nav,
    public navController: NavController,
    public formBuilder: FormBuilder,
    public afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private _service: Login,
    private _service_ve: Verifica,
    public storage: Storage,
    private diagnostic: Diagnostic,
    public custom: CustomFormsModule,
    private toastCtrl: ToastController
  ) {
        this.myForm = this.formBuilder.group({
            //fecha: ['', Validators.required],
            clave: ['', Validators.required],
            cabello: ['', Validators.required],

            nombres: ['',   [Validators.pattern('[a-zA-ZáéíóúÁÉÍÓÚñÑ ]*'), Validators.required]],
            apellidos: ['', [Validators.pattern('[a-zA-ZáéíóúÁÉÍÓÚñÑ ]*'), Validators.required]],
            //ci: ['',        [Validators.pattern('[0-9]*'), Validators.maxLength(11), Validators.required]],
            //telefono: ['',  [Validators.pattern('[0-9]*'), Validators.maxLength(20), Validators.required]]

            //nombres: ['',   Validators.required],
            //apellidos: ['', Validators.required],
            //ci: ['',        Validators.required],
            //telefono: ['',  Validators.required]
        });
       
  }//fin constructor
  getText(e){
     var elementValue = e.srcElement.value;
    if(elementValue){
       var regex = /^[a-zA-Z ]+$/;   
        var tempValue = elementValue.substring(0, elementValue.length - 1);
        if (!regex.test(elementValue)) {
          console.log("Entered char is not alphabet");
          e.srcElement.value = tempValue;
        }
    }
  }
  getNum(e){
     var elementValue = e.srcElement.value;
    if(elementValue){
       var regex = /^[0-9]+$/;   
        var tempValue = elementValue.substring(0, elementValue.length - 1);
        if (!regex.test(elementValue)) {
          console.log("Entered char is not alphabet");
          e.srcElement.value = tempValue;
        }
    }
  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
          this._service_ve.Verifica().subscribe((response) => { 
                  if (response.code!=200){

                  }else{
                    this.navCtrl.setRoot('HomePage');
                  }
          });
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            //this.navController.setRoot("VerificarPage");
        }
  }
  registroUser(){
    this.email = localStorage.getItem('EMAIL');
    this._service.registro(this.myForm.value, this.email).subscribe((response) => { 
              this.loading.dismiss().then( () => { 
                  console.log(response); 
                        if(response.token=='false'){
                             this.loading.dismiss().then( () => {
                                let alert = this.alertCtrl.create({
                                  message: response.msg,
                                  buttons: [
                                    {
                                      text: "Ok",
                                      role: 'cancel'
                                    }
                                  ]
                                });
                                alert.present();
                              });
                  }else if (response.code!=200){
                              this.loading.dismiss().then( () => {
                                let alert = this.alertCtrl.create({
                                  message: response.msg,
                                  buttons: [
                                    {
                                      text: "Ok",
                                      role: 'cancel'
                                    } 
                                  ]
                                });
                                alert.present();
                              });
                  }else{
                      //Guardamos la sesion de datos aca
                      this.storage.remove('ID');
                      this.storage.remove('USERNAME');
                      this.storage.remove('NOMBRES');
                      this.storage.remove('APELLIDOS');
                      this.storage.remove('TOKEN');
                      this.storage.remove('OPEN');
                      this.storage.set('REGISTRADO', response.nombre);
                      localStorage.setItem('REGISTRADO', response.nombre);  
                      
                      this.storage.set('CLAVE', this.myForm.value.clave);
                      localStorage.setItem('CLAVE', this.myForm.value.clave);  
                      
                      this.navCtrl.setRoot('RegistroendPage');
                      
                  }//Fin else 
              });//FIN POST
   },error => {
            let alerta = this.alertCtrl.create({
                  title: "Aviso",
                  message: "Disculpe, no se pudo conectar al servidor",
                  buttons: [
                    {
                        text: "Reintentar",
                        role: 'cancel',
                        cssClass:'ion-aceptar',
                        handler: data => {
                          this.loading.dismiss().then( () => {this.nav.pop();});
                        }
                    }
                  ]
                });
                alerta.present();
    });//FIN POST
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: false,
    });
    this.loading.present();
  }


}
