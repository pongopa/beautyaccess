import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisreservasPage } from './misreservas';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    MisreservasPage
  ],
  imports: [
    IonicPageModule.forChild(MisreservasPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    MisreservasPage
  ]
})
export class MisreservasPageModule {}
