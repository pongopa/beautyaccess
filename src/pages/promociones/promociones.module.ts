import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromocionesPage } from './promociones';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    PromocionesPage
  ],
  imports: [
    IonicPageModule.forChild(PromocionesPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    PromocionesPage
  ]
})
export class PromocionesPageModule {}
