import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Promociones } from '../../providers/servicio/promociones';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Ionic2RatingModule } from 'ionic2-rating';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-promociones',
  templateUrl: 'promociones.html',
  providers:[Promociones, GlobalVars, Principal]
})
export class PromocionesPage {
    usuario = "";
    usuarioid = "";
    historiaid = "";
    data  = "";
    fecha = "";
    dat  = 0;
    promocionid = 0;

    searchTerm:any="";
    allItems:any;
    items:any="";
    data_empresa:any="";
    contador_empresa = 0;
    public loading:Loading;

    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    constructor(
              public nav: Nav, 
              public navCtrl: NavController, 
              private navController: NavController, 
              public service: Promociones,
              public  alertCtrl: AlertController,
              public _service: Principal,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public navParams: NavParams) {

  }
  cargar(v, c){
   this.nav.push(v, { id : c});
  }
  recargar(codigo){
    this.service.Recargasgifcard(codigo, this.usuarioid, 1 ).subscribe((response) => {  
                  this.loading.dismiss().then( () => {
                            let alert = this.alertCtrl.create({
                              title: response.msgh,
                              message: response.msg,
                              buttons: [
                                {
                                  text: 'Aceptar',
                                  cssClass:'ion-aceptar',
                                  handler: data => {
                                      //this.nav.push("RecargasPage", {});
                                       this.nav.pop();
                                  }
                                }
                            ]
                            });
                            alert.present();
                  });
        },error => {
                let alerta = this.alertCtrl.create({
                      title: "Aviso",
                      message: "Disculpe, no se pudo conectar al servidor",
                      buttons: [
                        {
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                              this.loading.dismiss().then( () => {this.nav.pop();});
                            }
                        }
                      ]
                    });
                    alerta.present();
        });//FIN POST
        this.loading = this.loadingCtrl.create({
        dismissOnPageChange: false,
      });
      this.loading.present();
  }
  onEvent(identificador, codigo){


       let alert2 = this.alertCtrl.create({
                                      title: "!!Aviso de promoción!!",
                                      message: "Desea canjear este código en el siguiente centro",
                                      buttons: [
                                        {
                                          text: 'No',
                                          cssClass:'ion-cancelar',
                                          role: 'cancel',
                                          handler: data => {}
                                        },
                                        {
                                          text: 'Si',
                                          cssClass:'ion-aceptar',
                                          handler: data => {
                                                        this.service.Recargasgifcard(codigo, this.usuarioid, 1 ).subscribe((response) => {  
                                                                      this.loading.dismiss().then( () => {
                                                                                let alert = this.alertCtrl.create({
                                                                                  title: response.msgh,
                                                                                  message: response.msg,
                                                                                  buttons: [
                                                                                    {
                                                                                      text: 'Aceptar',
                                                                                      cssClass:'ion-aceptar',
                                                                                      handler: data => {
                                                                                          //this.nav.push("RecargasPage", {});
                                                                                           this.nav.push("SalonPage", { id : identificador});
                                                                                      }
                                                                                    }
                                                                                ]
                                                                                });
                                                                                alert.present();
                                                                      });
                                                            },error => {
                                                                    let alerta = this.alertCtrl.create({
                                                                          title: "Aviso",
                                                                          message: "Disculpe, no se pudo conectar al servidor",
                                                                          buttons: [
                                                                            {
                                                                                text: "Reintentar",
                                                                                role: 'cancel',
                                                                                cssClass:'ion-aceptar',
                                                                                handler: data => {
                                                                                  this.loading.dismiss().then( () => {this.nav.pop();});
                                                                                }
                                                                            }
                                                                          ]
                                                                        });
                                                                        alerta.present();
                                                            });//FIN POST
                                                            this.loading = this.loadingCtrl.create({
                                                            dismissOnPageChange: false,
                                                          });
                                                          this.loading.present();
                                          }
                                        }
                                    ]
                                    });
                                    alert2.present();




  }
  ionViewWillEnter() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario     = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid   = localStorage.getItem('ID');
            this.promocionid = this.navParams.get('id');
            this.urlimg2     = this.urlimg.getMyserve();
            this.service.views(this.promocionid).subscribe((response) => { 
              this.loading.dismiss().then( () => {
                      if (response.code!=200){

                      }else{
                            this.data         = response.datos;//publicidad
                            this.data_empresa = response.data2;//publicidad
                            this.contador_empresa = response.contar_empresa;//publicidad
                            console.log(response.datos);
                            console.log(response.data2);
                            console.log(response.contar_empresa); 
                            if(this.navCtrl.getPrevious().name != "PrincipalPage"){
                                  //this.navCtrl.remove(this.navCtrl.getPrevious().index);
                                  //this.nav.pop();
                                //this.navCtrl.remove(this.navCtrl.getActive().index);
                                
                            }

                      }
                });
              },error => {
                        let alerta = this.alertCtrl.create({
                              title: "Aviso",
                              message: "Disculpe, no se pudo conectar al servidor",
                              buttons: [
                                {
                                    text: "Reintentar",
                                    role: 'cancel',
                                    cssClass:'ion-aceptar',
                                    handler: data => {
                                      this.loading.dismiss().then( () => {this.nav.pop();});
                                    }
                                }
                              ]
                            });
                            alerta.present();
                });//FIN POST
              this.loading = this.loadingCtrl.create({
                  dismissOnPageChange: false,
                  content: 'Cargando... '
              });
              this.loading.present();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     //this.nav.push(componente, { id : p1});
     //this.navCtrl.remove(this.navCtrl.getActive().index);
     this.nav.pop();
    }
  }//fin function
}//fin componente 
