import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisreservasverPage } from './misreservasver';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    MisreservasverPage
  ],
  imports: [
    IonicPageModule.forChild(MisreservasverPage),
    MyNavbarModule
  ],
  exports: [
    MisreservasverPage
  ]
})
export class MisreservasverPageModule {}
