import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeautytipsviewsPage } from './beautytipsviews';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    BeautytipsviewsPage
  ],
  imports: [
    IonicPageModule.forChild(BeautytipsviewsPage),
    MyNavbarModule
  ],
  exports: [
    BeautytipsviewsPage
  ]
})
export class BeautytipsviewsPageModule {}
