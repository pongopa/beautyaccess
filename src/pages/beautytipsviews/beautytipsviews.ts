import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Beautytips } from '../../providers/servicio/beautytips';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { IonicPage, NavController,Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-beautytipsviews',
  templateUrl: 'beautytipsviews.html',
  providers:[Beautytips, GlobalVars]
})
export class BeautytipsviewsPage {
myForm: FormGroup;
salonid: any;
public loading:Loading;
public usuario = "";
public datos: any;
public contentString = "";
public contentStringT = "";
public contador = 0;
public valor    = 0;
public result   = 0;

public user_id ="";

@ViewChild('wizardSlider2') slider2: Slides;
sliderOptions2 = { pager2: true };
path2:boolean = false;
prev2:boolean = true;
next2:boolean = true;
finish2:boolean = true;

public titulo = "";
public descripcion       = "";
public descripcion_corta = "";
public url       = "";
public tipo_url  = "";
public url_imagen = "";

public img1     = "";
public img2     = "";
public img3     = "";
public img4     = "";
public img5     = "";
public img6     = "";
public img7     = "";
public img8     = "";
public img9     = "";
public urlimg2  = "";
public urlimg   = new GlobalVars();

  constructor(public nav: Nav,
              public navController: NavController,
              public alertCtrl: AlertController, 
              public nave: Nav,  
              private _service: Beautytips,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public formBuilder: FormBuilder,
              public navParams: NavParams) {
    this.myForm = this.formBuilder.group({
      texto: ['', Validators.required]
    });
  }
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            //this.navCtrl.setRoot('HomePage');
            this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
            this.salonid = this.navParams.get('id');
            this.loadpage();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");

            localStorage.clear();
        }
  }
  comentario(){
    this.nav.push("ComentarioPage", { id : this.salonid});
  }
  reservafechahora(){
    this.nav.push("ReservafechahoraPage", { id : this.salonid});
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  changeSlide2(index: number): void {
        if (index > 0) {
            this.slider2.slideNext(300);
        } else {
            this.slider2.slidePrev(300);
        }
  }
  slideHasChanged2(index: number): void {
        try {
            this.prev2   = !this.slider2.isBeginning();
            this.next2   = this.slider2.getActiveIndex() < (this.slider2.length() - 1);
            this.finish2 = this.slider2.isEnd();
        } catch (e) { }
  }
  loadpage(){
      this._service.beautypointviews(this.salonid).subscribe((response) => { 
              this.loading.dismiss().then( () => { 
                            if (response.code==200){
                              this.datos        = response['datos'];
                              this.titulo             = this.datos[0]['titulo'];
                              this.descripcion_corta  = this.datos[0]['descripcion_corta'];
                              this.url                = this.datos[0]['url'];
                              this.descripcion        = this.datos[0]['descripcion'];
                              this.tipo_url           = this.datos[0]['tipo_url'];
                              this.url_imagen           = this.datos[0]['url_imagen'];
                              this.urlimg2  = this.urlimg.getMyserve();
                              
                                                             
                      }else{
                        
                      }//Fin else
            });
      },error => {
            let alerta = this.alertCtrl.create({
                  title: "Aviso",
                  message: "Disculpe, no se pudo conectar al servidor",
                  buttons: [
                    {
                        text: "Reintentar",
                        role: 'cancel',
                        cssClass:'ion-aceptar',
                        handler: data => {
                          this.loading.dismiss().then( () => {this.nav.pop();});
                        }
                    }
                  ]
                });
                alerta.present();
        });//FIN POST
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: false,
        content: 'Cargando...'
      });
      this.loading.present();
  }
 
}//fin componente 
