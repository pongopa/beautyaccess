import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Recuperar3Page } from './recuperar3';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    Recuperar3Page,
  ],
  imports: [
    IonicPageModule.forChild(Recuperar3Page),
    Ionic2RatingModule
  ],
  exports: [
    Recuperar3Page
  ]
})
export class Recuperar3PageModule {}
