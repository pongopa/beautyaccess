import { Component } from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic';
import { ToastController, IonicPage, NavController,LoadingController, Loading, AlertController, Nav} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
//import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { Login } from '../../providers/servicio/login';
import { Verifica } from '../../providers/servicio/verifica';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-recuperar3',
  templateUrl: 'recuperar3.html',
  providers:[Login, Diagnostic, Verifica]
})
export class Recuperar3Page {

        myForm: FormGroup;
       // user: Observable<firebase.User>;
        public loading:Loading;
        public registro:Login;
        public email = "";

        constructor(
          public navCtrl: NavController,
          public nav: Nav,
          public navController: NavController,
          public formBuilder: FormBuilder,
          public afAuth: AngularFireAuth,
          public alertCtrl: AlertController,
          public loadingCtrl: LoadingController,
          private _service: Login,
          private _service_ve: Verifica,
          public storage: Storage,
          private diagnostic: Diagnostic,
          private toastCtrl: ToastController
        ) {
              this.myForm = this.formBuilder.group({
                  code_verificacion: ['', Validators.required],
                  code_verificacion2: ['', Validators.required]
              });
             
        }//fin constructor
        mapa(){
              this.navController.setRoot('InicioPage');
        }//fin function
        perfil(){
            this.nav.push('PerfilPage');
        }//fin function
        ionViewDidLoad() {
              if(localStorage.getItem('OPEN') == 'true'){
                  this.navCtrl.setRoot('HomePage');

              }else{
                  this.storage.remove('ID');
                  this.storage.remove('USERNAME');
                  this.storage.remove('NOMBRES');
                  this.storage.remove('APELLIDOS');
                  this.storage.remove('TOKEN');
                  this.storage.remove('OPEN');
                  this.storage.set('OPEN', 'false');
                  localStorage.setItem('OPEN','false');
                  //this.navController.setRoot("VerificarPage");
                  if(localStorage.getItem('INICIO')=='true'){
                  }else{
                         this.navController.push("Guia");
                  }
              }
        }
        enviarpassw(){
                this.email = localStorage.getItem('EMAIL');
                if(this.myForm.value.code_verificacion==this.myForm.value.code_verificacion2){
                                  this._service.recuperar3(this.email, this.myForm.value).subscribe((response) => {  
                                            if (response.code!=200){
                                                        this.loading.dismiss().then( () => {
                                                              let alert = this.alertCtrl.create({
                                                                message: response.msg,
                                                                buttons: [
                                                                  { 
                                                                    text: "Ok", 
                                                                    role: 'cancel'
                                                                  }
                                                                ]
                                                              }); 
                                                              alert.present();
                                                        });
                                            }else{
                                                      if(response.opcion==1){
                                                             this.loading.dismiss().then( () => {
                                                                    let alert = this.alertCtrl.create({
                                                                        title: "Actualización de contraseña",
                                                                        message: "La contraseña ha sido modificada con éxito",
                                                                        buttons: [
                                                                              {
                                                                                text: 'Aceptar',
                                                                                cssClass:'ion-aceptar',
                                                                                handler: data => {
                                                                                  this.navController.setRoot("VerificarPage");
                                                                                }
                                                                              }
                                                                            ],
                                                                    });
                                                                    alert.present();
                                                              });
                                                      }else{
                                                        this.loading.dismiss().then( () => {
                                                              let alert = this.alertCtrl.create({
                                                                message: response.msg,
                                                                buttons: [
                                                                  { 
                                                                    text: "Ok", 
                                                                    role: 'cancel'
                                                                  }
                                                                ]
                                                              }); 
                                                              alert.present();
                                                        });
                                                      }//fin else
                                            }//fin else
                                  },error => {
                                          let alerta = this.alertCtrl.create({
                                                title: "Aviso",
                                                message: "Disculpe, no se pudo conectar al servidor",
                                                buttons: [
                                                  {
                                                      text: "Reintentar",
                                                      role: 'cancel',
                                                      cssClass:'ion-aceptar',
                                                      handler: data => {
                                                        this.loading.dismiss().then( () => {this.nav.pop();});
                                                      }
                                                  }
                                                ]
                                              });
                                              alerta.present();
                                  });//FIN POST
                                  this.loading = this.loadingCtrl.create({
                                    dismissOnPageChange: false,
                                  });
                                  this.loading.present();
                }else{
                                  let alert = this.alertCtrl.create({
                                      message: "Las contraseñas no son iguales.",
                                      buttons: [
                                        { 
                                          text: "Ok", 
                                          role: 'cancel'
                                        }
                                      ]
                                  }); 
                                  alert.present();
                }//fin else
                
        }//fin function
}//FIN CLASS
