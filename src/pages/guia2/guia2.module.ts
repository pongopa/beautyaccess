import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Guia2 } from './guia2';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    Guia2,
  ],
  imports: [
    IonicPageModule.forChild(Guia2),
    Ionic2RatingModule
  ],
  exports: [
    Guia2
  ]
})
export class Guia2Module {}
