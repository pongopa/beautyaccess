import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalonPage } from './salon';
import { ComponentsExpandableComponent } from "../../components/components-expandable/components-expandable";
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    SalonPage,
    ComponentsExpandableComponent,
  ],
  imports: [
    IonicPageModule.forChild(SalonPage),
    MyNavbarModule
  ],
  exports: [
    SalonPage
  ]
})
export class SalonPageModule {}
