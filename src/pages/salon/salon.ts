import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Salon } from '../../providers/servicio/salon';
import { Storage } from '@ionic/storage';
import { Principal } from '../../providers/servicio/principal';
import { GlobalVars } from '../../providers/servicio/global';
import { IonicPage, NavController, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';

import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { Geolocation, Geoposition } from "@ionic-native/geolocation";

@IonicPage()
@Component({
  selector: 'page-salon',
  templateUrl: 'salon.html',
  providers:[Salon, GlobalVars, Principal, Geolocation, LaunchNavigator]
})
export class SalonPage {
myForm: FormGroup;
salonid: any;
public usuarioid = "";
public user_id = "";
public data6 = "";
public loading:Loading;
public loading2:Loading;
public usuario = "";
public datos: any;
public contentString = "";
public contentStringT = "";
public contentString_ = 0;
public contador:number = 0;
public valor:number = 0;
public result:any = 0;

public razon_social = "";
public email        = "";
public direccion    = "";
public telefono     = "";

public img1     = "";
public img2     = "";
public img3     = "";
public urlimg2  = "";
public urlimg   = new GlobalVars();

public itemHeight: number = 0;
public tiposervicio:any = 0;

public latitude:number ;
public longitude:number ;

public ver      = 0;
constructor(public nav: Nav,
              public navController: NavController,
              public alertCtrl: AlertController, 
              public nave: Nav,  
              public launchNavigator: LaunchNavigator,
              public geolocation: Geolocation,
              private _service: Salon,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public _principal: Principal,
              public formBuilder: FormBuilder,
              public navParams: NavParams) {
        this.myForm = this.formBuilder.group({
          texto: ['', Validators.required]
        });
  }//fin constructor
  expandItem(item){
      this.tiposervicio.map((listItem) => {
          if(item == listItem){
              listItem.expanded = !listItem.expanded;
          } else {
              listItem.expanded = false;
          }
          return listItem;
      });
  }
  link(componente, p1){ 
    if(componente!=""){
      if(componente=="ReservafechahoraPage"){
              this._service.Datos(this.salonid, this.user_id).subscribe((response) => { 
                          this.loading2.dismiss().then( () => {
                                if (response.code2==201){
                                      let alert_fecha = this.alertCtrl.create({ 
                                        title: "¡Alerta BeautyAccess!",
                                        message: "Debe completar su perfil para poder reservar",
                                        buttons: [
                                          {
                                                text: 'No',
                                                role: 'cancel',
                                                cssClass:'ion-cancelar',
                                                handler: data => {

                                                }
                                          },
                                          {
                                                text: 'Aceptar',
                                                cssClass:'ion-aceptar',
                                                handler: data => { this.nav.push('PerfilPage', { idsalon : p1});}
                                          }
                                      ]
                                      });
                                      alert_fecha.present();
                                }else{
                                      this.nav.push(componente, { id : p1});
                                }
                          });    
              },error => {
                          let alerta = this.alertCtrl.create({
                                title: "Aviso",
                                message: "Disculpe, no se pudo conectar al servidor",
                                buttons: [
                                  {
                                      text: "Reintentar",
                                      role: 'cancel',
                                      cssClass:'ion-aceptar',
                                      handler: data => {
                                        this.loading2.dismiss().then( () => {this.nav.pop();});
                                      }
                                  }
                                ]
                              });
                              alerta.present();
              });//FIN POST
              this.loading2 = this.loadingCtrl.create({
                dismissOnPageChange: false,
                content: 'Cargando...'
              });
              this.loading2.present();
      }else{
        this.nav.push(componente, { id : p1});
      }
    }
  }//fin function
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            //this.navCtrl.setRoot('HomePage');
            this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
            this.ver = this.navParams.get('id');
            if(this.ver!=null){
              this.storage.set('salonid', this.navParams.get('id'));
              localStorage.setItem('salonid',this.navParams.get('id'));
            }
            this.salonid    = localStorage.getItem('salonid');
            this.usuarioid  = localStorage.getItem('ID');
            this._principal.productosreservas(this.usuarioid, this.salonid).subscribe((response) => { 
                    this.loading2.dismiss().then( () => {
                          if (response.code!=200){ 

                          }else{
                              this.data6     = response.datos6;
                             
                            
                          }
                    });
            },error => {
                      let alerta = this.alertCtrl.create({
                            title: "Aviso",
                            message: "Disculpe, no se pudo conectar al servidor",
                            buttons: [
                              {
                                  text: "Reintentar",
                                  role: 'cancel',
                                  cssClass:'ion-aceptar',
                                  handler: data => {
                                    this.loading2.dismiss().then( () => {this.nav.pop();});
                                  }
                              }
                            ]
                          });
                          alerta.present();
            });//FIN POST
            this.loading2 = this.loadingCtrl.create({
              dismissOnPageChange: false,
              content: 'Cargando...'
            });
            this.loading2.present();
            
            this.loadpage();
           
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");

            localStorage.clear();
        }

  }
  navigateLocation(){
      let options: LaunchNavigatorOptions = {
        app: this.launchNavigator.APP.GOOGLE_MAPS
      };
      this.launchNavigator.navigate([this.latitude,this.longitude], options).then(success =>{
        console.log(success);
      },error=>{
        console.log(error);
      });
  }
  comentario(){
    this.nav.push("ComentarioPage", { id : this.salonid});
  }
  reservafechahora(){
    this.nav.push("ReservafechahoraPage", { id : this.salonid});
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  loadpage(){
      this.user_id = localStorage.getItem('ID');
      this._service.Datos(this.salonid, this.user_id).subscribe((response) => { 
                  this.loading.dismiss().then( () => {
                                if (response.code==200){
                                  this.datos        = response['datos'];
                                  this.tiposervicio = response['datos'][0].servicios[0].deno_tiposer;
                                  this.razon_social = this.datos[0]['razon_social'];
                                  this.email        = this.datos[0]['email'];
                                  this.direccion    = this.datos[0]['direccion'];
                                  this.telefono     = this.datos[0]['telefono'];

                                  this.latitude  = this.datos[0]['latitud'];
                                  this.longitude = this.datos[0]['longitud'];

                                  console.log('latitude: '+this.latitude);
                                  console.log('longitude: '+this.longitude);

                                  if(this.datos[0]['foto1']==""){this.datos[0]['foto1']="upload/default.png";}
                                  if(this.datos[0]['foto2']==""){this.datos[0]['foto2']="upload/default.png";}
                                  if(this.datos[0]['foto3']==""){this.datos[0]['foto3']="upload/default.png";}

                                  this.img1     = this.datos[0]['foto1'];
                                  this.img2     = this.datos[0]['foto2'];
                                  this.img3     = this.datos[0]['foto3'];
                                  this.urlimg2  = this.urlimg.getMyserve();

                                  
                                  console.log(response['datos']);
                                  console.log(response['datos2']);
                                  console.log(this.razon_social);

                                  let miscali  = response['datos2']; 

                                  this.contentString = '<div item-capa>' +  
                                                          '<table border="0"  width="90%">';
                                                          miscali.forEach(valores2 => {
                                                              this.contentString_ = this.contentString_ + 1;
                                                              this.contador=this.contador+1;
                                                              this.valor=this.valor+valores2.valor;
                                                              this.contentString +='<tr>'+
                                                                              '<td class="calificacion_ver"><span class="item-title">'+valores2.deno+':</span></td>'+
                                                                      '<td>';   if(valores2.valor<1){
                                                                                      if(valores2.valor<0.5){
                                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                                               }else{
                                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                                               }
                                                                          }else if(valores2.valor<2){
                                                                                      if(valores2.valor<1.5){
                                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                                               }else{
                                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                                               }
                                                                          }else if(valores2.valor<3){
                                                                                    if(valores2.valor<2.5){
                                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                                               }else{
                                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                                               }
                                                                          }else if(valores2.valor<4){
                                                                                    if(valores2.valor<3.5){
                                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                                               }else{
                                                                                     this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                                               }
                                                                          }else if(valores2.valor<5){
                                                                                   if(valores2.valor<4.5){
                                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-12.png"></img>';
                                                                               }else{
                                                                                    this.contentString +=  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-11.png"></img>';
                                                                               }
                                                                          }else{
                                                                           this.contentString += '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>' +
                                                                                  '<img class="margin-estrellas" weight="18px" height="18px" src="./assets/images/iconos-10.png"></img>';
                                                                          }
                                                      this.contentString +='</td>'+
                                                                      '</tr>';
                                                          });
                                            this.contentString += '</table><br>'+
                                                           '</div>'; 

                                            this.result = Number(this.valor/this.contador).toFixed(2);
                                            if(isNaN(this.result)){this.result=0;}
                                            this.contentStringT = '<div>' + 
                                                          '<table border="0">';
                                                              this.contentStringT +='<tr>'+
                                                                      '<td><b class="calificacion" style="">'+this.result+'</b></td>'+
                                                                      '<td>';   if(this.result<1){
                                                                                      if(this.result<0.5){
                                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                                               }else{
                                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-11.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                                               }
                                                                          }else if(this.result<2){
                                                                                      if(this.result<1.5){
                                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                                               }else{
                                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-11.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                                               }
                                                                          }else if(this.result<3){
                                                                                    if(this.result<2.5){
                                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                                               }else{
                                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-11.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                                               }
                                                                          }else if(this.result<4){
                                                                                    if(this.result<3.5){
                                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                                               }else{
                                                                                     this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-11.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                                               }
                                                                          }else if(this.result<5){
                                                                                   if(this.result<4.5){
                                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-12.png"></img>';
                                                                               }else{
                                                                                    this.contentStringT +=  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                          '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-11.png"></img>';
                                                                               }
                                                                          }else{
                                                                           this.contentStringT += '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>' +
                                                                                  '<img class="margin-estrellas" weight="14px" height="14px" src="./assets/images/iconos-10.png"></img>';
                                                                          }
                                                      this.contentStringT +='</td>'+
                                                                      '</tr>';
                                            this.contentStringT += '</table><br>'+
                                                           '</div>';                                            
                          }else{
                            
                          }//Fin else
                  });//fin loading
      },error => {
                  let alerta = this.alertCtrl.create({
                        title: "Aviso",
                        message: "Disculpe, no se pudo conectar al servidor",
                        buttons: [
                          {
                              text: "Reintentar",
                              role: 'cancel',
                              cssClass:'ion-aceptar',
                              handler: data => {
                                this.loading.dismiss().then( () => {this.nav.pop();});
                              }
                          }
                        ]
                      });
                      alerta.present();
      });//FIN POST
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: false,
        content: 'Cargando...'
      });
      this.loading.present();
  }
 
}//fin componente 
