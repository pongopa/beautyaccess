import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeautytipsPage } from './beautytips';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    BeautytipsPage
  ],
  imports: [
    IonicPageModule.forChild(BeautytipsPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    BeautytipsPage
  ]
})
export class BeautytipsPageModule {}
