import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Recuperar2Page } from './recuperar2';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    Recuperar2Page,
  ],
  imports: [
    IonicPageModule.forChild(Recuperar2Page),
    Ionic2RatingModule
  ],
  exports: [
    Recuperar2Page
  ]
})
export class Recuperar2PageModule {}
