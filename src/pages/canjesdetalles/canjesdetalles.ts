import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Canjes } from '../../providers/servicio/canjes';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Ionic2RatingModule } from 'ionic2-rating';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
     selector: 'page-canjesdetalles',
  templateUrl: 'canjesdetalles.html',
  providers:[Canjes, GlobalVars, Principal]
})
export class CanjesdetallesPage {
    usuario = "";
    usuarioid = "";
    historiaid = "";
    data  = "";
    data2 = "";
    fecha = "";
    dat  = 0;
    mensaje = "";

    searchTerm:any="";
    allItems:any;
    items:any="";


    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    constructor(
              public nav: Nav, 
              public navCtrl: NavController, 
              public alertCtrl: AlertController, 
              public service: Canjes,
              public storage: Storage,
              private navController: NavController, 
              public navParams: NavParams) {

  }
  cargar(v, c){
   this.nav.push(v, { id : c});
  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid = localStorage.getItem('ID');
            this.urlimg2   = this.urlimg.getMyserve();
            this.service.list().subscribe((response) => { 
              if (response.code!=200){

              }else{
                  this.data2 = response.datos2;//publicidad
              }
            });
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     //this.nav.push(componente, { id : p1});
     this.navCtrl.remove(this.navCtrl.getActive().index);
    }
  }//fin function
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
}//fin componente 
