import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CanjesdetallesPage } from './canjesdetalles';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    CanjesdetallesPage
  ],
  imports: [
    IonicPageModule.forChild(CanjesdetallesPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    CanjesdetallesPage
  ]
})
export class CanjesdetallesPageModule {}
