import { Component, ViewChild } from '@angular/core';
import { IonicPage, AlertController, NavController, MenuController, Nav, ModalController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoginPage } from '../../pages/login/login';
import { Login } from '../../providers/servicio/login';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[Login]
})
export class HomePage {
  @ViewChild(Nav) nav: Nav; 
  //@ViewChild(NavController) navController: NavController; 
  //rootPage = 'InicioPage';
  rootPage = 'PrincipalPage';
  public appPages = [
    {
      title: 'Inicio',
      url: '/inicio',
      icon: 'home',
      componente: 'PrincipalPage',
    },
    {
      title: 'Perfil', 
      url: '/perfil',
      icon: 'person',
      componente: 'PerfilPage',
    },
    {
      title: 'Contáctanos',
      url: '/contactanos',
      icon: 'mail',
      componente: 'ContactanosPage',
    },
    {
      title: 'Comparte y Gana',
      url: '/compartir',
      icon: 'git-branch',
      componente: 'CompartirPage',
    },
    {
      title: 'Giftcard y Códigos promocionales',
      url: '/recargas',
      icon: 'ribbon',
      componente: 'RecargasgifcardPage',   
    },
    {
      title: 'Métodos de Pagos',
      url: '/recargas',
      icon: 'cash',
      componente: 'RecargastargetasPage',   
    },
    {
      title: 'Información', 
      url: '/informativo',
      icon: 'information-circle',
      componente: 'InformativoPage',
    },
  ];
  public usuario = "";
  public push_token = "";
  public push_paltf = "";
  public usuarioid  = "";
  constructor(public  afAuth: AngularFireAuth, 
              public alertCtrl: AlertController, 
              public storage: Storage, 
              private _login: Login,
              public navController: NavController,
              public menuCtrl: MenuController
              ) { }

  ionViewDidLoad() { 
       //if(localStorage.getItem('OPEN') == 'true'){
             //this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
             //this.usuarioid  = localStorage.getItem('ID');
             this.push_token = localStorage.getItem('PUSH_TOKEN');
             this.push_paltf = localStorage.getItem('PUSH_PLATFORM');
       /* 
        }else{
              this.storage.remove('ID');
              this.storage.remove('USERNAME');
              this.storage.remove('NOMBRES');
              this.storage.remove('APELLIDOS');
              this.storage.remove('TOKEN');
              this.storage.remove('OPEN');
              this.storage.set('OPEN', 'false');
              localStorage.setItem('OPEN','false');
              this.navController.setRoot("VerificarPage");
        }
        */
  }
  logOut(){
    this.storage.remove('ID');
    this.storage.remove('USERNAME');
    this.storage.remove('NOMBRES');
    this.storage.remove('APELLIDOS');
    this.storage.remove('TOKEN');
    this.storage.set('OPEN', 'false');
    localStorage.setItem('OPEN','false');
    this.navController.setRoot("VerificarPage");
  }
  openPage2() {
      window.open('whatsapp://send?phone=+593988488913', '_system');
  }//fin
  openPage(pages) {
    if(localStorage.getItem('OPEN') == 'true'){ 
        if(pages=="PrincipalPage"){
          //this.navController.setRoot('PrincipalPage');
          //this.navController.setRoot('PrincipalPage');
          this.nav.push(pages); 
        }else{
           this.nav.push(pages);  
        }
    }else{
          this.nav.push("VerificarPage");
    }
  }
  link(componente, p1){ 
      //console.log(p1);
      if(componente!=""){
        if(localStorage.getItem('OPEN') == 'true'){ 
           this.nav.push(componente, { id : p1});
        }else{
          this.nav.push("VerificarPage");
        }
      }
  }//fin function
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
}//fin function
