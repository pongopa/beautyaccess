import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Historia } from '../../providers/servicio/historia';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Ionic2RatingModule } from 'ionic2-rating';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams, ModalController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-historialista',
  templateUrl: 'historialista.html',
  providers:[Historia, GlobalVars, Principal]
})
export class HistorialistaPage {
    usuario = "";
    usuarioid = "";
    historiaid = "";
    data = "";
    dat  = 0;
    searchTerm:any="";
  allItems:any;

    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    constructor(
              public nav: Nav, 
              public navCtrl: NavController, 
              private navController: NavController, 
              public service: Historia,
              public _service: Principal,
              public storage: Storage,
              public modalCtrl : ModalController,
              public navParams: NavParams) {

  }
  cargar(v, c){
   this.nav.push(v, { id : c});
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  ionViewWillEnter() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario   = localStorage.getItem('NOMBRESAPELLIDOS');
            this.urlimg2 = this.urlimg.getMyserve();
                      this._service.historialista().subscribe((response) => { 
                            if (response.code!=200){
                            }else{
                              console.log(response);
                              this.data = response.datos1;//publicidad
                              //this.navCtrl.remove(this.navCtrl.getPrevious().index);
                                 if(this.navCtrl.getPrevious().name != "PrincipalPage"){
                                    this.navCtrl.remove(this.navCtrl.getPrevious().index);
                                  }
                            }
                      });
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  principal(){
    //this.nav.push('PrincipalPage');
    this.navCtrl.setRoot('PrincipalPage');
  }
  getItems(event: any):void {
      if (!this.allItems) {
        this.allItems = this.data;
      }
      this.data = this.allItems.filter((item) => {
          return item.nombre_apellido.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
      });
      
  }
  onEvent(event:string, item:any) {//ITEM [EVENT OR SELECTED ITEM]
      //if (this.events[event]) {
        if ('onTextChange' === event) {
          this.getItems(item);
          //this.events[event](this.searchTerm);
        } else {
          //this.events[event](item);
          this.nav.push("HistoriaPage", { id : item.id});
        }
      //}
      console.log(event);
  }
  
}//fin componente 
