import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistorialistaPage } from './historialista';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    HistorialistaPage
  ],
  imports: [
    IonicPageModule.forChild(HistorialistaPage),
    MyNavbarModule
  ],
  exports: [
    HistorialistaPage
  ]
})
export class HistorialistaPageModule {}
