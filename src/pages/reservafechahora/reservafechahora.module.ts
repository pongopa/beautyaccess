import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReservafechahoraPage } from './reservafechahora';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

import { CalendarModule } from 'ionic3-calendar-en';

@NgModule({
  declarations: [
    ReservafechahoraPage
  ],
  imports: [
    IonicPageModule.forChild(ReservafechahoraPage),
    MyNavbarModule,
    CalendarModule
  ],
  exports: [
    ReservafechahoraPage
  ]
})
export class ReservafechahoraPageModule {}
