import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Salon } from '../../providers/servicio/salon';
import { Reservas } from '../../providers/servicio/reservas';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Events } from 'ionic-angular';
import { IonicPage, NavController, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-reservamodificarhora',
  templateUrl: 'reservamodificarhora.html',
  providers:[Salon, GlobalVars, Reservas]
})
export class ReservamodificarhoraPage {

  @Input()
  ver_hora: number = 0;

salonid: any;
usuario = "";
//f       = ""
//fecha   = "";
//h       = "";
//hora    = "";
end     = "";
mesa    = 0;
mesb    = "";
diaa    = 0;
diab    = "";

horaa:number   = 0;
horab   = "";
minta   = 0;
mintb   = "";

h2     = new Date();
hora2  = "";
horaa2   = 0;
horab2   = "";
minta2   = 0;
mintb2   = "";
idreserva = "";

public user_id = "";

h     = new Date();
hora  = "";
f     = new Date();
fecha = "";
public fecha_:any = [];


public loading:Loading;
public loading2:Loading;
public loading3:Loading;
public datos: any;
public datos2: any;
public contentString = "";
public contentStringT = "";
public contador:number = 0;
public valor:number = 0;
public result:any = 0;
public ver      = 0;

public id1 = "";
public id2 = "";

public razon_social = null;
public email        = "";
public direccion    = "";
public telefono     = "";

public img1     = "";
public img2     = "";
public img3     = "";
public urlimg2  = "";
public urlimg   = new GlobalVars();

public punto = "";

public disabled = 2;
public hora_:any = [];

constructor(public nav: Nav,
              private _service: Salon,
              public storage: Storage,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
              private navController: NavController,
              public navParams: NavParams,
              public ser_reserva: Reservas,
              private events: Events) {

 }/** Returns a value as a string value appropriate to the host environment's current locale. */
    /** Used by the JSON.stringify method to enable the transformation of an object's data for JavaScript Object Notation (JSON) serialization. */
  //validar(event){
    validar(vari, vari2){
          this.hora = vari;
          this.horaa2 = this.h2.getHours();
          if(this.horaa2<=9){ this.horab2 = '0'+this.horaa2;}else{this.horab2 = ''+this.horaa2;}
          this.minta2 = this.h2.getMinutes();
          if(this.minta2<=9){ this.mintb2 = '0'+this.minta2;}else{this.mintb2 = ''+this.minta2;}
          this.hora2 = this.horab2+":"+this.mintb2;
          ////////////////////////////////////////////////
          var x=new Date();
          this.fecha_ = this.fecha.split("-");
          x.setFullYear(this.fecha_[0],this.fecha_[1]-1,this.fecha_[2]);
          var today = new Date();
          console.log('Validar '+this.hora);
          console.log('Validar2 '+this.hora2);

          console.log('Validar3 '+x);
          console.log('Validar4 '+today);

          ////////////////////////////////////////////////
          if (this.hora2 >= this.hora && x==today){
                let alert = this.alertCtrl.create({
                  title: "Validación",
                  message: "La hora es menor a la actual",
                  buttons: [
                    {
                      text: 'Aceptar',
                      cssClass:'ion-aceptar',
                      handler: data => {
                      }
                    }
                ]
                });
                alert.present();
                this.disabled = 2;
          }else{
                this._service.validar_hora_fecha(this.salonid, this.fecha, this.hora).subscribe((response) => {
                            this.loading2.dismiss().then( () => {
                                    if (response.code==200){
                                                this.disabled = 1;
                                                document.getElementById(vari2).className    = 'button-seleccion';
                                                if(this.id1!=""){
                                                  document.getElementById(this.id1).className = 'button-md_principal';
                                                }
                                                this.id1 = vari2;
                                                console.log('a'+this.id1 );
                                    }else{

                                                 let alert = this.alertCtrl.create({
                                                    title: "Validación",
                                                    message: "Para la hora seleccionada no se encuentra disponibilidad, intente de nuevamente!",
                                                    buttons: [
                                                      {
                                                        text: 'Aceptar',
                                                        cssClass:'ion-aceptar',
                                                        handler: data => {
                                                        }
                                                      }
                                                  ]
                                                  });
                                                  alert.present();
                                                  this.disabled = 2;
                                      }
                            });
                },error => {
                          let alerta = this.alertCtrl.create({
                                title: "Aviso",
                                message: "Disculpe, no se pudo conectar al servidor",
                                buttons: [
                                  {
                                      text: "Reintentar",
                                      role: 'cancel',
                                      cssClass:'ion-aceptar',
                                      handler: data => {
                                        this.loading2.dismiss().then( () => {this.nav.pop();});
                                      }
                                  }
                                ]
                              });
                              alerta.present();
                });//FIN POST
                this.loading2 = this.loadingCtrl.create({
                    dismissOnPageChange: false,
                    content: 'Cargando...'
                });
                this.loading2.present();

          }
          //.log(this.hora);
  }

  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  comentario(){
    this.nav.push("ComentarioPage", { id : this.salonid});
  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.ver = this.navParams.get('id');
            if(this.ver!=null){
              this.storage.set('salonid', this.navParams.get('id'));
              localStorage.setItem('salonid',this.navParams.get('id'));

              this.storage.set('fechareserva',    this.navParams.get('f'));
              localStorage.setItem('fechareserva',this.navParams.get('f'));

              this.storage.set('idreserva',    this.navParams.get('idreserva'));
              localStorage.setItem('idreserva',this.navParams.get('idreserva'));

              this.storage.set('punto',    this.navParams.get('punto'));
              localStorage.setItem('punto',this.navParams.get('punto'));
            }
            this.salonid    = localStorage.getItem('salonid');
            this.fecha      = localStorage.getItem('fechareserva');
            this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
            this.idreserva  = localStorage.getItem('idreserva');
            this.punto      = localStorage.getItem('punto');

            this.horaa = this.h.getHours();
            if(this.horaa<=9){ this.horab = '0'+this.horaa;}else{this.horab = ''+this.horaa;}
            this.minta = this.h2.getMinutes();
            if(this.minta<=29){
                       this.hora = this.horab+":30";
             }else{
                       if(this.horaa<=23){
                               this.horaa = this.horaa + 1;
                               if(this.horaa<=9){ this.horab = '0'+this.horaa;}else{this.horab = ''+this.horaa;}
                       }
                       this.hora = this.horab+":00";
             }
             //this.hora = "00:00";
            this.loadpage();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage"); 
        }
  }
  modificar(){ 
            this.user_id = localStorage.getItem('ID');
            this.ser_reserva.reservasupdatehora(this.idreserva, this.user_id, this.hora, this.fecha).subscribe((response) => {
                  this.loading2.dismiss().then( () => {
                     let alert = this.alertCtrl.create({
                          title: "Aviso",
                          message: "Los datos fueron modificados",
                          buttons: [
                            {
                              text: 'Aceptar',
                              cssClass:'ion-aceptar',
                              role: 'cancel',
                              handler: data => {
                                  if(this.punto=='1'){
                                         this.nav.pop();
                                  }else{
                                        // this.navController.remove(2,2);
                                        const startIndex = this.navController.getActive().index - 1;
                                        this.navController.remove(startIndex, 1);
                                        this.nav.pop();
                                  }
                              }
                            }
                          ]
                      });
                      alert.present();
                  });
            },error => {
                    let alerta = this.alertCtrl.create({
                          title: "Aviso",
                          message: "Disculpe, no se pudo conectar al servidor",
                          buttons: [
                            {
                                text: "Reintentar",
                                role: 'cancel',
                                cssClass:'ion-aceptar',
                                handler: data => {
                                  this.loading2.dismiss().then( () => {
                                      //this.nav.pop();
                                  });
                                }
                            }
                          ]
                        });
                        alerta.present();
            });//FIN POST
            this.loading2 = this.loadingCtrl.create({
                dismissOnPageChange: false,
                content: 'Cargando...'
            });
            this.loading2.present();
  }
  anterior(){
    this.nav.pop();
  } 
  loadpage(){
   //console.log(this.salonid);
   this.user_id = localStorage.getItem('ID');
      this._service.Datos(this.salonid, this.user_id).subscribe((response) => {
                this.loading3.dismiss().then( () => {
                            if (response.code==200){
                                          this._service.salon_hora_fecha(this.salonid, this.fecha).subscribe((response2) => { 
                                              this.loading.dismiss().then( () => {
                                                  console.log(response2); 
                                                  this.datos2 = response2['datos'];
                                              });
                                          },error => {
                                                    let alerta = this.alertCtrl.create({
                                                          title: "Aviso",
                                                          message: "Disculpe, no se pudo conectar al servidor",
                                                          buttons: [
                                                            {
                                                                text: "Reintentar",
                                                                role: 'cancel',
                                                                cssClass:'ion-aceptar',
                                                                handler: data => {
                                                                  this.loading.dismiss().then( () => {this.nav.pop();});
                                                                }
                                                            }
                                                          ]
                                                        });
                                                        alerta.present();
                                          });//FIN POST
                                          this.loading = this.loadingCtrl.create({
                                              dismissOnPageChange: false,
                                              content: 'Cargando...'
                                          }); 
                                          this.loading.present();
                                          this.datos        = response['datos'];

                                          this.razon_social = this.datos[0]['razon_social'];
                                          this.email        = this.datos[0]['email'];
                                          this.direccion    = this.datos[0]['direccion'];
                                          this.telefono     = this.datos[0]['telefono'];

                                          if(this.datos[0]['foto1']==""){this.datos[0]['foto1']="upload/default.png";}
                                          if(this.datos[0]['foto2']==""){this.datos[0]['foto2']="upload/default.png";}
                                          if(this.datos[0]['foto3']==""){this.datos[0]['foto3']="upload/default.png";}

                                          this.img1     = this.datos[0]['foto1'];
                                          this.img2     = this.datos[0]['foto2'];
                                          this.img3     = this.datos[0]['foto3'];
                                          this.urlimg2  = this.urlimg.getMyserve();

                                          //console.log(response['datos']);
                                          //console.log(response['datos2']);
                                          //console.log(this.razon_social);

                          }else{

                          }//Fin else
              });//fin this.loading
      },error => {
              let alerta = this.alertCtrl.create({
                    title: "Aviso",
                    message: "Disculpe, no se pudo conectar al servidor",
                    buttons: [
                      {
                          text: "Reintentar",
                          role: 'cancel',
                          cssClass:'ion-aceptar',
                          handler: data => {
                            this.loading3.dismiss().then( () => {this.nav.pop();});
                          }
                      }
                    ]
                  });
                  alerta.present();
      });//FIN POST
      this.loading3 = this.loadingCtrl.create({
        dismissOnPageChange: false,
        content: 'Cargando...'
      });
      this.loading3.present();
  }
}//fin componente
