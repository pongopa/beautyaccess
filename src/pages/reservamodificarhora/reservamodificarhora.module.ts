import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReservamodificarhoraPage } from './reservamodificarhora';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    ReservamodificarhoraPage
  ],
  imports: [
    IonicPageModule.forChild(ReservamodificarhoraPage),
    MyNavbarModule
  ],
  exports: [
    ReservamodificarhoraPage
  ]
})
export class ReservamodificarhoraPageModule {}
