import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Historia } from '../../providers/servicio/historia';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-productos',
  templateUrl: 'productos.html',
  providers:[Historia, GlobalVars, Principal]
})
export class ProductosPage {
    usuario = "";
    usuarioid = "";
    data1 = "";
    data2 = "";
    data3 = "";

    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    constructor(
              public nav: Nav, 
              public navCtrl: NavController, 
              public service: Historia,
              public _service: Principal,
              private navController: NavController, 
              public storage: Storage,
              public navParams: NavParams) {

  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){ 
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario    = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid  = localStorage.getItem('ID');
            this.urlimg2 = this.urlimg.getMyserve();
                      this._service.productos(this.usuarioid).subscribe((response) => { 
                            if (response.code!=200){

                            }else{
                              this.data1 = response.datos1.cantida_servicio;
                              this.data2 = response.datos2;
                              this.data3 = response.datos3;
                              console.log(this.data1);
                            }
                      });
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  principal(){
    //this.nav.push('PrincipalPage');
    this.navCtrl.setRoot('PrincipalPage');
  }
  
}//fin componente 
