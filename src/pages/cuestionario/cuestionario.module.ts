import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CuestionarioPage } from './cuestionario';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    CuestionarioPage

  ],
  imports: [
    IonicPageModule.forChild(CuestionarioPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    CuestionarioPage
  ]
})
export class CuestionarioPageModule {}
