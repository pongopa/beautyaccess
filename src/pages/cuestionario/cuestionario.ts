import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Cuestionario } from '../../providers/servicio/cuestionario';
import { Principal } from '../../providers/servicio/principal';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Ionic2RatingModule } from 'ionic2-rating';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-cuestionario',
  templateUrl: 'cuestionario.html',
  providers:[Cuestionario, GlobalVars, Principal]
})
export class CuestionarioPage {
    usuario = "";
    usuarioid = "";
    historiaid = "";
    data  = "";
    data2 = "";
    fecha = "";
    dat  = 0;
    promocionid = 0;

    searchTerm:any="";
    allItems:any;
    items:any="";

   public loading:Loading;
    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    constructor(
              public nav: Nav, 
              public alertCtrl: AlertController, 
              public navCtrl: NavController, 
              private navController: NavController, 
              public service: Cuestionario,
              public loadingCtrl: LoadingController,
              public _service: Principal,
              public storage: Storage,
              public navParams: NavParams) {

  }
  cargar(v, c){
   this.nav.push(v, { id : c});
  }
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario     = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid   = localStorage.getItem('ID');
            this.urlimg2     = this.urlimg.getMyserve();
            this.service.list().subscribe((response) => { 
                this.loading.dismiss().then( () => {
                      if (response.code!=200){

                      }else{
                            this.data = response.datos1;//publicidad
                            console.log(response.datos1);
                            console.log(response.datos2);

                      }
                });
            },error => {
                  let alerta = this.alertCtrl.create({
                        title: "Aviso",
                        message: "Disculpe, no se pudo conectar al servidor",
                        buttons: [
                          {
                              text: "Reintentar",
                              role: 'cancel',
                              cssClass:'ion-aceptar',
                              handler: data => {
                                this.loading.dismiss().then( () => {this.nav.pop();});
                              }
                          }
                        ]
                      });
                      alerta.present();
            });//FIN POST
            this.loading = this.loadingCtrl.create({
                dismissOnPageChange: false,
                content: 'Cargando...'
            });
            this.loading.present();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
}//fin componente 
