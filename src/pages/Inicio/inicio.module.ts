import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InicioPage } from './inicio';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    InicioPage
  ],
  imports: [
    IonicPageModule.forChild(InicioPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    InicioPage
  ]
})
export class InicioPageModule {}
