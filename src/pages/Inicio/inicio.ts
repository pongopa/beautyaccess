import { Component } from '@angular/core';
import { IonicPage, NavController,LoadingController, Loading, AlertController, Nav, MenuController, ModalController} from 'ionic-angular';
import { Geolocation, Geoposition } from "@ionic-native/geolocation";
import { GlobalVars } from '../../providers/servicio/global';
import { PointsService } from '../../providers/servicio/mapas';
import { Storage } from '@ionic/storage';
import { Deeplinks } from '@ionic-native/deeplinks';

import {InformativoPage} from '../Informativo/informativo';
import {RecargasgifcardreferidosPage} from '../recargasgifcardreferidos/recargasgifcardreferidos';

import {ViewChild} from '@angular/core';

declare var google;
@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
  providers: [Geolocation, PointsService, GlobalVars, Deeplinks]
})
export class InicioPage {

	//@ViewChild(Nav) navChild:Nav;

    map: any;
	marker: any;
	features: any;
	datas: any; 
	public marcadores: any;
	public usuariodeno: any;
	public var : any;
	public query = "";
	public loading:Loading;
	public urlimg2  = "";
    public urlimg   = new GlobalVars();

    public pais = "";
    public estado = "";
    public municipio = "";
	directionsService: any = null;
	directionsDisplay: any = null;
	bounds: any;
	calificacion: any;
	myLatLng: any;
	waypoints: any[];
	public latitude  = -0.181339;
	public longitude = -78.470433;
	public usuario = "";
	constructor(
	    public nav: Nav,
	    private navCtrl: NavController,
	    private navController: NavController, 
	    private geolocation: Geolocation,
	    public  alertCtrl: AlertController,
	    public menuCtrl: MenuController,
	    private pointsService: PointsService,
	    public storage: Storage,
	    public modalCtrl : ModalController,
	    public loadingCtrl: LoadingController,
	    private deeplinks: Deeplinks
	  ) { 
        this.directionsService = new google.maps.DirectionsService();
	    this.directionsDisplay = new google.maps.DirectionsRenderer();
	    this.bounds            = new google.maps.LatLngBounds();
	    this.waypoints = [
		      {
		        location: { lat: 4.6241329, lng: -74.1768411 },
		        stopover: true,
		      },
		      {
		        location: { lat: 4.6247745, lng: -74.1698888 },
		        stopover: true,
		      },
		      {
		        location: { lat: 4.6212241, lng: -74.1631081 },
		        stopover: true,
		      },
		      {
		        location: { lat: 4.6222508, lng: -74.1667989 },
		        stopover: true,
		      }
		];
	}
	ionViewDidLoad() {
    	if(localStorage.getItem('OPEN') == 'true'){
            this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
            this.deeplinks.route({
		    	 '/referidos/:id': RecargasgifcardreferidosPage,
		    }).subscribe(match => {
		     	// match.$route - the route we matched, which is the matched entry from the arguments to route()
		     	// match.$args - the args passed in the link
		     	// match.$link - the full link data
		     	//if(match.route.name="CompartirPage"){
		     		this.nav.push("RecargasgifcardreferidosPage", { codigo : match.$args.id});
		     	//}
		     	console.log('Successfully matched route', match);
		    }, nomatch => {
		    	// nomatch.$link - the full link data
		    	console.error('Got a deeplink that didn\'t match', nomatch);
		    });
            this.getPosition();        
        }else{
        	this.usuario = " Bienvenido";
            this.getPosition(); 
        }
	}
	getPosition(): any {
		    this.geolocation.getCurrentPosition()
		      .then(response => {
		        this.loadMap(response, 1);
		     })
		      .catch(error => {
		      	localStorage.setItem('latitude',  '100');
				localStorage.setItem('longitude', '100');
		      	this.loadMap(error, 2);
		        console.log(error);
		     })
	}
	link_principal(componente, p1){ 
	    //console.log(p1);
	    if(componente!=""){
		    if(localStorage.getItem('OPEN') == 'true'){ 
	           this.navController.setRoot(componente, { id : p1});
	        }else{
	           this.nav.push("VerificarPage");
	        }
	    }
	}//fin function
	link(componente, p1){ 
	    //console.log(p1);
	    if(componente!=""){
		    if(localStorage.getItem('OPEN') == 'true'){ 
	           this.nav.push(componente, { id : p1});
	        }else{
	           this.nav.push("VerificarPage");
	        }
	    }
	}//fin function
	mapa(){
        this.navController.setRoot('InicioPage');
	}//fin function
	perfil(){
	    if(localStorage.getItem('OPEN') == 'true'){ 
           this.nav.push('PerfilPage');
        }else{
          this.nav.push("VerificarPage");
        }
	}//fin function
	loadMap(position: Geoposition, op){ 
				this.urlimg2  = this.urlimg.getMyserve();
                if(op == 2){
                        let alerta = this.alertCtrl.create({
                          title: "GPS",
                          message: "El gps no puede determinar su ubicación",

                          buttons: [
                            {
	                            text: "Ok",
	                            role: 'cancel',
	                            cssClass:'ion-aceptar',
	                            handler: data => {
                                }
                            }
                          ]
                        });
                        alerta.present();
                }else{ 
                	this.latitude  = position.coords.latitude;
			        this.longitude = position.coords.longitude;
                }
                //Saber en que estado esta y pais
                //this.latitude  = -0.182687;
				//this.longitude = -78.481934;
                this.pointsService.localizacion(this.latitude, this.longitude).subscribe((response_aux) => {
                console.log(response_aux);
                for(var i=0; i<6 ; i++) { 
                	if(response_aux['results'][0]['address_components'][i]){
	                    if (response_aux['results'][0]['address_components'][i]['types'][0] == 'country' ) {                      this.pais      = response_aux['results'][0]['address_components'][i]['long_name']; }
	                    //if (response_aux['results'][0]['address_components'][i]['types'][0] == 'administrative_area_level_1' ) {  this.estado    = response_aux['results'][0]['address_components'][i]['long_name']; } 
	                    if (response_aux['results'][0]['address_components'][i]['types'][0] == 'locality' ) {                     this.municipio = response_aux['results'][0]['address_components'][i]['long_name']; } 
                	}
                }
                //localStorage.setItem('latitud',  this.latitude);
                //localStorage.setItem('longitud', this.longitude);
                localStorage.setItem('estado',  this.estado);
                localStorage.setItem('pais', this.pais);
                localStorage.setItem('municipio', this.municipio);
                 console.log(this.pais+' - '+this.estado+' - '+this.municipio)
				        this.pointsService.List(this.pais).subscribe((response) => {
				        	this.loading.dismiss().then( () => {
						            if (response.code!=200){
						            		// create un nuevo mapa pasando HTMLElement
										    let mapEle: HTMLElement = document.getElementById('map');
										    let panelEle: HTMLElement = document.getElementById('panel');
										    // Creando mi posicion
										    let myLatLng = { lat: this.latitude, lng: this.longitude };
										    // crear mapa
										    this.map = new google.maps.Map(mapEle, {
										       center: myLatLng, 
										       zoom: 15
										    }); 
										    this.directionsDisplay.setMap(this.map);
				                            this.directionsDisplay.setPanel(panelEle);
						            	    this.marker = new google.maps.Marker({
										          position:  new google.maps.LatLng(myLatLng),
										          map: this.map,
										          title: 'Mi Posicion',
										          draggable: false,
										          animation: google.maps.Animation.DROP,
										    });
										    mapEle.classList.add('show-map');
						            }else{
				                           
										    // create un nuevo mapa pasando HTMLElement
										    let mapEle: HTMLElement = document.getElementById('map');
										    let panelEle: HTMLElement = document.getElementById('panel');
										    // Creando mi posicion
										    let myLatLng = { lat: this.latitude, lng: this.longitude };
										    // crear mapa
										    this.map = new google.maps.Map(mapEle, {
										       center: myLatLng, 
										       zoom: 15
										    }); 
										    this.directionsDisplay.setMap(this.map);
				                            this.directionsDisplay.setPanel(panelEle);
										    google.maps.event.addListenerOnce(this.map, 'idle', () => {
											        let misdatos  = response['datos'];
											        var controlid = 0;
											        var residuos  = '';
											        var nombre    = '';    
											        misdatos.forEach(puntos => {
											            nombre     = puntos.razon_social; 
											            controlid  = puntos.id; 
											            this.query = puntos.id;
											            var contentString = '<div>' +puntos.direccion+'<br><br></div>';
												            this.marker = new google.maps.Marker({ 
												                position:  new google.maps.LatLng(puntos.latitud,puntos.longitud),
												                map: this.map,
												                title: nombre, 
												                draggable: false,
												                icon: this.urlimg2+''+puntos.tipos.imagen,
												                animation: google.maps.Animation.DROP
												            }); 
												            // fin del marcador
												            var that = this;
												            this.marker.addListener('click', function() {
												              that.demo(puntos.id, puntos.razon_social, contentString, puntos.latitud, puntos.longitud, myLatLng);
												            });
											        });  //fin del foreach
											        mapEle.classList.add('show-map');
										    });
										    this.marker = new google.maps.Marker({
										          position:  new google.maps.LatLng(myLatLng),
										          map: this.map,
										          title: 'Mi Posicion',
										          draggable: false,
										          animation: google.maps.Animation.DROP,
										    });
										    mapEle.classList.add('show-map');
										   
						            }//Fin else
				            });//FIN POST
				        },error => {
				        		let alerta = this.alertCtrl.create({
		                          title: "Aviso",
		                          message: "Disculpe, no se pudo conectar al servidor",
		                          buttons: [
		                            {
			                            text: "Reintentar",
			                            role: 'cancel',
			                            cssClass:'ion-aceptar',
			                            handler: data => {
			                            	this.loading.dismiss().then( () => {this.navController.setRoot('InicioPage');});
			                            	
		                                }
		                            }
		                          ]
		                        });
		                        alerta.present();
				        });//FIN POST
						this.loading = this.loadingCtrl.create({
					        dismissOnPageChange: false,
					        content: 'Cargando...'
					    });
					    this.loading.present();
			    });//FIN POST
	}//fin function
	calculateRoute(lt, lg, vmylt){ 
        /*this.bounds.extend(vmylt);
	    var point = new google.maps.LatLng(lg, lt);
	    this.bounds.extend(point);
	    this.map.fitBounds(this.bounds);*/
	    console.log(vmylt);  
	    this.directionsService.route({
	        origin: new google.maps.LatLng(vmylt),
	        destination: new google.maps.LatLng(lt, lg),
	        travelMode: google.maps.TravelMode.DRIVING
	    }, (response, status)=> {
	        if(status === google.maps.DirectionsStatus.OK) {
	          console.log(response);
	          this.directionsDisplay.setDirections(response);
	        }else{
	          console.log(response);
	          console.log(status);
	          //alert('Could not display directions due to: ' + status);
	          //alert('No hay ruta viable para llegar hasta el centro de belleza. ');
	        } 
	    });  
	}//fin function
	demo(id, nombre , contenido, latitud, longitud, mylt){
	    let alert = this.alertCtrl.create({
		    title: nombre.charAt(0).toUpperCase() + nombre.slice(1),
		    message:contenido,
		       buttons: [
			    {
			        text: 'Cerrar',
			        cssClass:'ion-cancelar',
			        role: 'cancel',
			        handler: data => {
			        }
			    },
			    {
			        text: 'Ver',
			        cssClass:'ion-ir',
			        handler: data => {
			        	if(localStorage.getItem('OPEN') == 'true'){
			          		this.nav.push("SalonPage", { id : id});
			          	}else{
			          		this.nav.push("SalonoffPage", { id : id});
			          	}
			        }
			    },
			    {
			        text: 'Ruta al centro',
			        cssClass:'ion-aceptar2',
			        handler: data => {
			        	
			          	this.calculateRoute(latitud, longitud, mylt);
			        }
			    }
		    ]
	    });
	    alert.present();
	}//fin function
}//fin class