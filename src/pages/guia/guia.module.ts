import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Guia } from './guia';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    Guia,
  ],
  imports: [
    IonicPageModule.forChild(Guia),
    Ionic2RatingModule
  ],
  exports: [
    Guia
  ]
})
export class GuiaModule {}
