import { Component, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Afiliacion } from '../../providers/servicio/afiliacion';
import { Perfil } from '../../providers/servicio/perfil';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { Ionic2RatingModule } from 'ionic2-rating';
import { Observable } from 'rxjs/Observable';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser';
import { IonicPage, NavController, Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-afiliacionampliar',
  templateUrl: 'afiliacionampliar.html',
  providers:[Perfil, Afiliacion, GlobalVars]
})
export class AfiliacionampliarPage {
    myForm: FormGroup;
    usuario = "";
    usuarioid = "";
    historiaid = "";
    fecha = "";
    dat  = 0;
    promocionid = 0;
    searchTerm:any="";
    allItems:any;
    items:any="";
    dar : any;
    data5 = "";
    data6 = "";
    mensaje = "";
    public url_ser  = new GlobalVars();
    public url_ser_ = "";

    public url_ser_perfil  = new GlobalVars();
    public url_ser__perfil = "";

    public ser:any =  [];
    @Input() data: any;
    @Input() data2: any;
    @Input() data3: any;
    @Input() data4: any;
    @Input() events: any;
    @ViewChild('wizardSlider') slider: Slides;
    sliderOptions = { pager: true };
    path:boolean = false;
    prev:boolean = true;
    next:boolean = true;
    finish:boolean = true;
    public option3: any;
    public options2: ThemeableBrowserOptions = {
       location : 'no',
       fullscreen : 'yes',
        statusbar: {
           color: '#ffffffff'
       },
       toolbar: {
           height: 0,
           color: '#f0f0f0ff',
       },
       backButtonCanClose: true
    };
    public options : InAppBrowserOptions = {
      location : 'no',//Or 'no' 
      /*hidden : 'no', //Or  'yes'
      clearcache : 'yes',
      clearsessioncache : 'yes',
      zoom : 'yes',//Android only ,shows browser zoom controls 
      hardwareback : 'yes',
      mediaPlaybackRequiresUserAction : 'no',
      shouldPauseOnSuspend : 'no', //Android only 
      closebuttoncaption : 'Close', //iOS only
      disallowoverscroll : 'no', //iOS only 
      toolbar : 'yes', //iOS only 
      enableViewportScale : 'no', //iOS only 
      allowInlineMediaPlayback : 'no',//iOS only 
      presentationstyle : 'pagesheet',//iOS only */ 
      fullscreen : 'yes',//Windows only    
    };
    public urlimg   = new GlobalVars();
    public urlimg2  = "";
    public loading:Loading;
    constructor(
              public nav: Nav, 
              private service_perfil: Perfil,
              public formBuilder: FormBuilder,
              public navCtrl: NavController, 
              private navController: NavController, 
              public alertCtrl: AlertController, 
              public loadingCtrl: LoadingController,
              public service: Afiliacion,
              public iab: InAppBrowser,
              public storage: Storage,
              public themeableBrowser: ThemeableBrowser,
              public navParams: NavParams) {
        this.myForm = this.formBuilder.group({
            radio_value: ['', Validators.required]
        });

  }
  cargar(v, c){ 
   this.nav.push(v, { id : c});
  }
  llenar(v, d){
     this.data5 = v;
     this.data6 = d;
  }
  cargarradio(){
    if(this.data5!=""){
          if(this.data6=="1"){
                              this.mensaje ="&nbsp;&nbsp; Recuerda que si haces un <b>Downgrade</b> de su cuenta, estas dejando de obtener los <b>beneficios</b> que permiten los planes <b>BLACK</b> y <b>GOLD</b>.";
          }else{
                              this.mensaje ="&nbsp;&nbsp; Los planes <b>GOLD</b> y <b>BLACK</b> tienen muchas sorpresas adicionales, Visita nuestro portal web para enterarte de todos los beneficios.";
          }
           let alert_ = this.alertCtrl.create({
              title: "¿Está seguro de cambiar su afiliación?",
              message:this.mensaje,
              buttons: [
                    {
                      text: 'Cancelar',
                      role: 'cancel',
                      cssClass:'ion-cancelar',
                      handler: data => {

                      }
                    },
                    {
                      text: 'Cambiar Plan',
                      cssClass:'ion-aceptar',
                      handler: data => {
                            this.service.update(this.data5).subscribe((response) => { 
                                    if (response.code==200){
                                                        this.service.pasarela_afiliacion_procesar(response.id, response.requestId, this.data5).subscribe((response3) => {  
                                                                  this.loading.dismiss().then( () => {
                                                                      let alert = this.alertCtrl.create({
                                                                        title: response3.msgh,
                                                                        message: response3.pasarela_message,
                                                                        buttons: [
                                                                          {
                                                                            text: 'Aceptar',
                                                                            role: 'cancel',
                                                                            cssClass:'ion-aceptar',
                                                                            handler: data => {
                                                                                this.navController.setRoot('InicioPage');
                                                                            }
                                                                          }
                                                                        ]
                                                                      });
                                                                      alert.present();
                                                                  });
                                                        });//FIN POST

                                    }else if (response.code==301){/////////////////ENVIA A CREAR SUSCRIPCIóN
                                      this.usuarioid = localStorage.getItem('ID');
                                      this.service_perfil.Perfilsuscripcion(this.usuarioid).subscribe((response_perfil) => { 
                                            if (response_perfil.code==200){

                                                      /*const browser_perfil = this.iab.create(response_perfil.processUrl,"_blank", this.options);
                                                      browser_perfil.on('loadstart').subscribe((response2_perfl) => {
                                                              this.url_ser__perfil = this.url_ser_perfil.getMyGlobalVar()+'pasarela_suscripcion';
                                                              if(response2_perfl.url==this.url_ser__perfil){*/

                                                      const browser_perfil: ThemeableBrowserObject = this.themeableBrowser.create(response_perfil.processUrl, '_blank', this.options2);
                                                      browser_perfil.on('loadstart').subscribe(response2_perfl => {
                                                      this.url_ser__perfil = this.url_ser_perfil.getMyGlobalVar()+'pasarela_suscripcion';
                                                                if(response2_perfl['url']==this.url_ser__perfil){
                                                                  
                                                                    this.service_perfil.suscripcion_placetoplay(this.usuarioid, response_perfil.requestId).subscribe((response3_perfil) => {  
                                                                      browser_perfil.close();
                                                                                this.loading.dismiss().then( () => {
                                                                                    let alert_perfil = this.alertCtrl.create({
                                                                                      title: response3_perfil.msgh,
                                                                                      message: response3_perfil.pasarela_message,
                                                                                      buttons: [
                                                                                        {
                                                                                          text: 'Aceptar',
                                                                                          role: 'cancel',
                                                                                          cssClass:'ion-aceptar',
                                                                                          handler: data => {
                                                                                                            if (response3_perfil.code==200){
                                                                                                                  this.service.update(this.data5).subscribe((response__auxiliar) => {
                                                                                                                      //AQu RGRESA A HaCER EL COLLECT
                                                                                                                        this.service.pasarela_afiliacion_procesar(response__auxiliar.id, response__auxiliar.requestId, this.data5).subscribe((response3) => {  
                                                                                                                                  this.loading.dismiss().then( () => {
                                                                                                                                      let alert = this.alertCtrl.create({
                                                                                                                                        title: response3.msgh,
                                                                                                                                        message: response3.pasarela_message,
                                                                                                                                        buttons: [
                                                                                                                                          {
                                                                                                                                            text: 'Aceptar',
                                                                                                                                            cssClass:'ion-aceptar',
                                                                                                                                            handler: data => {
                                                                                                                                                this.navController.setRoot('InicioPage');
                                                                                                                                            }
                                                                                                                                          }
                                                                                                                                        ]
                                                                                                                                      });
                                                                                                                                      alert.present();
                                                                                                                                  });
                                                                                                                        });//FIN POST
                                                                                                                  });
                                                                                                                  this.loading = this.loadingCtrl.create({
                                                                                                                    dismissOnPageChange: false,
                                                                                                                  });
                                                                                                                  this.loading.present();
                                                                                                              }
                                                                                            }//fin handler
                                                                                          }
                                                                                        ]
                                                                                      });
                                                                                      alert_perfil.present();
                                                                                  });
                                                                      });//FIN POST
                                                                }//Fin if
                                                        });//fin proceso de suscripcion
                                              }else{
                                                        this.loading.dismiss().then( () => {
                                                              let alert = this.alertCtrl.create({
                                                                title: "Suscripción de tarjeta!",
                                                                message: "La Suscripción de tarjeta no pudo ser procesada, intente de nuevo!",
                                                                buttons: [
                                                                    {
                                                                    text: 'Salir',
                                                                    role: 'cancel',
                                                                    cssClass:'ion-cancelar',
                                                                    handler: data => {
                                                                              //AQUI REGRESAR
                                                                             // this.navController.remove(this.navController.getActive().index);
                                                                    }
                                                                  }
                                                              ]
                                                              });
                                                              alert.present();
                                                          });
                                                }//Fin else
                                      });//fin de service_perfil
                                    }else if (response.code==201){
                                            this.loading.dismiss().then( () => {
                                                let alert = this.alertCtrl.create({
                                                  title: response.msgh,
                                                  message: response.msg,
                                                  buttons: [
                                                              {
                                                                text: 'Aceptar',
                                                                role: 'cancel',
                                                                cssClass:'ion-aceptar',
                                                                handler: data => {
                                                                    this.navController.setRoot('InicioPage');
                                                                }
                                                              }
                                                            ]
                                                });
                                                alert.present();
                                            });
                                    }else{
                                            this.loading.dismiss().then( () => {
                                                  let alert = this.alertCtrl.create({
                                                    title: "Respuesta cambio afiliación!",
                                                    message: "La afiliación no pudo ser modificada.",
                                                    buttons: [
                                                        {
                                                        text: 'Salir',
                                                        role: 'cancel',
                                                        cssClass:'ion-cancelar',
                                                        handler: data => {
                                                            this.navController.setRoot('InicioPage');
                                                        }
                                                      },
                                                      {
                                                        text: 'Aceptar',
                                                        cssClass:'ion-aceptar',
                                                        handler: data => {

                                                        }
                                                      }
                                                  ]
                                                  });
                                                  alert.present();
                                              });
                                    }//Fin else
                            }); 
                            this.loading = this.loadingCtrl.create({
                                dismissOnPageChange: false,
                              });
                            this.loading.present();
                      }
                    }
                  ],
          });
          alert_.present();
    }//fin if
  }//fin function
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            this.usuario     = localStorage.getItem('NOMBRESAPELLIDOS');
            this.usuarioid   = localStorage.getItem('ID');
            this.urlimg2     = this.urlimg.getMyserve();
            this.service.afiliacion().subscribe((response) => { 
                      if (response.code!=200){

                      }else{
                            this.data2 = response.datos;//publicidad
                            this.data3 = response.datos;//publicidad
                            this.dar   = response.afiliacion_id;
                            this.service.list(this.dar).subscribe((response) => { 
                                if (response.code!=200){

                                }else{
                                      this.data4 = response.datos1;//publicidad
                                      console.log(this.data4);
 
                                }
                            });
                      } 
                }); 
            this.data  = this.getDataForLayout1(); 
        }else{ 
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");
        } 
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     //this.nav.push(componente, { id : p1});
     this.navController.remove(this.navController.getActive().index);
    }
  }//fin function
  getDataForLayout1 = (): any => {
        return {
            "toolBarTitle":"Simple + icon",
            "btnPrev":"<",
            "btnNext":">",
            "btnFinish":"Finish",
            "items":[
               {
                  "logo":"",
                  "iconSlider":"./assets/images/6-imagen2-03.png",
                  "title":"Fragment Example 1",
                  "buttonNext":">"
               },
               {
                  "logo":"",
                  "iconSlider":"./assets/images/6-imagen2-03.png",
                  "title":"Fragment Example 2",
                  "description":"Text for Fragment Example 2 Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                  "buttonNext":">",
                  "buttonPrevious":"<"
               },
               {
                  "logo":"",
                  "iconSlider":"./assets/images/6-imagen2-03.png",
                  "title":"Fragment Example 3",
                  "buttonPrevious":"<"
               }
            ]
         };
  };
  changeSlide(index: number): void {
        if (index > 0) {
            this.slider.slideNext(300);
        } else {
            this.slider.slidePrev(300);
        }
  }
  slideHasChanged(index: number): void {
        try {
            this.prev = !this.slider.isBeginning();
            this.next = this.slider.getActiveIndex() < (this.slider.length() - 1);
            this.finish = this.slider.isEnd();
        } catch (e) { }
  }
}//fin componente 
