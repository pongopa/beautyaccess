import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AfiliacionampliarPage } from './afiliacionampliar';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    AfiliacionampliarPage
  ],
  imports: [
    IonicPageModule.forChild(AfiliacionampliarPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    AfiliacionampliarPage
  ]
})
export class AfiliacionampliarPageModule {}
