import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InformativoPage } from './informativo';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    InformativoPage
  ],
  imports: [
    IonicPageModule.forChild(InformativoPage),
    MyNavbarModule
  ],
  exports: [
    InformativoPage
  ]
})
export class InformativoPageModule {}
