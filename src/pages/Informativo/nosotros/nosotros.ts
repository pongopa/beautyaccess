import { Component, ViewChild } from '@angular/core';
import { Informativo } from '../../../providers/servicio/informativo';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, LoadingController, Loading, AlertController, ViewController, MenuController, Nav } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-nosotros',
  templateUrl: 'nosotros.html',
  providers:[Informativo]
})
export class NosotrosPage {
@ViewChild(Nav) nav: Nav;
   public query = [
    {
      tipo: '1',
    }
  ];
  public texto = "";
  public usuario = "";
  constructor(public navController: NavController, 
  	          public nave: Nav,  
  	          private _service: Informativo,
  	          private viewCtrl: ViewController,
  	          public alertCtrl: AlertController,
              public storage: Storage
  	         ) {}
  ionViewDidLoad() {
       this.usuario = "Nosotros";
        this._service.List(this.query).subscribe((response) => { 
                      this.texto = response.datos.text;
        });//FIN POST
  
  }
} 
