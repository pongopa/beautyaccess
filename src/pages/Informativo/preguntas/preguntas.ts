import { Component, ViewChild } from '@angular/core';
import { Informativo } from '../../../providers/servicio/informativo';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, LoadingController, Loading, AlertController, ViewController, MenuController, Nav } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-preguntas',
  templateUrl: 'preguntas.html',
  providers:[Informativo]
})
export class PreguntasPage {
@ViewChild(Nav) nav: Nav;
   public query = [
    {
      tipo: '2',
    }
  ];
  public texto = "";
  public usuario = "";
  constructor(public navController: NavController, 
  	          public nave: Nav,  
  	          private _service: Informativo,
  	          private viewCtrl: ViewController,
  	          public alertCtrl: AlertController,
              public storage: Storage
  	         ) {}
  ionViewDidLoad() {
       this.usuario = "Preguntas frecuentes";
        this._service.List(this.query).subscribe((response) => { 
                      this.texto = response.datos.text;
        });//FIN POST
  
  }


}//fin class 
