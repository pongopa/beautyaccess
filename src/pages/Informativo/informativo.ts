import { Component, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, LoadingController, Loading, AlertController, ViewController, MenuController, Nav } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-informativo',
  templateUrl: 'informativo.html'
})
export class InformativoPage {
@ViewChild(Nav) nav: Nav;
public appPages = [
    {
      title: 'Nosotros',
      subtitle: 'información sobre nosotros',
      url: 1,
      icon: 'contacts',
    },
    {
      title: 'Preguntas frecuentes',
      subtitle: 'Preguntas realizadas con frecuencia',
      url: 2,
      icon: 'help-circle',
    },
    {
      title: 'Terminos y condiciones',
      subtitle: 'Terminos del sitio',
      url: 3,
      icon: 'attach',
    },
    {
      title: 'Politicas de usos',
      subtitle: 'Politicas del sitio',
      url: 4,
      icon: 'information-circle',
    },
    {
      title: 'Noticias',
      subtitle: 'Noticias del sitio',
      url: 5,
      icon: 'chatbubbles',
    }
    
  ];
  public usuario = "";
  constructor(public storage: Storage, public navController: NavController, public nave: Nav,  private viewCtrl: ViewController) {}
  openPage(pages) {
    
          if(pages==1){
                        this.navController.push("NosotrosPage");  
    }else if(pages==2){
                        this.navController.push("PreguntasPage");
    }else if(pages==3){
                        this.navController.push("TerminosPage");
    }else if(pages==4){
                        this.navController.push("PoliticasPage");
    }else if(pages==5){
                        this.navController.push("NoticiasPage");  
    }
    //alert(pages);
  }
  mapa(){
     this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
        this.nav.push('PerfilPage');
  }//fin function
  ionViewDidLoad() {
    if(localStorage.getItem('OPEN') == 'true'){
        //this.navCtrl.setRoot('HomePage');
         this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
    }else{
        this.storage.remove('ID');
        this.storage.remove('USERNAME');
        this.storage.remove('NOMBRES');
        this.storage.remove('APELLIDOS');
        this.storage.remove('TOKEN');
        this.storage.remove('OPEN');
    }
  }
} 
