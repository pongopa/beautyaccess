import { Component, Input } from '@angular/core';
import { IonicPage, NavController,LoadingController, Loading, AlertController, Nav, MenuController, ModalController} from 'ionic-angular';
import { Geolocation, Geoposition } from "@ionic-native/geolocation";
import { PointsService } from '../../providers/servicio/mapas';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalVars } from '../../providers/servicio/global';

declare var google;
@IonicPage()
@Component({
  selector: 'page-buscarservicio',
  templateUrl: 'buscarservicio.html',
  providers: [Geolocation, GlobalVars, PointsService]
})
export class BuscarservicioPage {
  @Input() data: any;
  @Input() events: any;

  searchTerm:any="";
  allItems:any;
  myForm: FormGroup;
  public loading:Loading;
  public loading2:Loading;

  items:any;
  headerImage = "";
	//@ViewChild(Nav) nav: Nav;
    map: any;
	marker: any;
	features: any;
	datas: any;
	public marcadores: any;
	public usuariodeno: any;
	public var : any;
	public query = "";

	public pais = "";
    public estado = "";
    public municipio = "";
    public userid = "";

	directionsService: any = null;
	directionsDisplay: any = null;
	bounds: any;
	calificacion: any;
	myLatLng: any;
	waypoints: any[];

	public usuario = "";
	public urlimg   = new GlobalVars();
	public urlimg2  = "";

	constructor(
	    public nav: Nav,
	    private navCtrl: NavController,
	    private navController: NavController, 
	    private geolocation: Geolocation,
	    public loadingCtrl: LoadingController,
	    public  alertCtrl: AlertController,
	    public menuCtrl: MenuController,
	    private pointsService: PointsService,
	    public storage: Storage,
	    public formBuilder: FormBuilder,
	    public modalCtrl : ModalController,
	  ) { 
          
        this.directionsService = new google.maps.DirectionsService();
	    this.directionsDisplay = new google.maps.DirectionsRenderer();
	    this.bounds            = new google.maps.LatLngBounds();
	    this.myForm = this.formBuilder.group({
	      searchbar: ['', Validators.required]
	    });
	    
	}
	ionViewDidLoad() {
    	if(localStorage.getItem('OPEN') == 'true'){
    		this.urlimg2 = this.urlimg.getMyserve();
            this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
            this.loag();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
		    localStorage.setItem('OPEN','false');
		    this.navController.setRoot("VerificarPage");
        }
	}
	loag(){

		
	}
	mapa(){
        this.navController.setRoot('InicioPage');
	}//fin function
	perfil(){
	    this.nav.push('PerfilPage');
	}//fin function
	getItems(event: any):void {
	    if (!this.allItems) {
	      this.allItems = this.items;
	    }
	    this.items = this.allItems.filter((item) => {
	        return item.razon_social.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
	    });
	    
	}
	onEvent2(event:string, item:any) {//ITEM [EVENT OR SELECTED ITEM]
	        console.log(this.searchTerm);
	      	this.pais      = localStorage.getItem('pais');
			this.estado    = localStorage.getItem('estado');
			this.municipio = localStorage.getItem('municipio');
			this.userid  = localStorage.getItem('ID');
			this.pointsService.BuscarserviciosAll(this.pais, this.estado, this.municipio, this.searchTerm, this.userid).subscribe((response) => { 
				this.loading2.dismiss().then( () => {
		 	        if (response.code==200){
		                this.items       = response['datos'];
		                this.headerImage = "assets/background/27.jpg";
		                 //this.getItems(item);
		                console.log(this.items);
		            }//fin else
			    }); 
		    },error => {
	            this.loading2.dismiss().then( () => {});
	        });//FIN POST
		    this.loading2 = this.loadingCtrl.create({
		        dismissOnPageChange: false,
		        content: 'Cargando...'
		    });
		    this.loading2.present();
	}//fin function
	onEvent(event:string, item:any) {//ITEM [EVENT OR SELECTED ITEM]
	    //if (this.events[event]) {
	      if ('onTextChange' === event) {
	      	            console.log(this.searchTerm);
				      	this.pais      = localStorage.getItem('pais');
						this.estado    = localStorage.getItem('estado');
						this.municipio = localStorage.getItem('municipio');
						this.userid  = localStorage.getItem('ID');
						this.pointsService.BuscarserviciosAll(this.pais, this.estado, this.municipio, this.searchTerm, this.userid).subscribe((response) => { 
							this.loading.dismiss().then( () => {
						 	        if (response.code==200){
						                this.items       = response['datos'];
						                this.headerImage = "assets/background/27.jpg";
						                 //this.getItems(item);
						                console.log(this.items);
						            }else{
						            	let alerta = this.alertCtrl.create({
							                  title: "Aviso!!",
							                  message: response.msg,
							                  buttons: [
							                    {
							                        text: "Reintentar",
							                        role: 'cancel',
							                        cssClass:'ion-aceptar',
							                        handler: data => { }
							                    }
							                  ]
							            });
							            alerta.present();
						            }//fin else
						    }); 
					    },error => {
				            let alerta = this.alertCtrl.create({
				                  title: "Aviso",
				                  message: "Disculpe, no se pudo conectar al servidor",
				                  buttons: [
				                    {
				                        text: "Reintentar",
				                        role: 'cancel',
				                        cssClass:'ion-aceptar',
				                        handler: data => {
				                          this.loading.dismiss().then( () => {this.nav.pop();});
				                        }
				                    }
				                  ]
				                });
				                alerta.present();
				        });//FIN POST
					    this.loading = this.loadingCtrl.create({
					        dismissOnPageChange: false,
					        content: 'Cargando...'
					    });
					    this.loading.present();
	      } else {
	        //this.events[event](item);
	        this.nav.push("SalonPage", { id : item.id});
	      }
	    //}
	    console.log(event);
	}
	

}//fin class