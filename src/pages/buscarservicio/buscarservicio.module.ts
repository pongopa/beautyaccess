import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuscarservicioPage } from './buscarservicio';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    BuscarservicioPage
  ],
  imports: [
    IonicPageModule.forChild(BuscarservicioPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    BuscarservicioPage
  ]
})
export class BuscarservicioPageModule {}
