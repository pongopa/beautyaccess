import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CuestionarioPreguntaPage } from './cuestionariopregunta';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    CuestionarioPreguntaPage
  ],
  imports: [
    IonicPageModule.forChild(CuestionarioPreguntaPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    CuestionarioPreguntaPage
  ]
})
export class CuestionarioPreguntaPageModule {}
