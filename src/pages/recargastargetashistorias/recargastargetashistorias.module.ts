import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecargastargetashistoriasPage } from './recargastargetashistorias';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    RecargastargetashistoriasPage
  ],
  imports: [
    IonicPageModule.forChild(RecargastargetashistoriasPage),
    MyNavbarModule
  ],
  exports: [
    RecargastargetashistoriasPage
  ]
})
export class RecargastargetashistoriasPageModule {}
