import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Salon } from '../../providers/servicio/salon';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/servicio/global';
import { IonicPage, NavController,Slides, LoadingController, Loading, AlertController, MenuController, Nav, NavParams } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-salonphoto',
  templateUrl: 'salonphoto.html',
  providers:[Salon, GlobalVars]
})
export class SalonphotoPage {
myForm: FormGroup;
salonid: any;
public loading:Loading;
public usuario = "";
public datos: any;
public contentString = "";
public contentStringT = "";
public contador = 0;
public valor    = 0;
public result   = 0;

public user_id ="";

@ViewChild('wizardSlider2') slider2: Slides;
sliderOptions2 = { pager2: true };
path2:boolean = false;
prev2:boolean = true;
next2:boolean = true;
finish2:boolean = true;

public razon_social = "";
public email        = "";
public direccion    = "";
public telefono     = "";
public servicios_t  = "";

public img1     = "";
public img2     = "";
public img3     = "";
public img4     = "";
public img5     = "";
public img6     = "";
public img7     = "";
public img8     = "";
public img9     = "";
public urlimg2  = "";
public urlimg   = new GlobalVars();

  constructor(public nav: Nav,
              public navController: NavController,
              public alertCtrl: AlertController, 
              public nave: Nav,  
              private _service: Salon,
              public loadingCtrl: LoadingController,
              public storage: Storage,
              public formBuilder: FormBuilder,
              public navParams: NavParams) {
    this.myForm = this.formBuilder.group({
      texto: ['', Validators.required]
    });
  }
  link(componente, p1){ 
    //console.log(p1);
    if(componente!=""){
     this.nav.push(componente, { id : p1});
    }
  }//fin function
  ionViewDidLoad() {
        if(localStorage.getItem('OPEN') == 'true'){
            //this.navCtrl.setRoot('HomePage');
            this.usuario = localStorage.getItem('NOMBRESAPELLIDOS');
            this.salonid = this.navParams.get('id');
            this.loadpage();
        }else{
            this.storage.remove('ID');
            this.storage.remove('USERNAME');
            this.storage.remove('NOMBRES');
            this.storage.remove('APELLIDOS');
            this.storage.remove('TOKEN');
            this.storage.remove('OPEN');
            this.storage.set('OPEN', 'false');
            localStorage.setItem('OPEN','false');
            this.navController.setRoot("VerificarPage");

            localStorage.clear();
        }
  }
  comentario(){
    this.nav.push("ComentarioPage", { id : this.salonid});
  }
  reservafechahora(){
    this.nav.push("ReservafechahoraPage", { id : this.salonid});
  }
  mapa(){
        this.navController.setRoot('InicioPage');
  }//fin function
  perfil(){
      this.nav.push('PerfilPage');
  }//fin function
  changeSlide2(index: number): void {
        if (index > 0) {
            this.slider2.slideNext(300);
        } else {
            this.slider2.slidePrev(300);
        }
  }
  slideHasChanged2(index: number): void {
        try {
            this.prev2   = !this.slider2.isBeginning();
            this.next2   = this.slider2.getActiveIndex() < (this.slider2.length() - 1);
            this.finish2 = this.slider2.isEnd();
        } catch (e) { }
  }
  loadpage(){
    this.user_id = localStorage.getItem('ID');
      this._service.Datos(this.salonid, this.user_id).subscribe((response) => { 
                  this.loading.dismiss().then( () => { 
                                if (response.code==200){
                                  this.datos        = response['datos'];
                                  
                                  this.razon_social = this.datos[0]['razon_social'];
                                  this.email        = this.datos[0]['email'];
                                  this.direccion    = this.datos[0]['direccion'];
                                  this.telefono     = this.datos[0]['telefono'];
                                  this.servicios_t  = this.datos[0]['servicios'][0]['deno_ser'];

                                  if(this.datos[0]['foto1']==""){this.datos[0]['foto1']="upload/default.png";}
                                  if(this.datos[0]['foto2']==""){this.datos[0]['foto2']="upload/default.png";}
                                  if(this.datos[0]['foto3']==""){this.datos[0]['foto3']="upload/default.png";}
                                  if(this.datos[0]['foto4']==""){this.datos[0]['foto4']="upload/default.png";}
                                  if(this.datos[0]['foto5']==""){this.datos[0]['foto5']="upload/default.png";}
                                  if(this.datos[0]['foto6']==""){this.datos[0]['foto6']="upload/default.png";}
                                  if(this.datos[0]['foto7']==""){this.datos[0]['foto7']="upload/default.png";}
                                  if(this.datos[0]['foto8']==""){this.datos[0]['foto8']="upload/default.png";}
                                  if(this.datos[0]['foto9']==""){this.datos[0]['foto9']="upload/default.png";}

                                  this.img1     = this.datos[0]['foto1'];
                                  this.img2     = this.datos[0]['foto2'];
                                  this.img3     = this.datos[0]['foto3'];
                                  this.img4     = this.datos[0]['foto4'];
                                  this.img5     = this.datos[0]['foto5'];
                                  this.img6     = this.datos[0]['foto6'];
                                  this.img7     = this.datos[0]['foto7'];
                                  this.img8     = this.datos[0]['foto8'];
                                  this.img9     = this.datos[0]['foto9'];
                                  this.urlimg2  = this.urlimg.getMyserve();
                                  
                                                                 
                          }else{
                            
                          }//Fin else
                });
      },error => {
                let alerta = this.alertCtrl.create({
                      title: "Aviso",
                      message: "Disculpe, no se pudo conectar al servidor",
                      buttons: [
                        {
                            text: "Reintentar",
                            role: 'cancel',
                            cssClass:'ion-aceptar',
                            handler: data => {
                              this.loading.dismiss().then( () => {this.nav.pop();});
                            }
                        }
                      ]
                    });
                    alerta.present();
      });//FIN POST
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: false,
        content: 'Cargando...'
      });
      this.loading.present();
  }
 
}//fin componente 
