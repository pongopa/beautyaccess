import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalonphotoPage } from './salonphoto';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    SalonphotoPage
  ],
  imports: [
    IonicPageModule.forChild(SalonphotoPage),
    MyNavbarModule
  ],
  exports: [
    SalonphotoPage
  ]
})
export class SalonphotoPageModule {}
