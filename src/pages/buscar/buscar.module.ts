import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuscarPage } from './buscar';

// Import ionic2-rating module
import { Ionic2RatingModule } from 'ionic2-rating';
import { MyNavbarModule } from "../../components/my-navbar-components/my-navbar.module";

@NgModule({
  declarations: [
    BuscarPage
  ],
  imports: [
    IonicPageModule.forChild(BuscarPage),
    Ionic2RatingModule,
    MyNavbarModule
  ],
  exports: [
    BuscarPage
  ]
})
export class BuscarPageModule {}
