import {Injectable} from '@angular/core';
@Injectable()
export class GlobalVars {
  public myGlobalVar:string;
  public apikey:string;
  public serve:string;
  constructor() {
  	this.apikey      = "$2y$10$DKfEwA4gu2/io,whglebp223VM5HGsaafc.lAZfBlgQqpon776mm";
    
    //this.myGlobalVar = "http://localhost/proyectos_ionic/beautyaccess/Backend/public/api/";
    //this.serve       = "http://localhost/proyectos_ionic/beautyaccess/sistema_admin/app/webroot/";

    //this.myGlobalVar = "https://beautyaccess.jcloudtec.com/api/public/api/";   
    //this.serve       = "https://beautyaccess.jcloudtec.com/administrador/app/webroot/";

    this.myGlobalVar = "https://app.beautyaccess.app/api/public/api/";    
    this.serve       = "https://app.beautyaccess.app/administrador/app/webroot/";
  }
  getMyGlobalVar() {
    return this.myGlobalVar;
  }
  getMyapiKey() {  
    return this.apikey;
  }
  getMyserve() {
    return this.serve; 
  }   
}               

///usr/local/bin/php -q /home/beautyaccessjclo/public_html/api/public/observar_canjes.php