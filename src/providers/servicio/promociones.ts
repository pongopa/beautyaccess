import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './global';
@Injectable()
@Component({
  providers:[GlobalVars]
})
export class Promociones {
    public url = new GlobalVars();
    public headers = new Headers();
    constructor(
        public _http: Http, 
      ) {
        this.headers.append('content-type','application/json');
        this.headers.append('token2',localStorage.getItem('TOKEN'));
        this.headers.append('remember_token',this.url.getMyapiKey());
    }
    list(datos){
                return this._http.post(this.url.getMyGlobalVar()+"promocioneslist",
                        JSON.stringify({
                            'user_id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    views(datos){
                return this._http.post(this.url.getMyGlobalVar()+"promocion",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    Recargasgifcard(datos, user, tiporecarga ){
                return this._http.post(this.url.getMyGlobalVar()+"recargasgifcard",
                        JSON.stringify({
                            'codigo':datos,
                            'user_id':user,
                            'tiporecarga_id':tiporecarga
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    } 

}