import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './global';
import { Calif } from '../models/calificacion.model';

@Injectable()
@Component({
  providers:[GlobalVars]
})
export class Reservas {
    public url = new GlobalVars();
    public headers = new Headers();
    constructor(
        public _http: Http, 
      ) {
        this.headers.append('content-type','application/json');
        this.headers.append('token2',localStorage.getItem('TOKEN'));
        this.headers.append('remember_token',this.url.getMyapiKey());
    }
    ReservasAll(datos1){
                return this._http.post(this.url.getMyGlobalVar()+"reservasall",
                        JSON.stringify({
                            'usuarioid':datos1,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    } 
    ReservashistoricoAll(datos1){
                return this._http.post(this.url.getMyGlobalVar()+"reservashistoricoall",
                        JSON.stringify({
                            'usuarioid':datos1,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    } 
    Ver(datos){
                return this._http.post(this.url.getMyGlobalVar()+"reservasver",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    cancelar(datos){
                return this._http.post(this.url.getMyGlobalVar()+"reservascancelar",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    } 
    reservasupdatehora(datos1, datos2, datos3, datos4){
                return this._http.post(this.url.getMyGlobalVar()+"reservasupdatehora",
                        JSON.stringify({
                            'idreserva':datos1,
                            'iduser':datos2,
                            'hora':datos3,
                            'fecha':datos4,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    reservasupdatefecha(datos1, datos2, datos3){
                return this._http.post(this.url.getMyGlobalVar()+"reservasupdatefecha",
                        JSON.stringify({
                            'idreserva':datos1,
                            'iduser':datos2,
                            'fecha':datos3,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    } 
    factura(datos1, datos2, datos3, datos4, datos5){
                return this._http.post(this.url.getMyGlobalVar()+"reservasfactura",
                        JSON.stringify({
                            'salonid':datos1,
                            'fecha':datos2,
                            'hora':datos3,
                            'servicios':datos4,
                            'usuario_id':datos5,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    procesar(datos1, datos11, datos2, datos3, datos4, datos5){
                return this._http.post(this.url.getMyGlobalVar()+"reservasprocesar",
                        JSON.stringify({
                            'salonid':datos1,
                            'usuarioid':datos11,
                            'fecha':datos2,
                            'hora':datos3,
                            'servicios':datos4,
                            'version':'1',
                            'tarjeta':datos5,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    update_transaccion(datos1){
                return this._http.post(this.url.getMyGlobalVar()+"update_transaccion",
                        JSON.stringify({
                            'requestId':datos1,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    pasarela_verifica(datos1, datos2){
                return this._http.post(this.url.getMyGlobalVar()+"update_transaccion",
                        JSON.stringify({
                            'reservaid':datos1,
                            'requestId':datos2,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    calificaciontipos(){
                return this._http.post(this.url.getMyGlobalVar()+"rerservacalificaciones",
                        JSON.stringify({
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    calificaciones(datos1, datos2, datos3, datos4, datos5, datos6){
                return this._http.post(this.url.getMyGlobalVar()+"rerservacalificacionesadd",
                        JSON.stringify({
                            'salonid':datos1,
                            'usuarioid':datos2,
                            'reservaid':datos3,
                            'comentario':datos4.comentario,
                            'calificacion':datos5,
                            'atendido':datos6,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
}