import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './global';
import { Datos } from '../models/salon.model';

@Injectable()
@Component({
  providers:[GlobalVars]
})
export class Principal {
    public url = new GlobalVars();
    public headers = new Headers();
    constructor(
        public _http: Http, 
      ) {
        this.headers.append('content-type','application/json');
        this.headers.append('remember_token',this.url.getMyapiKey());
    }
    principal(datos){
                return this._http.post(this.url.getMyGlobalVar()+"principal",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    historias(datos){
                return this._http.post(this.url.getMyGlobalVar()+"historias",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    historialista(){
                return this._http.post(this.url.getMyGlobalVar()+"historialista",
                        JSON.stringify({
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }


    //////////Beautypoint////////
    historial(datos){
                return this._http.post(this.url.getMyGlobalVar()+"historial",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    beautypoint(datos){
                return this._http.post(this.url.getMyGlobalVar()+"beautypoint",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    //////////////////////


    productos(datos){
                return this._http.post(this.url.getMyGlobalVar()+"productos",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
     productosreservas(datos, datos2){
                return this._http.post(this.url.getMyGlobalVar()+"productosreservas",
                        JSON.stringify({
                            'usuario_id':datos,
                            'salon_id':datos2,  
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
}