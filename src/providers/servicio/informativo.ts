import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Datos } from '../models/informativo.model';
import { GlobalVars } from './global';
@Injectable()
@Component({
  providers:[GlobalVars]
})
export class Informativo{ //Se define la clase
        public v:string;
        public url = new GlobalVars();
        public headers = new Headers();
        constructor(
            public _http:Http,
        ){
             this.headers.append('content-type','application/json');
             this.headers.append('token2',localStorage.getItem('TOKEN'));
             this.headers.append('remember_token',this.url.getMyapiKey());
        }
        List(datos){
                return this._http.post(this.url.getMyGlobalVar()+"informativolist",
                        JSON.stringify({
                            'tipo':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }
        
}