import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './global';

@Injectable()
@Component({
  providers:[GlobalVars]
})
export class PointsService {
    public url = new GlobalVars();
    public headers = new Headers();
    constructor(
        public _http: Http, 
      ) {
            this.headers.append('content-type','application/json');
            this.headers.append('Accept','application/json');
            this.headers.append('remember_token',this.url.getMyapiKey());
    }
    localizacion(lat, lng){ 
                return this._http.post('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&key=AIzaSyDLW19BOiktbivjhDaTSU7vnTNCf5ZNyNk',
                        JSON.stringify({
                           
                        })
                        ,{})
                        .map(res=>res.json());
    } 
    List(a){ 
                return this._http.post(this.url.getMyGlobalVar()+"mapaslist",
                        JSON.stringify({
                            'pais':a
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    } 
    BuscarAll(a,b,c){
                return this._http.post(this.url.getMyGlobalVar()+"buscarall",
                        JSON.stringify({
                            'pais':a,
                            'estado':b,
                            'municipio':c,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    } 
    BuscarserviciosAll(a,b,c,d,e){
                return this._http.post(this.url.getMyGlobalVar()+"buscarservicioslist",
                        JSON.stringify({
                            'pais':a,
                            'estado':b,
                            'municipio':c,
                            'etiqueta':d,
                            'userid':e,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    } 
    Calificacion(datos){
                return this._http.post(this.url.getMyGlobalVar()+"calificacion",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
}