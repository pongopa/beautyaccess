import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Datos } from '../models/afiliacion.model';
import { GlobalVars } from './global';
@Injectable()
@Component({
  providers:[GlobalVars]
})
export class Afiliacion{ //Se define la clase
        public v:string;
        public url = new GlobalVars();
        public headers = new Headers();
        public user_id = "";
        constructor(
            public _http:Http,
        ){ 
            this.headers.append('content-type','application/json');
            this.headers.append('token2',localStorage.getItem('TOKEN'));
            this.headers.append('remember_token',this.url.getMyapiKey());
        } 
        pasarela_afiliacion_procesar(datos1, datos2, datos3){
                return this._http.post(this.url.getMyGlobalVar()+"pasarela_afiliacion_procesar",
                        JSON.stringify({
                            'recargaid':datos1,
                            'requestId':datos2,
                            'afiliacion_id':datos3,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }
        update(datos){
            this.user_id = localStorage.getItem('ID');
            return this._http.post(this.url.getMyGlobalVar()+"update",
                    JSON.stringify({
                        'afiliacion_id':datos,
                        'user_id':this.user_id
                    })
                    ,{headers:this.headers})
                    .map(res=>res.json());
        }
         list(datos){
            this.user_id = localStorage.getItem('ID');
            return this._http.post(this.url.getMyGlobalVar()+"afiliacionlis",
                    JSON.stringify({
                        'afiliacion_id':datos,
                        'user_id':this.user_id
                    })
                    ,{headers:this.headers})
                    .map(res=>res.json());
        } 
         afiliacion(){
            this.user_id = localStorage.getItem('ID');
            return this._http.post(this.url.getMyGlobalVar()+"afiliacion",
                    JSON.stringify({
                        'user_id':this.user_id
                    })
                    ,{headers:this.headers})
                    .map(res=>res.json());
        }
        
}