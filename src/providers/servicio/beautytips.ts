import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './global';

@Injectable()
@Component({
  providers:[GlobalVars]
})
export class Beautytips {
    public url = new GlobalVars();
    public headers = new Headers();
    constructor(
        public _http: Http, 
      ) {
        this.headers.append('content-type','application/json');
        this.headers.append('token2',localStorage.getItem('TOKEN'));
        this.headers.append('remember_token',this.url.getMyapiKey());
    }
    beautypointviews(datos){
                return this._http.post(this.url.getMyGlobalVar()+"beautypointviews",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }

    beautypointlist(){
                return this._http.post(this.url.getMyGlobalVar()+"beautypointlist",
                        JSON.stringify({
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    } 

}