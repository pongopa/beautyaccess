import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './global';
import { Datos } from '../models/salon.model';

@Injectable()
@Component({
  providers:[GlobalVars]
})
export class Salon {
    public url = new GlobalVars();
    public headers = new Headers();
    constructor(
        public _http: Http, 
      ) {
        this.headers.append('content-type','application/json');
        this.headers.append('token2',localStorage.getItem('TOKEN'));
        this.headers.append('remember_token',this.url.getMyapiKey());
    }
    Datos(datos, usuario){
                return this._http.post(this.url.getMyGlobalVar()+"datos",
                        JSON.stringify({
                            'id':datos,
                            'user_id':usuario,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    salon_hora_fecha(dato1, dato2){
                return this._http.post(this.url.getMyGlobalVar()+"salon_hora_fecha",
                        JSON.stringify({
                            'salonid':dato1,
                            'fecha':dato2,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    validar_hora_fecha(dato1, dato2, dato3){
                return this._http.post(this.url.getMyGlobalVar()+"validar_hora_fecha",
                        JSON.stringify({
                            'salonid':dato1,
                            'fecha':dato2,
                            'hora':dato3,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }

    validar_fecha(dato1, dato2){
                return this._http.post(this.url.getMyGlobalVar()+"validar_fecha",
                        JSON.stringify({
                            'salonid':dato1,
                            'fecha':dato2
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }

    Datoscomentarios(datos){
                return this._http.post(this.url.getMyGlobalVar()+"datoscomentarios",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
}