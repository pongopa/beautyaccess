import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './global';

@Injectable()
@Component({
  providers:[GlobalVars]
})
export class Perfil {
    public url = new GlobalVars();
    public headers = new Headers();
    constructor(
        public _http: Http, 
      ) {
            this.headers.append('content-type','application/json');
            this.headers.append('Accept','application/json');
            this.headers.append('remember_token',this.url.getMyapiKey());
    }
    compartirlist(datos1){
                return this._http.post(this.url.getMyGlobalVar()+"compartirlist",
                        JSON.stringify({
                            'user_id':datos1,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    Recargashostoria(datos1){
                return this._http.post(this.url.getMyGlobalVar()+"recargahistoria",
                        JSON.stringify({
                            'user_id':datos1,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    Suscripcionhostoria(datos1){
                return this._http.post(this.url.getMyGlobalVar()+"suscripcionhistoria",
                        JSON.stringify({
                            'user_id':datos1,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    Suscripcionlist(datos1){
                return this._http.post(this.url.getMyGlobalVar()+"suscripcionlist",
                        JSON.stringify({
                            'user_id':datos1,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    Suscripcionupdate(datos1, datos2, datos3){
                return this._http.post(this.url.getMyGlobalVar()+"suscripcionupdate",
                        JSON.stringify({
                            'id_predeterminado':datos1,
                            'id_nuevo':datos2,
                            'user_id':datos3,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    Suscripciondelete(datos1, datos2, datos3){
                return this._http.post(this.url.getMyGlobalVar()+"suscripciondelete",
                        JSON.stringify({
                            'id_predeterminado':datos1,
                            'id_nuevo':datos2,
                            'user_id':datos3,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    Perfilsuscripcion(datos){
                return this._http.post(this.url.getMyGlobalVar()+"perfilsuscripcion",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    suscripcion_placetoplay(datos1, datos2){
                return this._http.post(this.url.getMyGlobalVar()+"suscripcion_placetoplay",
                        JSON.stringify({
                            'user_id':datos1,
                            'requestId':datos2,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    Perfillist(datos){
                return this._http.post(this.url.getMyGlobalVar()+"perfillist",
                        JSON.stringify({
                            'id':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    
    Perfilupdate(use, datos){
                return this._http.post(this.url.getMyGlobalVar()+"perfilupdate",
                        JSON.stringify({
                            'id':use,
                            'datos':datos,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
}