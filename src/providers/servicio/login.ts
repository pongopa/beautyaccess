import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Datos } from '../models/login.model';
import { GlobalVars } from './global';
@Injectable()
@Component({
  providers:[GlobalVars]
})
export class Login{ //Se define la clase
        public v:string;
        public url = new GlobalVars();
        public headers = new Headers();
        public direccionGoogle : string = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=';
        constructor(
            public _http:Http,
        ){
            this.headers.append('content-type','application/json');
            this.headers.append('Accept','application/json');
            this.headers.append('remember_token',this.url.getMyapiKey());
        }
        UpdatePush(dato1, dato2, dato3){
                return this._http.post(this.url.getMyGlobalVar()+"updatepush",
                        JSON.stringify({
                            'token_push':dato1,
                            'platf_push':dato2,
                            'userid_push':dato3,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }
        login(datos:Datos){
                return this._http.post(this.url.getMyGlobalVar()+"login",
                        JSON.stringify({
                            'code_verificacion':datos.code_verificacion,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }
        verificar(datos:Datos){
                return this._http.post(this.url.getMyGlobalVar()+"verificar",
                        JSON.stringify({
                            'code_verificacion':datos.code_verificacion,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }
        registro(datos:Datos, email){
                return this._http.post(this.url.getMyGlobalVar()+"registro",
                        JSON.stringify({
                            'datos':datos,
                            'email':email,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }
        verificarclave(datos:Datos, email){

                return this._http.post(this.url.getMyGlobalVar()+"verificarclave",
                        JSON.stringify({
                            'code_verificacion':datos.code_verificacion,
                            'email':email,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }
         verificarclave_registro(clave, email){
                return this._http.post(this.url.getMyGlobalVar()+"verificarclaveregistro",
                        JSON.stringify({
                            'code_verificacion':clave,
                            'email':email,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }
        logout(datos:Datos){

        }
        recuperar1(datos:Datos){
                return this._http.post(this.url.getMyGlobalVar()+"recuperar1",
                        JSON.stringify({
                            'code_verificacion':datos.code_verificacion,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }

        recuperar2(datos:Datos){
                return this._http.post(this.url.getMyGlobalVar()+"recuperar2",
                        JSON.stringify({
                            'code_verificacion':datos.code_verificacion,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }
        recuperar3(email, datos:Datos){
                return this._http.post(this.url.getMyGlobalVar()+"recuperar3",
                        JSON.stringify({
                            'email':email,
                            'code_verificacion':datos.code_verificacion,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
        }
}

    