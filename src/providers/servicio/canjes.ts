import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './global';
@Injectable()
@Component({
  providers:[GlobalVars]
})
export class Canjes {
    public url = new GlobalVars();
    public headers = new Headers();
    constructor(
        public _http: Http, 
      ) {
        this.headers.append('content-type','application/json');
        this.headers.append('token2',localStorage.getItem('TOKEN'));
        this.headers.append('remember_token',this.url.getMyapiKey());
    }
    list(){ 
                return this._http.post(this.url.getMyGlobalVar()+"canjes",
                        JSON.stringify({
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    save(id_canje, id_user){
                return this._http.post(this.url.getMyGlobalVar()+"cajes_save",
                        JSON.stringify({
                            'id_canjes':id_canje,
                            'id_user':id_user,
                            'version':'1'

                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    pasarela_canje_procesar(datos1, datos2, datos3){
                return this._http.post(this.url.getMyGlobalVar()+"update_transaccion",
                        JSON.stringify({
                            'recargaid':datos1,
                            'requestId':datos2,
                            'canje_id':datos3,
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }

}