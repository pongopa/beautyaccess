import { Injectable, Component } from '@angular/core';
import { Http,Response,Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './global';
@Injectable()
@Component({
  providers:[GlobalVars]
})
export class Cuestionario {
    public url = new GlobalVars();
    public headers = new Headers();
    public user_id = "";
    constructor(
        public _http: Http, 
      ) {
        this.headers.append('content-type','application/json');
        this.headers.append('token2',localStorage.getItem('TOKEN'));
        this.headers.append('remember_token',this.url.getMyapiKey());
    }
    list(){
            this.user_id = localStorage.getItem('ID');
                return this._http.post(this.url.getMyGlobalVar()+"list",
                        JSON.stringify({
                            'user_id':this.user_id
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());
    }
    whats(id){
            this.user_id = localStorage.getItem('ID');
                return this._http.post(this.url.getMyGlobalVar()+"whats",
                        JSON.stringify({
                             'cuestionario_id':id,
                             'user_id':this.user_id
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());

    }
    save(id1, id2, id3){
            this.user_id = localStorage.getItem('ID');
                return this._http.post(this.url.getMyGlobalVar()+"save",
                        JSON.stringify({
                            'cuestionario_id':id1,
                            'cuestionariopregunta_id':id2,
                            'cuestionariodetalle_id':id3,
                            'user_id':this.user_id
                        })
                        ,{headers:this.headers})
                        .map(res=>res.json());

    }

}